import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("DESCRIPTION", function() {
	it("one", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0, "change setting1");
		const setting2 = undoable(0, "change setting2")
		const appearance1 = preservable("0");
		const appearance2 = preservable("1");
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		setting1[1](1);
		assert.equal(undoDescription(), "undo change setting1");
		assert.equal(redoDescription(), null);
		appearance1[1]("1");
		assert.equal(undoDescription(), "undo change setting1");
		assert.equal(redoDescription(), null);
		setting2[1](2);
		assert.equal(undoDescription(), "undo change setting2");
		assert.equal(redoDescription(), null);
		appearance2[1]("2");
		assert.equal(undoDescription(), "undo change setting2");
		assert.equal(redoDescription(), null);
		group( () => {
			setting1[1](11);
			setting2[1](22);
			appearance1[1]("11");
			appearance2[1]("22");
		}, "change both setting1 and setting2");
		assert.equal(undoDescription(), "undo change both setting1 and setting2");
		assert.equal(redoDescription(), null);
		group( () => {
			setting1[1](111);
			setting2[1](222);
			appearance1[1]("111");
			appearance2[1]("222");
		}, "change both setting1 and setting2");
		assert.equal(undoDescription(), "undo change both setting1 and setting2");
		assert.equal(redoDescription(), null);
		group( () => {
			setting1[1](1111);
			setting2[1](2222);
			appearance1[1]("1111");
		}, "change both setting1 and setting2 differently", "coalesce one");
		assert.equal(undoDescription(), "undo change both setting1 and setting2 differently");
		assert.equal(redoDescription(), null);
		group( () => {
			setting1[1](11111);
			setting2[1](22222);
			appearance2[1]("22222");
		}, "change both setting1 and setting2 differently", "coalesce one");
		assert.equal(undoDescription(), "undo change both setting1 and setting2 differently");
		assert.equal(redoDescription(), null);
		group( () => {
			setting1[1](111111);
			setting2[1](222222);
		}, "change both setting1 and setting2 without changing appearance", "coalesce two");
		assert.equal(undoDescription(), "undo change both setting1 and setting2 without changing appearance");	
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), "undo change both setting1 and setting2 differently");
		assert.equal(redoDescription(), "redo change both setting1 and setting2 without changing appearance");
		undo();
		assert.equal(undoDescription(), "undo change both setting1 and setting2");
		assert.equal(redoDescription(), "redo change both setting1 and setting2 differently");
		undo();
		assert.equal(undoDescription(), "undo change both setting1 and setting2");
		assert.equal(redoDescription(), "redo change both setting1 and setting2");
		undo();
		assert.equal(undoDescription(), "undo change setting2");
		assert.equal(redoDescription(), "redo change both setting1 and setting2");
		undo();
		assert.equal(undoDescription(), "undo change setting1");
		assert.equal(redoDescription(), "redo change setting2");
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting1");
		undo(); // one extra should have no effect
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting1");
		redo();
		assert.equal(undoDescription(), "undo change setting1");
		assert.equal(redoDescription(), "redo change setting2");
		redo();
		assert.equal(undoDescription(), "undo change setting2");
		assert.equal(redoDescription(), "redo change both setting1 and setting2");
		redo();
		assert.equal(undoDescription(), "undo change both setting1 and setting2");
		assert.equal(redoDescription(), "redo change both setting1 and setting2");
		redo();
		assert.equal(undoDescription(), "undo change both setting1 and setting2");
		assert.equal(redoDescription(), "redo change both setting1 and setting2 differently");
		redo();
		assert.equal(undoDescription(), "undo change both setting1 and setting2 differently");
		assert.equal(redoDescription(), "redo change both setting1 and setting2 without changing appearance");
		redo();
		assert.equal(undoDescription(), "undo change both setting1 and setting2 without changing appearance");
		assert.equal(redoDescription(), null);
		redo(); // one extra should have no effect
		assert.equal(undoDescription(), "undo change both setting1 and setting2 without changing appearance");
		assert.equal(redoDescription(), null);
	});

	it("二番", function() {
		const undoLocalizer = function(description) {
			return description + "を取り消す";
		};
		const redoLocalizer = function(description) {
			return description + "をやり直す";
		};
		const { undoable, preservable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0, "設定 1 の変更");
		const setting2 = undoable(0, "設定 2 の変更")
		const appearance1 = preservable("0");
		const appearance2 = preservable("1");
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		setting1[1](1);
		assert.equal(undoDescription(), "設定 1 の変更を取り消す");
		assert.equal(redoDescription(), null);
		appearance1[1]("1");
		assert.equal(undoDescription(), "設定 1 の変更を取り消す");
		assert.equal(redoDescription(), null);
		setting2[1](2);
		assert.equal(undoDescription(), "設定 2 の変更を取り消す");
		assert.equal(redoDescription(), null);
		appearance2[1]("2");
		assert.equal(undoDescription(), "設定 2 の変更を取り消す");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), "設定 1 の変更を取り消す");
		assert.equal(redoDescription(), "設定 2 の変更をやり直す");
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "設定 1 の変更をやり直す");
		redo();
		assert.equal(undoDescription(), "設定 1 の変更を取り消す");
		assert.equal(redoDescription(), "設定 2 の変更をやり直す");
		redo();
		assert.equal(undoDescription(), "設定 2 の変更を取り消す");
		assert.equal(redoDescription(), null);
	});

	it("language chooser declaration", function() {
		function undoLocalizer(description) {
			const position = languageEnum[language[0]()];
			return undoPrepend[position] + description[position] + undoAppend[position];
		};
		function redoLocalizer(description) {
			const position = languageEnum[language[0]()];
			return redoPrepend[position] + description[position] + redoAppend[position];
		};
		const languageEnum = {
			"English": 0,
			"日本語": 1
		};
		const undoPrepend = [
			"Undo ",
			""
		];
		const undoAppend = [
			"",
			"を取り消す"
		];
		const redoPrepend = [
			"Redo ",
			""
		];
		const redoAppend = [
			"",
			"をやり直す"
		];
		const { undoable, preservable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, ["change setting", "設定の変更"]);
		const language = preservable("English");
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		setting[1](1);
		assert.equal(setting[0](), 1);
		assert.equal(language[0](), "English");
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		language[1]("日本語");
		assert.equal(setting[0](), 1);
		assert.equal(language[0](), "日本語");
		assert.equal(undoDescription(), "設定の変更を取り消す");
		assert.equal(redoDescription(), null);
		setting[1](2);
		assert.equal(setting[0](), 2);
		assert.equal(language[0](), "日本語");
		assert.equal(undoDescription(), "設定の変更を取り消す");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(setting[0](), 1);
		assert.equal(language[0](), "日本語");
		assert.equal(undoDescription(), "設定の変更を取り消す");
		assert.equal(redoDescription(), "設定の変更をやり直す");
		undo();
		assert.equal(setting[0](), 0);
		assert.equal(language[0](), "English");
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo change setting");
		redo();
		assert.equal(setting[0](), 1);
		assert.equal(language[0](), "English");
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), "Redo change setting");
		redo();
		assert.equal(setting[0](), 2);
		assert.equal(language[0](), "日本語");
		assert.equal(undoDescription(), "設定の変更を取り消す");
		assert.equal(redoDescription(), null);
	});

	it("language chooser expression", function() {
		const undoLocalizer = function(description) {
			const position = languageEnum[language[0]()];
			return undoPrepend[position] + description[position] + undoAppend[position];
		};
		const redoLocalizer = function(description) {
			const position = languageEnum[language[0]()];
			return redoPrepend[position] + description[position] + redoAppend[position];
		};
		const languageEnum = {
			"English": 0,
			"日本語": 1
		};
		const undoPrepend = [
			"Undo ",
			""
		];
		const undoAppend = [
			"",
			"を取り消す"
		];
		const redoPrepend = [
			"Redo ",
			""
		];
		const redoAppend = [
			"",
			"をやり直す"
		];
		const { undoable, preservable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, ["change setting", "設定の変更"]);
		const language = preservable("English");
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		setting[1](1);
		assert.equal(setting[0](), 1);
		assert.equal(language[0](), "English");
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		language[1]("日本語");
		assert.equal(setting[0](), 1);
		assert.equal(language[0](), "日本語");
		assert.equal(undoDescription(), "設定の変更を取り消す");
		assert.equal(redoDescription(), null);
		setting[1](2);
		assert.equal(setting[0](), 2);
		assert.equal(language[0](), "日本語");
		assert.equal(undoDescription(), "設定の変更を取り消す");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(setting[0](), 1);
		assert.equal(language[0](), "日本語");
		assert.equal(undoDescription(), "設定の変更を取り消す");
		assert.equal(redoDescription(), "設定の変更をやり直す");
		undo();
		assert.equal(setting[0](), 0);
		assert.equal(language[0](), "English");
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo change setting");
		redo();
		assert.equal(setting[0](), 1);
		assert.equal(language[0](), "English");
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), "Redo change setting");
		redo();
		assert.equal(setting[0](), 2);
		assert.equal(language[0](), "日本語");
		assert.equal(undoDescription(), "設定の変更を取り消す");
		assert.equal(redoDescription(), null);
	});

	it("six", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const string = undoable("");
		const setString = function(what) {
			group( () => {
				string[1](what);
			}, "change string to " + what, "string");
		};
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		setString("a");
		assert.equal(undoDescription(), "undo change string to a");
		assert.equal(redoDescription(), null);
		setString("as");
		assert.equal(undoDescription(), "undo change string to as");
		assert.equal(redoDescription(), null);
		setString("asd");
		assert.equal(undoDescription(), "undo change string to asd");
		assert.equal(redoDescription(), null);
		setString("asdf");
		assert.equal(undoDescription(), "undo change string to asdf");
		assert.equal(redoDescription(), null);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change string to asdf");
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		redo();
		assert.equal(undoDescription(), "undo change string to asdf");
		assert.equal(redoDescription(), null);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		setString("asdf ");
		assert.equal(undoDescription(), "undo change string to asdf ");
		assert.equal(redoDescription(), null);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		setString("asdf z");
		assert.equal(undoDescription(), "undo change string to asdf z");
		assert.equal(redoDescription(), null);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		setString("asdf zx");
		assert.equal(undoDescription(), "undo change string to asdf zx");
		assert.equal(redoDescription(), null);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		setString("asdf zxc");
		assert.equal(undoDescription(), "undo change string to asdf zxc");
		assert.equal(redoDescription(), null);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		setString("asdf zxcv");
		assert.equal(undoDescription(), "undo change string to asdf zxcv");
		assert.equal(redoDescription(), null);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		undo();
		assert.equal(undoDescription(), "undo change string to asdf");
		assert.equal(redoDescription(), "redo change string to asdf zxcv");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change string to asdf");
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
	});

	it("seven", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		const setSettingOne = function(what) {
			group( () => {
				setting1[1](what);
			}, "change setting one to " + what, "setting1");
		};
		const setSettingTwo = function(what) {
			group( () => {
				setting2[1](what);
			}, "change setting two to " + what, "setting2");
		};
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		setSettingOne(1);
		assert.equal(undoDescription(), "undo change setting one to 1");
		assert.equal(redoDescription(), null);
		setSettingTwo(2);
		assert.equal(undoDescription(), "undo change setting two to 2");
		assert.equal(redoDescription(), null);
		setSettingOne(3);
		assert.equal(undoDescription(), "undo change setting one to 3");
		assert.equal(redoDescription(), null);
		setSettingOne(4);
		assert.equal(undoDescription(), "undo change setting one to 4");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), "undo change setting two to 2");
		assert.equal(redoDescription(), "redo change setting one to 4");
		undo();
		assert.equal(undoDescription(), "undo change setting one to 1");
		assert.equal(redoDescription(), "redo change setting two to 2");
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting one to 1");
		redo();
		assert.equal(undoDescription(), "undo change setting one to 1");
		assert.equal(redoDescription(), "redo change setting two to 2");
		redo();
		assert.equal(undoDescription(), "undo change setting two to 2");
		assert.equal(redoDescription(), "redo change setting one to 4");
		redo();
		assert.equal(undoDescription(), "undo change setting one to 4");
		assert.equal(redoDescription(), null);
	});

	it("eight", function() { // Should not coalesce after undo, but this one ensures that input stack is also reset
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, true);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		appearance[1](4); // interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 4);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 4);
		group( () => {
			setting[1](3);
			appearance[1](3);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 3);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 4);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
	});

	it("eight alternate", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		appearance[1](4); // not interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 4);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		group( () => {
			setting[1](3);
			appearance[1](3);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 3);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
	});

	it("nine", function() { // null changeDescription will produce null undoDescription
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, null, true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, null, true); // should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
	});

	it("ten", function() { // undefined changeDescription will produce null undoDescription
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, undefined, true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, undefined, true); // should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
	});

	it("group false change description is valid and coalesces if key is true", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, false, true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo false");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, false, true); // should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo false");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo false");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
	});

	it("undoable false change description is valid and coalesces if key is true", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, false, true);
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo false");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		setting[1](2); // should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo false");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo false");
		assert.equal(setting[0](), 0);
	});

	it("group zero change description is valid and coalesces if key is true", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, 0, true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, 0, true); // should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo 0");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
	});

	it("maxCount and undoDescription", function() {
		const maxCount = 2;
		const undoLocalizer = function(description) {
			return "Undo " + description;
		} 
		const { undoable, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, null, maxCount);
		const setting = undoable(0, "change setting");
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		setting[1](3);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		undo(); // extra does nothing
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 3);
		redo(); // extra does nothing
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 3);
	});

	it("maxCount and redoDescription", function() {
		const maxCount = 2;
		const redoLocalizer = function(description) {
			return "Redo " + description;
		} 
		const { undoable, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(null, redoLocalizer, maxCount);
		const setting = undoable(0, "change setting");
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		setting[1](3);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo change setting");
		assert.equal(setting[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo change setting");
		assert.equal(setting[0](), 1);
		undo(); // extra does nothing
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo change setting");
		assert.equal(setting[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo change setting");
		assert.equal(setting[0](), 2);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 3);
		redo(); // extra does nothing
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 3);
	});

	it("maxCount and undoDescription and redoDescription", function() {
		const maxCount = 2;
		const undoLocalizer = function(description) {
			return "Undo " + description;
		}
		const redoLocalizer = function(description) {
			return "Redo " + description;
		}
		const { undoable, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer, maxCount);
		const setting = undoable(0, "change setting");
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		setting[1](3);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), "Redo change setting");
		assert.equal(setting[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo change setting");
		assert.equal(setting[0](), 1);
		undo(); // extra does nothing
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo change setting");
		assert.equal(setting[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), "Redo change setting");
		assert.equal(setting[0](), 2);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 3);
		redo(); // extra does nothing
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "Undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 3);
	});

	it("null undoLocalizer", function() {
		const undoLocalizer = null;
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting[1](1);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		redo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
	});

	it("null redoLocalizer", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = null;
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting[1](1);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		redo();
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
	});

	it("undefined undoLocalizer", function() { // not enumerable so have to set and check
		const undoLocalizer = undefined;
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting[1](1);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		redo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
	});

	it("undefined redoLocalizer", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = undefined;
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting[1](1);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		redo();
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
	});

	it("undoable zero change description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, 0);
		setting[1](1);
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo 0");
		redo();
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
	});

	it("undoable null description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, null);
		setting[1](1);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		redo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
	});

	it("undoable undefined description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, undefined);
		setting[1](1);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		redo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
	});

	it("undoable false description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, false);
		setting[1](1);
		assert.equal(undoDescription(), "Undo false");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo false");
		redo();
		assert.equal(undoDescription(), "Undo false");
		assert.equal(redoDescription(), null);
	});

	it("undoable non-falsy description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "0");
		setting[1](1);
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo 0");
		redo();
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
	});

	it("group overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting[1](1);
		}, "change setting override");
		assert.equal(undoDescription(), "Undo change setting override");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo change setting override");
		redo();
		assert.equal(undoDescription(), "Undo change setting override");
		assert.equal(redoDescription(), null);
	});

	it("group zero description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting[1](1);
		}, 0);
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo 0");
		redo();
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
	});

	it("group null description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting[1](1);
		}, null);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		redo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
	});

	it("group false description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting[1](1);
		}, false);
		assert.equal(undoDescription(), "Undo false");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo false");
		redo();
		assert.equal(undoDescription(), "Undo false");
		assert.equal(redoDescription(), null);
	});

	it("group non-falsy description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting[1](1);
		}, "0");
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo 0");
		redo();
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
	});

	it("group undefined description overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting[1](1);
		});
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		redo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
	});

	it("group zero description is valid and overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting[1](1);
		}, 0);
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo 0");
		redo();
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
	});

	it("group null description overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting[1](1);
		}, null);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		redo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
	});

	it("group false description is valid and overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting[1](1);
		}, false);
		assert.equal(undoDescription(), "Undo false");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo false");
		redo();
		assert.equal(undoDescription(), "Undo false");
		assert.equal(redoDescription(), null);
	});

	it("group non-falsy description overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting[1](1);
		}, "0");
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
		undo();
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "Redo 0");
		redo();
		assert.equal(undoDescription(), "Undo 0");
		assert.equal(redoDescription(), null);
	});
});