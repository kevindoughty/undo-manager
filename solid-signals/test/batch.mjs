import { assert } from "chai";
import { batch, createEffect, createMemo } from "solid-js";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("BATCH", function() {
	it("wontfix", function() { // Can't fix. Expected behavior. Don't use batch, use group. See first in test/group.js
		const { undoable, undo, canUndo, canRedo } = new UndoManager();
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting1[0](), 0);
		assert.equal(setting2[0](), 0);
		batch(()=> {
			setting1[1](1);
			setting2[1](2);
		});
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting1[0](), 1);
		assert.equal(setting2[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting1[0](), 1);
		assert.equal(setting2[0](), 0);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting1[0](), 0);
		assert.equal(setting2[0](), 0);
	});

	it("batching multiple undos", function() { // different // currently createEffect and createMemo can't be tested
		// const { undoable, preservable, group, undo, redo } = new UndoManager();
		// const setting = undoable(1);
		// let sum = 0;
		// createEffect(() => sum += setting[0]());
		// setting[1](2);
		// setting[1](3);
		// batch( () => { // must use batch, not group
		// 	undo();
		// 	undo();
		// });
		// assert.equal(setting[0](), 1);
		// assert.equal(sum, 7);
		assert.equal("not applicable", "not applicable");
	});

	it("not batching multiple undos", function() { // different // currently createEffect and createMemo can't be tested
		// const { undoable, preservable, group, undo, redo } = new UndoManager();
		// const setting = undoable(1);
		// let sum = 0;
		// createEffect(() => sum += setting[0]());
		// setting[1](2);
		// setting[1](3);
		// undo();
		// undo();
		// assert.equal(setting[0](), 1);
		// assert.equal(sum, 9);
		assert.equal("currently createEffect and createMemo can't be tested", "currently createEffect and createMemo can't be tested");
	});
});