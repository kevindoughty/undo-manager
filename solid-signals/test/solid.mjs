import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { createMemo, createEffect, createSignal, createRoot } from "solid-js";

function isFunction(w) {
	return w && {}.toString.call(w) === "[object Function]";
}

describe("SOLID", function() {
	it("Constructor exists", () => {
		assert.exists(UndoManager);
	});
	it("Instance exists", () => {
		const manager = new UndoManager();
		assert.exists(manager);
	});
	it("undoable exists", function() {
		const { undoable } = new UndoManager();
		assert(isFunction(undoable));
	});
	it("preservable exists", function() {
		const { preservable } = new UndoManager();
		assert(isFunction(preservable));
	});
	it("group exists", function() {
		const { group } = new UndoManager();
		assert(isFunction(group));
	});
	it("undo exists", function() {
		const { undo } = new UndoManager();
		assert(isFunction(undo));
	});
	it("redo exists", function() {
		const { redo } = new UndoManager();
		assert(isFunction(redo));
	});
	it("canUndo exists", function() {
		const { canUndo } = new UndoManager();
		assert(typeof canUndo !== "undefined" && canUndo !== undefined && canUndo !== null);
		//assert(canUndo.value === false);
	});
	it("canRedo exists", function() {
		const { canRedo } = new UndoManager();
		assert(typeof canRedo !== "undefined" && canRedo !== undefined && canRedo !== null);
		//assert(canRedo.value === false);
	});
	it("undoDescription exists", function() {
		const { undoDescription } = new UndoManager();
		assert(typeof undoDescription !== "undefined" && undoDescription !== undefined && undoDescription !== null);
		//assert.equal(undoDescription.value, null);
	});
	it("redoDescription exists", function() {
		const { redoDescription } = new UndoManager();
		assert(typeof redoDescription !== "undefined" && redoDescription !== undefined && redoDescription !== null);
		//assert.equal(redoDescription.value, null); 
	});


	it("canUndo initial", function() {
		const { canUndo } = new UndoManager();
		assert(canUndo() === false);
	});
	it("canRedo initial", function() {
		const { canRedo } = new UndoManager();
		assert(canRedo() === false);
	});
	it("undoDescription initial", function() {
		const { undoDescription } = new UndoManager();
		assert.equal(undoDescription(), null);
	});
	it("redoDescription initial", function() {
		const { redoDescription } = new UndoManager();
		assert.equal(redoDescription(), null); 
	});

	it("signal create", function() {
		const setting = createSignal(1);
		assert.equal(setting[0](), 1);
	});

	it("signal set", function() {
		const setting = createSignal(1);
		setting[1](2);
		assert.equal(setting[0](), 2);
	});

	it("signal update", function() {
		const setting = createSignal(1);
		setting[1]((value)=>value+10);
		assert.equal(setting[0](), 11);
	});

	it("signal set derived", function() {
		const setting = createSignal(1);
		const derived = () => setting[0]() + 10;
		assert.equal(derived(), 11);
		setting[1](2);
		assert.equal(derived(), 12);
	});

	it("signal update derived", function() {
		const setting = createSignal(1);
		const derived = () => setting[0]() + 10;
		const updater = (value) => value + 1;
		assert.equal(derived(), 11);
		setting[1](updater);
		assert.equal(derived(), 12);
	});

	// it("memo docs example", function() { // different // currently createEffect and createMemo can't be tested
	// 	const [count, setCount] = createSignal(0);
	// 	const isEven = createMemo(() => count() % 2 === 0);
	// 	const one = isEven();
	// 	assert.equal(one, true);
	// 	setCount(3);
	// 	const two = isEven();
	// 	assert.equal(two, false);
	// });

	// it("memo docs example within createRoot", function(done) { // different // currently createEffect and createMemo can't be tested
	// 	createRoot(dispose => {
	// 		const [count, setCount] = createSignal(0);
	// 		const isEven = createMemo(() => count() % 2 === 0);
	// 		const one = isEven();
	// 		assert.equal(one, true);
	// 		setCount(3);
	// 		const two = isEven();
	// 		setTimeout(() => {
	// 			assert.equal(two, false);
	// 			dispose();
	// 			done();
	// 		});
	// 	});
	// });

	// it("signal set memo", function() { // different // currently createEffect and createMemo can't be tested
	// 	const [getSetting, setSetting] = createSignal(1);
	// 	const memo = createMemo(() => getSetting() + 10);
	// 	assert.equal(getSetting(), 1);
	// 	assert.equal(memo(), 11);
	// 	setSetting(2);
	// 	assert.equal(getSetting(), 2);
	// 	assert.equal(memo(), 12);
	// });

	// it("signal update memo", function() { // different // currently createEffect and createMemo can't be tested
	// 	const [getSetting, setSetting] = createSignal(1);
	// 	const memo = createMemo(() => getSetting() + 10);
	// 	const updater = (value) => value + 1;
	// 	assert.equal(memo(), 11);
	// 	setSetting(updater);
	// 	assert.equal(memo(), 12);
	// });

	// it("signal set effect", function() { // different // currently createEffect and createMemo can't be tested
	// 	const [getSetting, setSetting] = createSignal(1);
	// 	let value = 0;
	// 	let count = 0;
	// 	createEffect(() => {
	// 		value = getSetting() + 10;
	// 		count++;
	// 	});
	// 	assert.equal(count, 1);
	// 	assert.equal(value, 11);
	// 	setSetting(2);
	// 	assert.equal(count, 2);
	// 	assert.equal(value, 12);
	// });

	// it("signal update effect", function() { // different // currently createEffect and createMemo can't be tested
	// 	const [getSetting, setSetting] = createSignal(1);
	// 	let value = 0;
	// 	let count = 0;
	// 	createEffect(() => {
	// 		value = getSetting() + 10;
	// 		count++;
	// 	});
	// 	const updater = (value) => value + 1;
	// 	assert.equal(count, 1);
	// 	assert.equal(value, 11);
	// 	setSetting(updater);
	// 	assert.equal(count, 2);
	// 	assert.equal(value, 12);
	// });

	it("undoable create", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		assert.equal(setting[0](), 1);
	});

	it("undoable set", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		setting[1](2);
		assert.equal(setting[0](), 2);
	});

	it("preservable create", function() {
		const { preservable } = new UndoManager();
		const appearance = preservable(1);
		assert.equal(appearance[0](), 1);
	});

	it("preservable set", function() {
		const { preservable } = new UndoManager();
		const appearance = preservable(1);
		appearance[1](2);
		assert.equal(appearance[0](), 2);
	});

	// it("undoable create effect", function() { // different // currently createEffect and createMemo can't be tested
	// 	const { undoable } = new UndoManager();
	// 	const setting = undoable(1);
	// 	let value = 0;
	// 	createEffect(
	// 		() => {
	// 			value = setting[0]() + 10;
	// 		}
	// 	);
	// 	assert.equal(value, 11);
	// });

	// it("undoable set effect before", function() { // different // currently createEffect and createMemo can't be tested
	// 	const { undoable } = new UndoManager();
	// 	const setting = undoable(1);
	// 	setting[1](2);
	// 	let value = 0;
	// 	createEffect(
	// 		() => {
	// 			value = setting[0]() + 10;
	// 		}
	// 	);
	// 	assert.equal(value, 12);
	// });

	// it("undoable set effect after", function() { // different // currently createEffect and createMemo can't be tested
	// 	const { undoable } = new UndoManager();
	// 	const setting = undoable(1);
	// 	let value = 0;
	// 	createEffect(
	// 		() => {
	// 			value = setting[0]() + 10;
	// 		}
	// 	);
	// 	setting[1](2);
	// 	assert.equal(value, 12);
	// });

	it("undoable create memo", function() { // different // currently createEffect and createMemo can't be tested, here createMemo only works on creation
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		let value = 0;
		createMemo(
			() => {
				value = setting[0]() + 10;
			}
		);
		assert.equal(value, 11);
	});

	it("undoable set memo before", function() { // different // currently createEffect and createMemo can't be tested, here createMemo only works on creation
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		setting[1](2);
		let value = 0;
		createMemo(
			() => {
				value = setting[0]() + 10;
			}
		);
		assert.equal(value, 12);
	});

	// it("undoable set memo after", function() { // different // currently createEffect and createMemo can't be tested
	// 	const { undoable } = new UndoManager();
	// 	const setting = undoable(1);
	// 	let value = 0;
	// 	createMemo(
	// 		() => {
	// 			value = setting[0]() + 10;
	// 		}
	// 	);
	// 	setting[1](2);
	// 	assert.equal(value, 12);
	// });

	it("undoable canUndo initial", function() {
		const { undoable, canUndo } = new UndoManager();
		const setting = undoable(1);
		assert.equal(canUndo(), false);
	});
	it("undoable canRedo initial", function() {
		const { undoable, canRedo } = new UndoManager();
		const setting = undoable(1);
		assert.equal(canRedo(), false);
	});

	it("undoable canUndo first", function() {
		const { undoable, canUndo } = new UndoManager();
		const setting = undoable(1);
		setting[1](2);
		assert.equal(canUndo(), true);
	});

	it("undoable undo", function() {
		const { undoable, undo } = new UndoManager();
		const setting = undoable(1);
		setting[1](2);
		undo();
		assert.equal(setting[0](), 1);
	});

	it("undoable canRedo first", function() {
		const { undoable, undo, canRedo } = new UndoManager();
		const setting = undoable(1);
		setting[1](2);
		undo();
		assert.equal(canRedo(), true);
	});

	it("undoable update", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		const updater = (value) => {
			return value + 10;
		};
		assert.equal(setting[0](), 1);
		setting[1](updater);
		assert.equal(setting[0](), 11);
	});

	it("undoable update undo", function() {
		const { undoable, undo } = new UndoManager();
		const setting = undoable(1);
		const updater = (value) => {
			return value + 10;
		};
		assert.equal(setting[0](), 1);
		setting[1](updater);
		assert.equal(setting[0](), 11);
		undo();
		assert.equal(setting[0](), 1);
	});

	it("preservable update", function() {
		const { undoable, preservable } = new UndoManager();
		const setting = undoable("a");
		const appearance = preservable(1);
		const updater = (value) => {
			return value + 10;
		};
		assert.equal(appearance[0](), 1);
		appearance[1](updater);
		assert.equal(appearance[0](), 11);
	});

	it("preservable update undo", function() {
		const { undoable, preservable, undo } = new UndoManager();
		const setting = undoable("a");
		const appearance = preservable(1);
		const updater = (value) => {
			return value + 10;
		};
		assert.equal(appearance[0](), 1);
		setting[1]("b");
		appearance[1](updater);
		assert.equal(appearance[0](), 11);
		undo();
		assert.equal(appearance[0](), 1);
	});

	it("undoable signal options equals, set", function() {
		const { undoable, preservable, undo, canUndo } = new UndoManager();
		const setting = undoable({
			key:0,
			value:"zero"
		}, "change setting", false, {
			equals: (a,b) => a.key === b.key
		});
		assert.equal(canUndo(), false);
		assert.equal(setting[0]().value, "zero");
		setting[1]({
			key:0,
			value:"one"
		});
		assert.equal(canUndo(), false);
		assert.equal(setting[0]().value, "zero");
		setting[1]({
			key:1,
			value:"two"
		});
		assert.equal(canUndo(), true);
		assert.equal(setting[0]().value, "two");
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting[0]().value, "zero");
	});

	it("undoable signal options equals, update", function() {
		const { undoable, preservable, undo, canUndo } = new UndoManager();
		const setting = undoable({
			key:0,
			value:"zero"
		}, "change setting", false, {
			equals: (a,b) => a.key === b.key
		});
		assert.equal(canUndo(), false);
		assert.equal(setting[0]().value, "zero");
		setting[1](prev => {
			return {
				key:0,
				value:"one"
			};
		});
		assert.equal(canUndo(), false);
		assert.equal(setting[0]().value, "zero");
		setting[1](prev => {
			return {
				key:1,
				value:"two"
			};
		});
		assert.equal(canUndo(), true);
		assert.equal(setting[0]().value, "two");
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting[0]().value, "zero");
	});

	it("preservable signal options equals, set, coalescing", function() {
		const { undoable, preservable, undo, canUndo } = new UndoManager();
		const setting = undoable(0, "change setting", true);
		const appearance = preservable({
			key:0,
			value:"zero"
		}, true, {
			equals: (a,b) => a.key === b.key
		});
		assert.equal(canUndo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0]().value, "zero");
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0]().value, "zero");
		appearance[1](prev => { // does not interrupt coalescing
			return {
				key:0,
				value:"one"
			};
		});
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0]().value, "zero");
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0]().value, "zero");
		setting[1](1);
		appearance[1](prev => { // does interrupt coalescing
			return {
				key:1,
				value:"two"
			};
		});
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0]().value, "two");
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0]().value, "two");
		undo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0]().value, "two");
	});

	it("preservable signal options equals, update, coalescing", function() {
		const { undoable, preservable, undo, canUndo } = new UndoManager();
		const setting = undoable(0, "change setting", true);
		const appearance = preservable({
			key:0,
			value:"zero"
		}, true, {
			equals: (a,b) => a.key === b.key
		});
		assert.equal(canUndo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0]().value, "zero");
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0]().value, "zero");
		appearance[1]({ // does not interrupt coalescing
			key:0,
			value:"one"
		});
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0]().value, "zero");
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0]().value, "zero");
		setting[1](1);
		appearance[1]({ // does interrupt coalescing
			key:1,
			value:"two"
		});
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0]().value, "two");
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0]().value, "two");
		undo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0]().value, "two");
	});

	it("undoable update previous value", function() {
		const { undoable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable({
			key:0,
			value:"zero"
		}, "change setting", false, {
			equals: (a,b) => a.key === b.key
		});
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0]().value, "zero");
		assert.equal(setting[0]().key, 0);
		setting[1](prev => {
			return {
				key:prev.key + 1,
				value:"one"
			};
		});
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0]().value, "one");
		assert.equal(setting[0]().key, 1);
		setting[1](prev => {
			return {
				key:prev.key + 1,
				value:"two"
			};
		});
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0]().value, "two");
		assert.equal(setting[0]().key, 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0]().value, "one");
		assert.equal(setting[0]().key, 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0]().value, "two");
		assert.equal(setting[0]().key, 2);
	});

	it("preservable update previous value", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable({
			key:0,
			value:"zero"
		}, true, {
			equals: (a,b) => a.key === b.key
		});
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0]().value, "zero");
		assert.equal(appearance[0]().key, 0);
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0]().value, "zero");
		assert.equal(appearance[0]().key, 0);
		appearance[1](prev => {
			return {
				key:prev.key + 1,
				value:"one"
			};
		});
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0]().value, "one");
		assert.equal(appearance[0]().key, 1);
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0]().value, "one");
		assert.equal(appearance[0]().key, 1);
		undo()
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0]().value, "one");
		assert.equal(appearance[0]().key, 1);
		undo()
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0]().value, "zero");
		assert.equal(appearance[0]().key, 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0]().value, "zero");
		assert.equal(appearance[0]().key, 0);
	});

	it("group coalescing update previous value", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable({
			key:0,
			value:0
		}, undefined, undefined, {
			equals: (a,b) => a.key === b.key
		});
		const appearance = preservable({
			key:0,
			value:"zero"
		}, undefined, {
			equals: (a,b) => a.key === b.key
		});
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0]().value, 0);
		assert.equal(setting[0]().key, 0);
		assert.equal(appearance[0]().value, "zero");
		assert.equal(appearance[0]().key, 0);
		group(()=>{
			setting[1](prev => {
				return {
					key:prev.key + 1,
					value:1
				};
			});
			appearance[1](prev => {
				return {
					key:prev.key + 1,
					value:"one"
				};
			});
		}, "change setting and more", true); // coalescing
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0]().value, 1);
		assert.equal(setting[0]().key, 1);
		assert.equal(appearance[0]().value, "one");
		assert.equal(appearance[0]().key, 1);
		group(()=>{
			setting[1](prev => {
				return {
					key:prev.key + 1,
					value:2
				};
			});
			appearance[1](prev => {
				return {
					key:prev.key + 1,
					value:"two"
				};
			});
		}, "change setting and more", true); // coalescing
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0]().value, 2);
		assert.equal(setting[0]().key, 2);
		assert.equal(appearance[0]().value, "two");
		assert.equal(appearance[0]().key, 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0]().value, 0);
		assert.equal(setting[0]().key, 0);
		assert.equal(appearance[0]().value, "zero");
		assert.equal(appearance[0]().key, 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0]().value, 2);
		assert.equal(setting[0]().key, 2);
		assert.equal(appearance[0]().value, "two");
		assert.equal(appearance[0]().key, 2);
	});

	it("signals support passeing undefined to setter", function() {
		const setting = createSignal(1);
		assert.equal(setting[0](), 1);
		setting[1]();
		assert.equal(setting[0](), undefined);
		setting[1](2);
		assert.equal(setting[0](), 2);
		setting[1](undefined);
		assert.equal(setting[0](), undefined);
	});

	it("undoables support passing undefined to setter", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		assert.equal(setting[0](), 1);
		setting[1]();
		assert.equal(setting[0](), undefined);
		setting[1](2);
		assert.equal(setting[0](), 2);
		setting[1](undefined);
		assert.equal(setting[0](), undefined);
	});

	it("preservables support passing undefined to setter", function() {
		const { preservable } = new UndoManager();
		const appearance = preservable(1);
		assert.equal(appearance[0](), 1);
		appearance[1]();
		assert.equal(appearance[0](), undefined);
		appearance[1](2);
		assert.equal(appearance[0](), 2);
		appearance[1](undefined);
		assert.equal(appearance[0](), undefined);
	});

	it("undoables support passing undefined to setter, undo", function() {
		const { undoable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(1);
		assert.equal(setting[0](), 1);
		setting[1]();
		assert.equal(setting[0](), undefined);
		setting[1](2);
		assert.equal(setting[0](), 2);
		setting[1](undefined);
		assert.equal(setting[0](), undefined);
		undo();
		assert.equal(setting[0](), 2);
		undo();
		assert.equal(setting[0](), undefined);
		undo();
		assert.equal(setting[0](), 1);
		assert.equal(canUndo(), false);
		redo();
		assert.equal(setting[0](), undefined);
		redo();
		assert.equal(setting[0](), 2);
		redo();
		assert.equal(setting[0](), undefined);
		assert.equal(canRedo(), false);
	});
});