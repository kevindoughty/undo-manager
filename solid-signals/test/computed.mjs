import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";

describe("DERIVED", function() {
	it("one", function() {
		const { undoable, preservable } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		const derived = () => {
			return setting[0]() + appearance[0]();
		};
		assert.equal(derived(), 0);
		setting[1](1);
		assert.equal(derived(), 1);
		appearance[1](2);
		assert.equal(derived(), 3);
	});
});