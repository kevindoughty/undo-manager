import { assert } from "chai";
import { createEffect } from "solid-js";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("USAGE", function() {
	it("conditional", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		}
		const redoLocalizer = function(description) {
			return "Undo " + description;
		}
		const { undoable, preservable, group, undoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		const setter = function(a,b,c) {
			const message = "change setting to " + a;
			const coalescer = Symbol();
			group( () => {
				setting[1](a);
				appearance1[1](b);
			}, message, coalescer);
			if (c !== undefined) {
				group( () => {
					appearance2[1](c);
				}, null, coalescer); // does not erase undo description
			}
		};
		setter(1,1);
		assert.equal(setting[0](), 1);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 0);
		assert.equal(undoDescription(), "Undo change setting to 1");
		
		setter(2,2,2);
		assert.equal(setting[0](), 2);
		assert.equal(appearance1[0](), 2);
		assert.equal(appearance2[0](), 2);
		assert.equal(undoDescription(), "Undo change setting to 2");

		setter(3,3);
		assert.equal(setting[0](), 3);
		assert.equal(appearance1[0](), 3);
		assert.equal(appearance2[0](), 2);
		assert.equal(undoDescription(), "Undo change setting to 3");		
	});

	it("extra", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);

		const volume = undoable(0);
		const volumeSymbol = Symbol();
		const crankVolume = function(value) {
			const description = "crank the volume to " +value;
			group( () => {
				volume[1](value);	
			}, description, volumeSymbol);
		}
		crankVolume(10);
		assert.equal(undoDescription(), "Undo crank the volume to 10");

		group( () => {
			appearance[1](-1);
			setting[1](-1);
			group( () => {
				setting[1](-2);
				appearance[1](-2);
			}, "ignored", Symbol());
		}, null, volumeSymbol); // does not null out undo description
		assert.equal(undoDescription(), "Undo crank the volume to 10");
		crankVolume(11); // coalesces
		assert.equal(undoDescription(), "Undo crank the volume to 11");
		undo();
		assert.equal(volume[0](), 0);
		assert.equal(redoDescription(), "Redo crank the volume to 11");

		appearance[1](-3); // lost
		redo();
		assert.equal(appearance[0](), -2);
		appearance[1](-4); // lost
		undo();
		redo();
		assert.equal(appearance[0](), -2);

		const color = () => {
			if (volume[0]() < 0) return "black";
			if (volume[0]() < 6) return "green";
			if (volume[0]() < 9) return "yellow";
			if (volume[0]() < 11) return "red";
			return "purple";
		};
		assert.equal(color(), "purple");
		createEffect( () => {
			assert.equal(volume[0](), 11, "no way don't touch the volume");
		});
		// undo() // don't do it
	});
});