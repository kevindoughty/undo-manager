import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { createEffect, createSignal } from "solid-js";


describe("EFFECT", function() {
	it("one", function() { // different // currently createEffect and createMemo can't be tested
		// const { undoable, preservable, undo, redo } = new UndoManager();
		// const setting = undoable(0);
		// const appearance = preservable(0);
		// const derived = () => {
		// 	return setting[0]() + appearance[0]();
		// };
		// let count = 0;
		// let summation = 0;
		// createEffect(() => {
		// 	count++;
		// 	summation = summation + setting[0]();
		// });
		// assert.equal(count, 1);
		// assert.equal(summation, 0);
		// appearance[1](1);
		// assert.equal(count, 2);
		// assert.equal(summation, 1); // 0 + (0 + 1)
		// setting[1](10);
		// assert.equal(count, 3);
		// assert.equal(summation, 12); // 1 + (10 + 1)
		// appearance[1](100);
		// assert.equal(count, 4);
		// assert.equal(summation, 122); // 12 + (10 + 100)
		// setting[1](1000);
		// assert.equal(count, 5);
		// assert.equal(summation, 1222); // 122 + (1000 + 100)
		// appearance[1](10000);
		// assert.equal(count, 6);
		// assert.equal(summation, 12222); // 1222 + (1000 + 10000)
		// undo(); // Changes in undo are grouped.
		// assert.equal(count, 7);
		// assert.equal(summation, 12332); // 12222 + (10 + 100)
		// undo(); // Changes in undo are grouped.
		// assert.equal(count, 8);
		// assert.equal(summation, 12333); // 12332 + (0 + 1)
		// redo(); // Changes in redo are grouped.
		// assert.equal(count, 9);
		// assert.equal(summation, 12344); // 12333 + (10 + 1)
		// redo(); // Changes in redo are grouped.
		// assert.equal(count, 10);
		// assert.equal(summation, 13444); // 12344 + (1000 + 100)
		assert.equal("currently createEffect and createMemo can't be tested", "currently createEffect and createMemo can't be tested");
	});
});