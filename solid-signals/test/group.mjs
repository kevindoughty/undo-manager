import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("GROUP", function() {
	it("one", function() { // Compare to first in test/batch.js
		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting1[0](), 0);
		assert.equal(setting2[0](), 0);
		group(()=> {
			setting1[1](1);
			setting2[1](2);
		});
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting1[0](), 1);
		assert.equal(setting2[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting1[0](), 0);
		assert.equal(setting2[0](), 0);
	});
	
	it("two", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		group(()=> {
			setting[1](1);
			appearance[1](1);
		});
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group(()=> {
			setting[1](2);
			appearance[1](2);
		});
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		undo(); // one extra should have no effect
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		redo(); // one extra should have no effect
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("three", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		group(()=> {
			setting[1](1);
			appearance[1](1);
		});
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		appearance[1](3);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 3);
		group(()=> {
			setting[1](2);
			appearance[1](2);
		});
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 3);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		undo(); // one extra should have no effect
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		redo(); // one extra should have no effect
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("four", function() { // using coalesce key
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, null, "setting"); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", "setting"); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("five", function() { // coalesce key can be an object reference
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, null, setting); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", setting); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("six", function() { // does not coalesce if keys don't match
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, null, "setting"); // these two should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change not setting", "not setting"); // these two should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change not setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change not setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change not setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change not setting");
		assert.equal(redoDescription(), null); 
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("seven", function() { // can coalesce even if group signatures don't match
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		group( () => {
			setting1[1](1);
			appearance1[1](1);
		}, "change settings", "setting"); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change settings");
		assert.equal(redoDescription(), null);
		assert.equal(setting1[0](), 1);
		assert.equal(setting2[0](), 0);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 0);
		group( () => {
			setting2[1](2);
			appearance2[1](2);
		}, "change settings", "setting"); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change settings");
		assert.equal(redoDescription(), null);
		assert.equal(setting1[0](), 1);
		assert.equal(setting2[0](), 2);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change settings");
		assert.equal(setting1[0](), 0);
		assert.equal(setting2[0](), 0);
		assert.equal(appearance1[0](), 0);
		assert.equal(appearance2[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change settings");
		assert.equal(redoDescription(), null);
		assert.equal(setting1[0](), 1);
		assert.equal(setting2[0](), 2);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 2);
	});

	it("eight", function() { // setting undoable between coalescing groups prevents coalescing
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		setting[1](3); // this should prevent coalescing
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
	});

	it("nine", function() { // setting preservable between coalescing groups prevents coalescing
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, true);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		appearance[1](3); // interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 3);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 3);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("nine alternate", function() {
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		appearance[1](3); // not interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 3);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("ten", function() { // should not coalesce after redo
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, null, "setting"); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", "setting"); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		group( () => {
			setting[1](3);
			appearance[1](3);
		}, "change setting again", "setting"); // should not coalesce after redo
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting again");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 3);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting again");
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("eleven interrupting", function() { // should not coalesce after undo
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, true);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, null, "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		appearance[1](4); // interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 4);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, null, "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 4);
		group( () => {
			setting[1](3);
			appearance[1](3);
		}, null, "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 3);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 4);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
	});
	
	it("eleven alternate", function() {
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, null, "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		appearance[1](4); // not interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 4);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, null, "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		group( () => {
			setting[1](3);
			appearance[1](3);
		}, null, "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 3);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
	});

	it("twelve", function() { // undefined coalesce key will not coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "change setting"); // should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting"); // should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("thirteen", function() { // null coalesce key will not coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "change setting", null); // should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", null); // should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});
	
	it("fourteen", function() { // boolean false coalesce key will not coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "change setting", false); // should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", false); // should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("fifteen", function() { // boolean true coalesce key will use changeDescription to coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "change setting", true); // should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting", true); // should coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("sixteen", function() { // should not coalesce even with boolean true coalesce key if changeDescriptions don't match
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "change setting", true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "change setting again", true); // should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting again");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting again");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), null);
		assert.equal(redoDescription(), "redo change setting");
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(undoDescription(), "undo change setting");
		assert.equal(redoDescription(), "redo change setting again");
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(undoDescription(), "undo change setting again");
		assert.equal(redoDescription(), null);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("nested group call order one", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0);
		group( () => {
			setting[1](1);
			group( () => {
				setting[1](2);
			});
		});
		assert.equal(setting[0](), 2);
		assert.equal(canUndo(), true);
		undo();
		assert.equal(setting[0](), 0);
		assert.equal(canUndo(), false);
		
	});

	it("nested group call order two", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0);
		group( () => {
			group( () => {
				setting[1](1);
			});
			setting[1](2);
		});
		assert.equal(setting[0](), 2);
		assert.equal(canUndo(), true);
		undo();
		assert.equal(setting[0](), 0);
		assert.equal(canUndo(), false);
	});

	it("nesting outer group wins description", function() {
		const undoLocalizer = function(input) {
			return "Undo " + input;
		};
		const redoLocalizer = function(input) {
			return "Redo " + input;
		};
		const { undoable, group, undo, canUndo, undoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0,"change undoable");
		group( () => {
			group( () => {
				setting[1](2);
			}, "change inner");
			setting[1](1);
		}, "change outer");
		assert.equal(setting[0](), 1);
		assert.equal(canUndo(), true);
		assert.equal(undoDescription(), "Undo change outer");
		undo();
		assert.equal(setting[0](), 0);
		assert.equal(canUndo(), false);
		assert.equal(undoDescription(), null);
	});

	it("nesting outer group wins coalescing false", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, null, "undoable coalesce");
		group( () => {
			setting[1](1);
			group( () => {
				setting[1](2);
			}, null, "inner coalesce");
		}, null, false);
		group( () => {
			setting[1](3);
			group( () => {
				setting[1](4);
			}, null, "inner coalesce");
		}, null, false);
		assert.equal(setting[0](), 4);
		assert.equal(canUndo(), true);
		undo();
		assert.equal(setting[0](), 2);
		assert.equal(canUndo(), true);
		undo();
		assert.equal(setting[0](), 0);
		assert.equal(canUndo(), false);
	});

	it("nesting outer group wins coalescing true", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, null, false);
		group( () => {
			setting[1](1);
			group( () => {
				setting[1](2);
			}, null, false);
		}, null, "outer coalesce");
		group( () => {
			setting[1](3);
			group( () => {
				setting[1](4);
			}, null, false);
		}, null, "outer coalesce");
		assert.equal(setting[0](), 4);
		assert.equal(canUndo(), true);
		undo();
		assert.equal(setting[0](), 0);
		assert.equal(canUndo(), false);
	});

	it("nesting outer group wins coalescing false two", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, null, "undoable coalesce");
		group( () => {
			setting[1](1);
			group( () => {
				setting[1](2);
			}, null, "inner coalesce");
			group( () => {
				setting[1](3);
			}, null, "inner coalesce");
		}, null, false);
		group( () => {
			setting[1](4);
			group( () => {
				setting[1](5);
			}, null, "inner coalesce");
			group( () => {
				setting[1](6);
			}, null, "inner coalesce");
		}, null, false);
		assert.equal(setting[0](), 6);
		assert.equal(canUndo(), true);
		undo();
		assert.equal(setting[0](), 3);
		assert.equal(canUndo(), true);
		undo();
		assert.equal(setting[0](), 0);
		assert.equal(canUndo(), false);
	});

	it("nesting outer group wins coalescing true", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, null, false);
		group( () => {
			setting[1](1);
			group( () => {
				setting[1](2);
			}, null, false);
		}, null, "outer coalesce");
		group( () => {
			setting[1](3);
			group( () => {
				setting[1](4);
			}, null, false);
		}, null, "outer coalesce");
		assert.equal(setting[0](), 4);
		assert.equal(canUndo(), true);
		undo();
		assert.equal(setting[0](), 0);
		assert.equal(canUndo(), false);
	});

	it("nesting outer group wins coalescing true two", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, null, false);
		group( () => {
			setting[1](1);
			group( () => {
				setting[1](2);
			}, null, false);
			group( () => {
				setting[1](3);
			}, null, false);
		}, null, "outer coalesce");
		group( () => {
			setting[1](4);
			group( () => {
				setting[1](5);
			}, null, false);
			group( () => {
				setting[1](6);
			}, null, false);
		}, null, "outer coalesce");
		assert.equal(setting[0](), 6);
		assert.equal(canUndo(), true);
		undo();
		assert.equal(setting[0](), 0);
		assert.equal(canUndo(), false);
	});

	it("empty group does not register one", function() {
		const { group, canUndo } = new UndoManager();
		group( () => {} );
		assert.equal(canUndo(), false);
	});

	it("empty group does not register two", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, "test", true);
		setting[1](2);
		group( () => {
		}, "test", true);
		undo();
		assert.equal(canUndo(), false);
	});

	it("empty group does not register three", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, "test", true);
		setting[1](1);
		group( () => {
		});
		undo();
		assert.equal(canUndo(), false);
	});

	it("empty group does not register four", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0);
		setting[1](1);
		group( () => {
		}, "test", true);
		undo();
		assert.equal(canUndo(), false);
	});

	it("empty group does not register five", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0, true);
		group( () => {
			appearance[1](1);
		});
		assert.equal(canUndo(), false);
	});

	it("empty group does not register six", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0, false);
		group( () => {
			appearance[1](1);
		});
		assert.equal(canUndo(), false);
	});

	it("empty group does not register seven", function() {
		const { undoable, preservable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, "test", true);
		const appearance = preservable(1, true);
		setting[1](2);
		group( () => {
			appearance[1](3);
		});
		undo();
		assert.equal(canUndo(), false);
	});

	it("empty group does not register eight", function() {
		const { undoable, preservable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, "test", true);
		const appearance = preservable(1, false);
		setting[1](2);
		group( () => {
			appearance[1](3);
		});
		undo();
		assert.equal(canUndo(), false);
	});

	it("empty group does not register nine", function() {
		const { undoable, preservable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(1, true);
		setting[1](2);
		group( () => {
			appearance[1](3);
		}, "test", true);
		undo();
		assert.equal(canUndo(), false);
	});

	it("empty group does not register ten", function() {
		const { undoable, preservable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(1, false);
		setting[1](2);
		group( () => {
			appearance[1](3);
		}, "test", true);
		undo();
		assert.equal(canUndo(), false);
	});

	it("register after one", function() {
		const { undoable, preservable, group, undo, redo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(1);
		group( () => {
			appearance[1](2);
		}, "test", true);
		group( () => {
			setting[1](3);
		}, "test", true);
		undo();
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(appearance[0](), 2);
	});

	it("register after two", function() {
		const { undoable, preservable, group, undo, redo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(1);
		setting[1](2);
		group( () => {
			appearance[1](3);
		}, "test", true);
		group( () => {
			setting[1](4);
		}, "test", true);
		undo();
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(appearance[0](), 3);
	});
});