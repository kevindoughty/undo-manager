import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("UNDO", function() {
	it("calling undo in a group will silently fail", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		
		const setting = undoable(0, "change setting", true);
		setting[1](1);
		
		const appearance = preservable(0, true);
		appearance[1](2);

		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 2);
		group( () => {
			undo();
		}, "change setting", false);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 2);

	});

	it("calling redo in a group will silently fail", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		
		const setting = undoable(0, "change setting", true);
		setting[1](1);
		
		const appearance = preservable(0, true);
		appearance[1](2);

		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 2);
		group( () => {
			redo();
		}, "change setting", false);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 2);

	});
});