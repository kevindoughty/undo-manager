import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("README", function() {
	it("the documentation is accurate", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		
		const setting = undoable(0, "change setting", true);
		setting[1](1);
		
		const appearance = preservable(0, true);
		appearance[1](2);
		setting[1](3);
		appearance[1](4);

		group( () => {
			setting[1](5);
			appearance[1](6);
		}, "change setting and more", true);
		group( () => {
			setting[1](7);
			appearance[1](8);
		}, "change setting and more", true);
		
		undo();
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 4);
		undo();
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 2);
		
		redo();
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 2);

		assert.equal(canUndo(), true);

		assert.equal(canRedo(), true);

		assert.equal(undoDescription(), "Undo change setting");

		assert.equal(redoDescription(), "Redo change setting and more");

		const dont = undoable("just"); // just don't
		dont[1]("dont");
		undo();
		undo();// before it existed
		assert(dont[0](), "just");
		redo();
		redo();

		group( () => {
			appearance[1](9); // will be registered as the after change[0]() 
		}, "conceptual section changes", true);
		group( () => {
			setting[1](7); 
		}, "conceptual section changes", true); // coalesces
		undo();
		assert.equal(appearance[0](), 2); // coalescing group did not affect before[0]()
		redo();
		assert.equal(appearance[0](), 9);

		group( () => {
			appearance[1](10);
		}, "conceptual section changes", true); // does not coalesce
		assert.equal(appearance[0](), 10);
		undo();
		redo();
		assert.equal(appearance[0](), 9); // changes are lost

	});
});