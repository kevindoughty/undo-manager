import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";

function isFunction(w) {
	return w && {}.toString.call(w) === "[object Function]";
}

describe("API", function() {
	it("isFunction", function() {
		const test = function() {};
		assert(isFunction(test));
		assert(isFunction(function() {}));
		assert(isFunction(() => {}));
		assert(!isFunction({}));
		assert(!isFunction("[object Function]"));
	});

	it("undoable", function() {
		const { undoable } = new UndoManager();
		assert(isFunction(undoable));
	});
	it("preservable", function() {
		const { preservable } = new UndoManager();
		assert(isFunction(preservable));
	});
	it("group", function() {
		const { group } = new UndoManager();
		assert(isFunction(group));
	});
	it("undo", function() {
		const { undo } = new UndoManager();
		assert(isFunction(undo));
	});
	it("redo", function() {
		const { redo } = new UndoManager();
		assert(isFunction(redo));
	});
	it("canUndo", function() {
		const { canUndo } = new UndoManager();
		assert(typeof canUndo !== "undefined" && canUndo !== undefined && canUndo !== null);
		assert(canUndo() === false);
	});
	it("canRedo", function() {
		const { canRedo } = new UndoManager();
		assert(typeof canRedo !== "undefined" && canRedo !== undefined && canRedo !== null);
		assert(canRedo() === false);
	});
	it("undoDescription", function() {
		const { undoDescription } = new UndoManager();
		assert(typeof undoDescription !== "undefined" && undoDescription !== undefined && undoDescription !== null);
		assert.equal(undoDescription(), null);
	});
	it("redoDescription", function() {
		const { redoDescription } = new UndoManager();
		assert(typeof redoDescription !== "undefined" && redoDescription !== undefined && redoDescription !== null);
		assert.equal(redoDescription(), null); 
	});
	it("Object.keys", function() {
		const manager = new UndoManager();
		assert.equal(Object.keys(manager).length, 9);
	});
	it("Object.getOwnPropertyNames", function() {
		const manager = new UndoManager();
		assert.equal(Object.getOwnPropertyNames(manager).length, 9);
	});
});