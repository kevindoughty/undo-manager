import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("COALESCE", function() {
	it("one coalescing", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "setting", true); // these two should coalesce
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "setting", true); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("two coalescing", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance1[1](1);
		}, "setting", true); // these two should coalesce
		group( () => {
			setting[1](2);
			appearance2[1](2);
		}, "setting", true); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance1[0](), 0);
		assert.equal(appearance2[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 2);
	});

	it("three not coalescing", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance1[1](1);
		}, "setting style one", true); // these two should not coalesce
		group( () => {
			setting[1](2);
			appearance2[1](2);
		},  "setting style two", true); // these two should not coalesce
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 0);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance1[0](), 0);
		assert.equal(appearance2[0](), 0);
		redo();
		redo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 2);
	});

	it("four coalescing", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new UndoManager();
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		assert.equal(canUndo(), false);
		group( () => {
			setting1[1](1);
			appearance1[1](1);
		}, "setting", true); // these two should coalesce
		group( () => {
			setting2[1](2);
			appearance2[1](2);
		}, "setting", true); // these two should coalesce
		assert.equal(canUndo(), true);
		assert.equal(setting1[0](), 1);
		assert.equal(setting2[0](), 2);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting1[0](), 0);
		assert.equal(setting2[0](), 0);
		assert.equal(appearance1[0](), 0);
		assert.equal(appearance2[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(setting1[0](), 1);
		assert.equal(setting2[0](), 2);
		assert.equal(appearance1[0](), 1);
		assert.equal(appearance2[0](), 2);
	});

	it("five interrupting", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0, true);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "setting", true);
		appearance[1](3); // interrupting
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "setting", true);
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 3);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("five alternate not interrupting", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0, false);
		assert.equal(canUndo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "setting", true);
		appearance[1](3); // not interrupting
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "setting", true);
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("six prevent coalescing", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		group( () => {
			setting[1](1);
			appearance[1](1);
		}, "setting", true);
		setting[1](3); // this should prevent coalescing
		group( () => {
			setting[1](2);
			appearance[1](2);
		}, "setting", true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("seven interrupting", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0, true);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		group( () => {
			setting[1](1);
		}, "setting", true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1); // interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
		}, "setting", true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](3);
		}, "setting", true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
	});

	it("seven alternate not interrupting", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0, false);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		group( () => {
			setting[1](1);
		}, "setting", true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1); // not interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
		}, "setting", true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](3);
		}, "setting", true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
	});

	it("eight interrupting", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0, "change setting", true);
		const appearance = preservable(0, true);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1); // interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		setting[1](3);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
	});

	it("eight alternate not interrupting", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0, "change setting", true);
		const appearance = preservable(0, false);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1); // not interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		setting[1](3);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
	});

	it("nine interrupting", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0, true);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		group( () => {
			setting[1](1);
		}, Math.random() + "", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1); // interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
		}, Math.random() + "", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](3);
		}, Math.random() + "", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
	});

	it("nine alternate not interrupting", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0, false);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		group( () => {
			setting[1](1);
		}, Math.random() + "", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1); // not interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](2);
		}, Math.random() + "", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		group( () => {
			setting[1](3);
		}, Math.random() + "", "setting");
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
	});

	it("undoable coalesce true without description does coalesce", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0, null, true);
		const appearance = preservable(0, true);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1); // interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		setting[1](3);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
	});

	it("alternate undoable coalesce true without description does coalesce", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0, null, true);
		const appearance = preservable(0, false);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1); // not interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		setting[1](3);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
	});

	it("group coalesce true without description", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0, null, true);
		const appearance = preservable(0, true);
		const grouper = () => {
			setting[1](setting[0]() + 1);
		};
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		group(grouper, null, true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1); // interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group(grouper, null, true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		group(grouper, null, true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
	});

	it("alternate group coalesce true without description", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0, null, true);
		const appearance = preservable(0, false);
		const grouper = () => {
			setting[1](setting[0]() + 1);
		};
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		group(grouper, null, true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1); // not interrupting
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		group(grouper, null, true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		group(grouper, null, true);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		assert.equal(appearance[0](), 1);
	});

	it("eleven", function() {
		const { undoable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const setSetting = function(what) {
			group( () => {
				setting[1](what);
			}, null, "setting");
		};
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		setSetting(1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		setSetting(2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		setSetting(3);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 3);
	});

// 	it("twelve", function() { // preservables do not affect coalescing, when inside coalescing group (they do affect when outside)
// 		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		const appearance1 = preservable(0);
// 		const appearance2 = preservable(0);
// 		group( () => {
// 			setting[1](1);
// 			appearance1[1](1);
// 		}, null, "setting");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 			appearance2[1](2);
// 		}, null, "setting");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 	});

// 	it("thirteen", function() { // can coalesce anything with a coalescing key
// 		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager();
// 		const setting1 = undoable(0);
// 		const setting2 = undoable(0);
// 		const appearance = preservable(0);
// 		group( () => {
// 			setting1[1](1);
// 		}, null, "various");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting1[0](), 1);
// 		assert.equal(setting2[0](), 0);
// 		assert.equal(appearance[0](), 0);
// 		group( () => {
// 			appearance[1](1);
// 		}, null, "various");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting1[0](), 1);
// 		assert.equal(setting2[0](), 0);
// 		assert.equal(appearance[0](), 1);
// 		group( () => {
// 			setting2[1](1);
// 		}, null, "various");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting1[0](), 1);
// 		assert.equal(setting2[0](), 1);
// 		assert.equal(appearance[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting1[0](), 0);
// 		assert.equal(setting2[0](), 0);
// 		assert.equal(appearance[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting1[0](), 1);
// 		assert.equal(setting2[0](), 1);
// 		assert.equal(appearance[0](), 1);
// 	});

// 	it("fourteen", function() { // can coalesce anything with a coalescing key
// 		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager();
// 		const setting1 = undoable(0);
// 		const setting2 = undoable(0);
// 		const appearance = preservable(0);
// 		group( () => {
// 			setting1[1](1);
// 		}, null, "various");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting1[0](), 1);
// 		assert.equal(setting2[0](), 0);
// 		assert.equal(appearance[0](), 0);
// 		group( () => {
// 			appearance[1](1);
// 		}, null, "various");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting1[0](), 1);
// 		assert.equal(setting2[0](), 0);
// 		assert.equal(appearance[0](), 1);
// 		group( () => {
// 			setting2[1](1);
// 		}, null, "various");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting1[0](), 1);
// 		assert.equal(setting2[0](), 1);
// 		assert.equal(appearance[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting1[0](), 0);
// 		assert.equal(setting2[0](), 0);
// 		assert.equal(appearance[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting1[0](), 1);
// 		assert.equal(setting2[0](), 1);
// 		assert.equal(appearance[0](), 1);
// 	});

// 	it("fourteen", function() { // coalesce key can be an object reference // identical to group test number five
// 		const undoLocalizer = function(description) {
// 			return "undo " + description;
// 		};
// 		const redoLocalizer = function(description) {
// 			return "redo " + description;
// 		};
// 		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
// 		const setting = undoable(0);
// 		const appearance = preservable(0);
// 		assert.equal(canUndo(), false);
// 		group( () => {
// 			setting[1](1);
// 			appearance[1](1);
// 		}, null, setting); // these two should coalesce
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 1);
// 		assert.equal(appearance[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 			appearance[1](2);
// 		}, "change setting", setting); // these two should coalesce
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		assert.equal(appearance[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), "redo change setting");
// 		assert.equal(setting[0](), 0);
// 		assert.equal(appearance[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		assert.equal(appearance[0](), 2);
// 	});

// 	it("fourteen", function() { // coalesce key can be a Symbol
// 		const symbol = Symbol("asdf");
// 		const undoLocalizer = function(description) {
// 			return "undo " + description;
// 		};
// 		const redoLocalizer = function(description) {
// 			return "redo " + description;
// 		};
// 		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
// 		const setting = undoable(0);
// 		const appearance = preservable(0);
// 		assert.equal(canUndo(), false);
// 		group( () => {
// 			setting[1](1);
// 			appearance[1](1);
// 		}, null, symbol); // these two should coalesce
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 1);
// 		assert.equal(appearance[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 			appearance[1](2);
// 		}, "change setting", symbol); // these two should coalesce
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		assert.equal(appearance[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), "redo change setting");
// 		assert.equal(setting[0](), 0);
// 		assert.equal(appearance[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		assert.equal(appearance[0](), 2);
// 	});

// 	it("fifteen", function() { // coalesce key can be a Symbol, and coalesce both groups and individual undoables
// 		const symbol = Symbol("asdf");
// 		const undoLocalizer = function(description) {
// 			return "undo " + description;
// 		};
// 		const redoLocalizer = function(description) {
// 			return "redo " + description;
// 		};
// 		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
// 		const setting = undoable(0, null, symbol);
// 		const appearance = preservable(0);
// 		assert.equal(canUndo(), false);
// 		group( () => {
// 			setting[1](1);
// 			appearance[1](1);
// 		}, null, symbol);  // these three should coalesce
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 1);
// 		assert.equal(appearance[0](), 1);
// 		setting[1](3); // these three should coalesce
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 3);
// 		assert.equal(appearance[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 			appearance[1](2);
// 		}, "change setting", symbol); // these three should coalesce
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		assert.equal(appearance[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), "redo change setting");
// 		assert.equal(setting[0](), 0);
// 		assert.equal(appearance[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		assert.equal(appearance[0](), 2);
// 	});

// 	it("undoable coalesce string", function() {
// 		const { undoable, undo, redo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, null, "setting");
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 	});

// 	it("undoable coalesce symbol", function() {
// 		const symbol = Symbol("asdf");
// 		const { undoable, undo, redo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, null, symbol);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 	});

// 	it("undoable coalesce true and group true with no descriptions", function() {
// 		const { undoable, undo, group, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, null, true);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2); // should not coalesce
// 		}, null, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable coalesce true and group true with description", function() {
// 		const undoLocalizer = function(description) {
// 			return "Undo " + description;
// 		};
// 		const redoLocalizer = function(description) {
// 			return "Redo " + description;
// 		};
// 		const { undoable, undo, group, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
// 		const setting = undoable(0, null, true);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2); // should not coalesce
// 		}, "change setting", true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), "Redo change setting");
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable coalesce key and group coalesce key", function() {
// 		const { undoable, group, undo, redo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, null, "setting");
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group (() => {
// 			setting[1](2);
// 		}, null, "setting");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 	});

// 	it("undoable coalesce description and group coalesce description", function() {
// 		const { undoable, group, undo, redo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, "change setting", true);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group (() => {
// 			setting[1](2);
// 		}, "change setting", true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 	});

// 	it("not coalescing with group and descriptions", function() {
// 		const undoLocalizer = function(description) {
// 			return "Undo " + description;
// 		}
// 		const redoLocalizer = function(description) {
// 			return "Redo " + description;
// 		}
// 		const { undoable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer, 2);
// 		const setting = undoable(0, "change setting", true);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		group( () => {
// 			setting[1](3);
// 		}, "change setting", false);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 3);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), "Undo change setting");
// 		assert.equal(redoDescription(), "Redo change setting");
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), "Redo change setting");
// 		assert.equal(setting[0](), 0);
// 		undo(); // extra does nothing
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), "Redo change setting");
// 		assert.equal(setting[0](), 0);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), "Undo change setting");
// 		assert.equal(redoDescription(), "Redo change setting");
// 		assert.equal(setting[0](), 2);
// 		redo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 3);
// 		redo(); // extra does nothing
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo change setting");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 3);
// 	});

// 	it("zero description does coalesce", function() {
// 		const undoLocalizer = function(input) {
// 			return "Undo " + input;
// 		};
// 		const redoLocalizer = function(input) {
// 			return "Redo " + input;
// 		};
// 		const { undoable, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
// 		const setting = undoable(0, 0, true);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo 0");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo 0");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), "Redo 0");
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("non-falsy description does coalesce", function() {
// 		const undoLocalizer = function(input) {
// 			return "Undo " + input;
// 		};
// 		const redoLocalizer = function(input) {
// 			return "Redo " + input;
// 		};
// 		const { undoable, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
// 		const setting = undoable(0, "0", true);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo 0");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo 0");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), "Redo 0");
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group undefined coalescing overrides undoable and does not coalesce", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, "change setting", true);
// 		group( () => {
// 			setting[1](1);
// 		}, null);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, null);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group null coalescing overrides undoable and does not coalesce", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, "change setting", true);
// 		group( () => {
// 			setting[1](1);
// 		}, null, null);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, null, null);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group null coalescing overrides undoable", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, "change setting", true);
// 		group( () => {
// 			setting[1](1);
// 		}, null, null);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, null, null);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group false coalescing overrides undoable and does not coalesce", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, "change setting", true);
// 		group( () => {
// 			setting[1](1);
// 		}, null, false);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, null, false);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group zero coalescing overrides undoable and does coalesce", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		group( () => {
// 			setting[1](1);
// 		}, null, 0);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, null, 0);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group non-falsy coalescing overrides undoable", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		group( () => {
// 			setting[1](1);
// 		}, null, "0");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, null, "0");
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable coalesce true with null description does coalesce", function() {
// 		const { undoable, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, null, true);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable coalesce true with undefined description does coalesce", function() {
// 		const { undoable, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, undefined, true);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group coalesce true with null description can coalesce if callback is the same", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		const grouper = () => {
// 			setting[1](setting[0]() + 1);
// 		}
// 		group(grouper, null, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group(grouper, null, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group coalesce true with undefined description can coalesce if callback is the same", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		const grouper = () => {
// 			setting[1](setting[0]() + 1);
// 		}
// 		group(grouper, undefined, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group(grouper, undefined, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable coalesce true with undefined description does coalesce", function() {
// 		const { undoable, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, undefined, true);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group coalesce true with undefined description can coalesce if callback is the same", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		const grouper = () => {
// 			setting[1](setting[0]() + 1);
// 		};
// 		group(grouper, undefined, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group(grouper, undefined, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group coalesce true with null description does not coalesce if callback is not the same", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		group( () => {
// 			setting[1](1);
// 		}, null, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, null, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 	});

// 	it("group coalesce true with undefined description does not coalesce if callback is not the same", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		group( () => {
// 			setting[1](1);
// 		}, undefined, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, undefined, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 	});

// 	it("group true coalesce and false description does coalesce", function() {
// 		const undoLocalizer = function(input) {
// 			return "Undo " + input;
// 		};
// 		const redoLocalizer = function(input) {
// 			return "Redo " + input;
// 		};
// 		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
// 		const setting = undoable(0);
// 		const appearance = preservable(0);
// 		group( () => {
// 			setting[1](1);
// 			appearance[1](1);
// 		}, false, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo false");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 1);
// 		assert.equal(appearance[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 			appearance[1](2);
// 		}, false, true); // should coalesce
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo false");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		assert.equal(appearance[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), "Redo false");
// 		assert.equal(setting[0](), 0);
// 		assert.equal(appearance[0](), 0);
// 	});

// 	it("group true coalesce and zero description does coalesce", function() {
// 		const undoLocalizer = function(input) {
// 			return "Undo " + input;
// 		};
// 		const redoLocalizer = function(input) {
// 			return "Redo " + input;
// 		};
// 		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
// 		const setting = undoable(0);
// 		const appearance = preservable(0);
// 		group( () => {
// 			setting[1](1);
// 			appearance[1](1);
// 		}, 0, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo 0");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 1);
// 		assert.equal(appearance[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 			appearance[1](2);
// 		}, 0, true); // should coalesce
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(undoDescription(), "Undo 0");
// 		assert.equal(redoDescription(), null);
// 		assert.equal(setting[0](), 2);
// 		assert.equal(appearance[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(undoDescription(), null);
// 		assert.equal(redoDescription(), "Redo 0");
// 		assert.equal(setting[0](), 0);
// 		assert.equal(appearance[0](), 0);
// 	});

// 	it("undoable zero coalescing key is valid and coalesces", function() {
// 		const { undoable, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, null, 0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group zero coalescing key is valid and coalesces", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		group( () => {
// 			setting[1](1);
// 		}, null, 0);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, null, 0);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable false coalescing key does not coalesce", function() {
// 		const { undoable, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, null, false);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});
	
// 	it("group false coalescing key does not coalesce", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		group( () => {
// 			setting[1](1);
// 		}, null, false);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, null, false);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable coalesce NaN does not coalesce", function() {
// 		const { undoable, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, "change setting", Number.NaN);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group coalesce NaN does not coalesce", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		const grouper = () => {
// 			setting[1](setting[0]() + 1);
// 		};
// 		group(grouper, "change setting", Number.NaN);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group(grouper, undefined, true);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("group coalesce undefined does not coalesce", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		group( () => {
// 			setting[1](1);
// 		}, undefined, undefined);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, undefined, undefined);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 	});

// 	it("group coalesce null does not coalesce", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		group( () => {
// 			setting[1](1);
// 		}, undefined, null);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, undefined, null);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 	});

// 	it("group coalesce false does not coalesce", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		group( () => {
// 			setting[1](1);
// 		}, undefined, false);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, undefined, false);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 	});

// 	it("group coalesce zero does coalesce", function() {
// 		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		group( () => {
// 			setting[1](1);
// 		}, undefined, 0);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		group( () => {
// 			setting[1](2);
// 		}, undefined, 0);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable coalesce false does not coalesce", function() {
// 		const { undoable, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, "change setting", false);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable coalesce undefined does not coalesce", function() {
// 		const { undoable, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, "change setting", undefined);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable coalesce null does not coalesce", function() {
// 		const { undoable, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, "change setting", null);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 1);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});

// 	it("undoable coalesce zero does coalesce", function() {
// 		const { undoable, undo, canUndo, canRedo } = new UndoManager();
// 		const setting = undoable(0, "change setting", 0);
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 0);
// 		setting[1](1);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 1);
// 		setting[1](2);
// 		assert.equal(canUndo(), true);
// 		assert.equal(canRedo(), false);
// 		assert.equal(setting[0](), 2);
// 		undo();
// 		assert.equal(canUndo(), false);
// 		assert.equal(canRedo(), true);
// 		assert.equal(setting[0](), 0);
// 	});
});