import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("PRESERVABLE", function() {
	it("one", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		appearance[1](1);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 1);
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		appearance[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 2);
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		appearance[1](3);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 3);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 2);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 1);
		undo(); // one extra should have no effect
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
		redo(); // one extra should have no effect
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 2);
	});

	it("two", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		appearance[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		setting[1](2);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 0);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(setting[0](), 2);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 1);
		assert.equal(appearance[0](), 1);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(setting[0](), 0);
		assert.equal(appearance[0](), 0);
	});

	it("three", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo} = new UndoManager();
		const setting = undoable(0, "change setting");
		const appearance = preservable(0);
		appearance[1](1);
		setting[1](1);
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		appearance[1](2);
		setting[1](2);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		appearance[1](3);
		setting[1](3);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		undo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		undo();
		assert.equal(canUndo(), false);
		assert.equal(canRedo(), true);
		assert.equal(appearance[0](), 1);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), true);
		redo();
		assert.equal(canUndo(), true);
		assert.equal(canRedo(), false);
		assert.equal(appearance[0](), 3);
	});

	it("Changing a preservable does not affect canUndo without undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		appearance[1](1);
		assert.equal(canUndo(), false);
	});

	it("Changing a preservable does not affect canUndo with undoable", function() {
		const { undoable, preservable, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		appearance[1](1);
		assert.equal(canUndo(), false);
	});

	it("Changing a preservable in a group does not affect canUndo without undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		group(() => {
			appearance[1](1);
		});
		assert.equal(canUndo(), false);
	});

	it("Changing a preservable in a group does not affect canUndo with undoable", function() {
		const { undoable, preservable, group, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		group(() => {
			appearance[1](1);
		});
		assert.equal(canUndo(), false);
	});

	it("Changing a preservable in a coalescing group does not affect canUndo without undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		group(() => {
			appearance[1](1);
		}, "change", "change");
		group(() => {
			appearance[1](2);
		}, "change", "change");
		assert.equal(canUndo(), false);
	});

	it("Changing a preservable in a coalescing group does not affect canUndo with undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		group(() => {
			appearance[1](1);
		}, "change", "change");
		group(() => {
			appearance[1](2);
		}, "change", "change");
		assert.equal(canUndo(), false);
	});
});