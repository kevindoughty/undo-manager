import { SignalOptions, Setter, Signal, createSignal } from "solid-js";
import { Context } from "./context.js";

/**
 * An undoable object
 * @typedef {function} Undoable
 * @param {*} value
 * @param {*} description
 * @param {*} coalescing
 */
class Undoable<T> {
	private context:Context;
	private description:any;
	private coalescing:any;
	private id:number;
	private _signal:Signal<T>;
	private comparator:(a:T,b:T)=>boolean;

	constructor(context:Context, value:T, description?:any, coalescing?:any, options?:SignalOptions<T>) {
		this.context = context;
		this.description = description;
		this.coalescing = coalescing;
		this.id = context.registerUndoable();
		this._signal = createSignal(value, options);
		this.comparator = options && options.equals ? options.equals : (a,b) => a === b;
	}

	private registerUndo(instance:Undoable<T>, value:T) {
		const undoer = () => {
			instance.context.group( () => {
				const original:T = instance.getter();
				instance._signal[1](()=>value);
				instance.registerUndo(instance, original);
			}, instance.description);
		};
		this.context.stack.registerUndoerInvocation(undoer, instance.id);
	}

	getter() {
		return this._signal[0]();
	}

	setter(value:Setter<T>) {
		let actualCoalescing = this.coalescing;
		if (this.coalescing === true && (this.description === null || this.description === undefined)) {
			actualCoalescing = this;
		}
		const original:T = this.getter();
		if (typeof value === "function") {
			const funk: (prev:T)=>Exclude<T, Function> = value;
			const result:Exclude<T, Function> = funk(original);
			if (!this.comparator(result,original)) {
				this.context.group( () => {
					this._signal[1](result);
					this.registerUndo(this, original);
				}, this.description, actualCoalescing);
			}
		} else {
			if (!this.comparator(value,original)) {
				this.context.group( () => {
					this._signal[1](value);
					this.registerUndo(this, original);
				}, this.description, actualCoalescing);
			}
		}
	}
}

export { Undoable };