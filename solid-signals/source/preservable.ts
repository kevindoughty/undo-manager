import { SignalOptions, Setter, Signal, createSignal } from "solid-js";
import { Context } from "./context.js";

/**
 * A preservable object
 * @typedef {function} Preservable
 * @param {*} value
 * @param {boolean} interrupting
 */
class Preservable<T> {
	private context:Context;
	private _signal:Signal<T>;
	private interrupting:boolean;
	private comparator:(a:T,b:T)=>boolean;

	constructor(context:Context, value:T, interrupting?:boolean, options?:SignalOptions<T>) {
		this.context = context;
		this._signal = createSignal(value, options);
		this.interrupting = interrupting ? true : false;
		this.comparator = options && options.equals ? options.equals : (a,b) => a === b;
		context.registerPreservable(this, this.registerBefore.bind(this), this.registerAfter.bind(this));
	}

	private registerBefore(value:Exclude<T, Function>) {
		const invocation = (direction:number) => {
			if (direction < 0) { // undo
				this._signal[1](value);
			}
			return this.registerBefore(value);
		};
		return invocation;
	}
	
	private registerAfter(value:Exclude<T, Function>) {
		const invocation = (direction:number) => {
			if (direction > 0) { // redo
				this._signal[1](value);
			}
			return this.registerAfter(value);
		};
		return invocation;
	}

	getter() {
		return this._signal[0]();
	}

	setter(value:Setter<T>) {
		const original:T = this.getter();
		if (typeof value === "function") {
			const funk: (prev:T)=>Exclude<T, Function> = value;
			const result:Exclude<T, Function> = funk(original);
			if (!this.comparator(result,original)) {
				if (this.interrupting) this.context.makeNonCoalescingChange();
				this._signal[1](value);
			}
		} else {
			if (!this.comparator(value,original)) {
				if (this.interrupting) this.context.makeNonCoalescingChange();
				this._signal[1](value);
			}
		}
	}
}

export { Preservable };