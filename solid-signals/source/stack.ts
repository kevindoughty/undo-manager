import { Accessor, Signal, createSignal, createMemo } from "solid-js";
import { Entry } from "./entry.js";
import type { Localizer } from "./types.js";
import type { Invocation, Undoer } from "./entry.js";

class Stack {
	private maxCount:number;
	private next: Entry;
	private staging: Entry | undefined;
	private undoStack:Signal<Array<Entry>>;
	private redoStack:Signal<Array<Entry>>;
	private undoDescriptions:Signal<Array<any>>;
	private redoDescriptions:Signal<Array<any>>;
	canUndo:Accessor<boolean>;
	canRedo:Accessor<boolean>;
	undoDescription:Accessor<string | undefined>;
	redoDescription:Accessor<string | undefined>;

	constructor(undoLocalizer?:Localizer, redoLocalizer?:Localizer, maxCount?: number) {
		if (!undoLocalizer) undoLocalizer = () => undefined;
		if (!redoLocalizer) redoLocalizer = () => undefined;
		if (!Number.isInteger(maxCount) || !maxCount || maxCount < 0) maxCount = Infinity;
		this.maxCount = maxCount;
		this.next = this.empty();
		this.staging = this.empty();
		const undoStackValue: Array<Entry> = [];
		this.undoStack = createSignal(undoStackValue);
		const redoStackValue: Array<Entry> = [];
		this.redoStack = createSignal(redoStackValue);
		const undoDescriptionsValue: Array<any> = [];
		this.undoDescriptions = createSignal(undoDescriptionsValue);
		const redoDescriptionsValue: Array<any> = [];
		this.redoDescriptions = createSignal(redoDescriptionsValue);
		
		this.canUndo = () => {
			return this.undoStack[0]().length > 0;
		};

		this.canRedo = () => {
			return this.redoStack[0]().length > 0;
		};

		this.undoDescription = () => {
			const input:Array<any> = this.undoDescriptions[0]();
			const length = input.length;
			const value = length ? input[length-1] : undefined;
			if (value === null || value === undefined) return undefined;
			return undoLocalizer(value);
		};

		this.redoDescription = () => {
			const input:Array<any> = this.redoDescriptions[0]();
			const length = input.length;
			const value = length ? input[length-1] : undefined;
			if (value === null || value === undefined) return undefined;
			return redoLocalizer(value);
		};
	}

	private empty() {
		return new Entry();
	}

	willUndo() { // pop undo stack to staging
		const undoPrevious = this.undoStack[0]();
		const undoNext = [ ...undoPrevious ];
		this.staging = undoNext.pop();
		this.undoStack[1](undoNext);
		this.next = this.empty();
		const redoNext = [ ...this.redoStack[0](), this.next ];
		this.redoStack[1](redoNext);

		const undoDescriptionsNext = [ ...this.undoDescriptions[0]() ];
		const currentUndoDescription = undoDescriptionsNext.pop();
		this.undoDescriptions[1](undoDescriptionsNext);
		this.redoDescriptions[1]([ ...this.redoDescriptions[0](), currentUndoDescription ]);
	}

	willRedo() { // pop redo stack to staging
		const redoPrevious = this.redoStack[0]();
		const redoNext = [ ...redoPrevious ];
		this.staging = redoNext.pop();
		this.redoStack[1](redoNext);
		this.next = this.empty();
		const undoNext = [ ...this.undoStack[0](), this.next ];
		this.undoStack[1](undoNext);
		
		const redoDescriptionsNext = [ ...this.redoDescriptions[0]()];
		const currentRedoDescription = redoDescriptionsNext.pop();
		this.redoDescriptions[1](redoDescriptionsNext);
		this.undoDescriptions[1]([ ...this.undoDescriptions[0](), currentRedoDescription ]);
	}

	changeReset() { // before change
		this.next = this.empty();
		this.redoDescriptions[1]([]);
	}

	coalesceReset() { // wipe existing after state for recapture
		const nextNext = Entry.clone(this.next);
		nextNext.after = [];
		this.next = nextNext;
	}

	afterChange(changeDescription?:any, coalesce?:any) {
		if (this.maxCount > 0 && Object.keys(this.next.invocations).length) {
			if (coalesce) {
				const nextStack = this.undoStack[0]().slice(0);
				nextStack.pop();
				nextStack.push(this.next);
				this.undoStack[1](nextStack);
				const nextInput = this.undoDescriptions[0]().slice(0);
				if (changeDescription !== undefined && changeDescription !== null) {
					nextInput.pop();
					nextInput.push(changeDescription);
				}
				this.undoDescriptions[1](nextInput);
			} else {
				const undoStackNext = [ ...this.undoStack[0]() ];
				const undoDescriptionsNext = [ ...this.undoDescriptions[0]() ];
				if (undoStackNext.length === this.maxCount) {
					undoStackNext.shift();
					undoDescriptionsNext.shift();
				}
				if (undoStackNext.length < this.maxCount) {
					undoStackNext.push(this.next);
					undoDescriptionsNext.push(changeDescription);
				}
				this.undoStack[1](undoStackNext);
				this.redoStack[1]([]);
				this.undoDescriptions[1](undoDescriptionsNext);
				this.redoDescriptions[1]([]);
			}
		}
	}

	registerBeforeInvocation(invocation:Invocation) {
		this.next.before.push(invocation);
	}

	registerUndoerInvocation(undoer:Undoer, key:number) {
		const existing = this.next.invocations[key]; // don't overwrite existing when coalescing
		if (existing === undefined) {
			this.next.invocations[key] = undoer;
		}
	}

	registerAfterInvocation(invocation:Invocation) {
		this.next.after.push(invocation);
	}

	applier(direction:number) {
		if (this.staging !== undefined) {
			const before = this.staging.before;
			before.forEach( invocation => {
				this.next.before.push(invocation(direction));
			});
			const invocations = this.staging.invocations;
			Object.keys(this.staging.invocations).forEach( key => {
				const invocation = invocations[Number(key)];
				invocation(direction);
			});
			const after = this.staging.after;
			after.forEach( invocation => {
				this.next.after.push(invocation(direction));
			});
		}
	}
}

export { Stack };