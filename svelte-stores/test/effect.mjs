import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get, derived } from "svelte/store";


describe("SUBSCRIBE", function() { // formerly EFFECT
	it("one", function() { // different from Preact Signals
		const { undoable, preservable, undo, redo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		const computed = derived(
			[setting, appearance],
			() => {
				return get(setting) + get(appearance);
			}
		);
		let summation = 0;
		let count = 0;
		computed.subscribe(value => {
			summation = summation + value;
			count++;
		});
		assert.equal(count, 1);
		assert.equal(summation, 0);
		appearance.set(1);
		assert.equal(count, 2);
		assert.equal(summation, 1); // 0 + (0 + 1)
		setting.set(10);
		assert.equal(count, 3);
		assert.equal(summation, 12); // 1 + (10 + 1)
		appearance.set(100);
		assert.equal(count, 4);
		assert.equal(summation, 122); // 12 + (10 + 100)
		setting.set(1000);
		assert.equal(count, 5);
		assert.equal(summation, 1222); // 122 + (1000 + 100)
		appearance.set(10000);
		assert.equal(count, 6);
		assert.equal(summation, 12222); // 1222 + (1000 + 10000)
		undo(); // Changes in undo are grouped.

		// different from Preact Signals:
		// assert.equal(count, 7); // ? 8
		// assert.equal(get(setting),10);
		// assert.equal(get(appearance),100);
		// assert.equal(summation, 12332); // ? 13432 // 12222 + (10 + 100)
		// undo(); // Changes in undo are grouped.
		// assert.equal(count, 8);
		// assert.equal(summation, 12333); // 12332 + (0 + 1)
		// redo(); // Changes in redo are grouped.
		// assert.equal(count, 9);
		// assert.equal(summation, 12344); // 12333 + (10 + 1)
		// redo(); // Changes in redo are grouped.
		// assert.equal(count, 10);
		// assert.equal(summation, 13444); // 12344 + (1000 + 100)
	});

	it("Differs from Preact Signals", function() {
		const { undoable, preservable, undo, redo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		const computed = derived(
			[setting, appearance],
			() => {
				return get(setting) + get(appearance);
			}
		);
		let summation = 0;
		let count = 0;
		computed.subscribe(value => {
			summation = summation + value;
			count++;
		});
		assert.equal(count, 1);
		assert.equal(summation, 0);
		appearance.set(1);
		assert.equal(count, 2);
		assert.equal(summation, 1); // 0 + (0 + 1)
		setting.set(10);
		assert.equal(count, 3);
		assert.equal(summation, 12); // 1 + (10 + 1)
		appearance.set(100);
		assert.equal(count, 4);
		assert.equal(summation, 122); // 12 + (10 + 100)
		setting.set(1000);
		assert.equal(count, 5);
		assert.equal(summation, 1222); // 122 + (1000 + 100)
		appearance.set(10000);
		assert.equal(count, 6);
		assert.equal(summation, 12222); // 1222 + (1000 + 10000)
		undo(); // Changes in undo are grouped.
		
		// different from Preact Signals:
		assert.equal(count, 8); // 7 with Preact Signals
		assert.equal(get(setting),10);
		assert.equal(get(appearance),100);
		assert.equal(summation, 13432); // 12222 + (10 + 100) = 12332 with Preact Signals
		undo(); // Changes in undo are grouped.
		assert.equal(count, 10); // 8 with Preact Signals
		assert.equal(summation, 13444); // 12332 + (0 + 1) = 12333 with Preact Signals
		redo(); // Changes in redo are grouped.
		assert.equal(count, 11); // 9 with Preact Signals
		assert.equal(summation, 13455); // 12333 + (10 + 1) = 12344 with Preact Signals
		redo(); // Changes in redo are grouped.
		assert.equal(count, 13); // 10 with Preact Signals
		assert.equal(summation, 15556); // 12344 + (1000 + 100) = 13444 with Preact Signals
	});
});