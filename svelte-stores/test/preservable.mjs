import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get } from "svelte/store";


describe("PRESERVABLE", function() {
	it("one", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		appearance.set(1);
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 1);
		setting.set(1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		appearance.set(2);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 2);
		setting.set(2);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		appearance.set(3);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 3);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 1);
		undo(); // one extra should have no effect
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		redo(); // one extra should have no effect
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("two", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		setting.set(1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 0);
		appearance.set(1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		setting.set(2);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
	});

	it("three", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo} = new UndoManager();
		const setting = undoable(0, "change setting");
		const appearance = preservable(0);
		appearance.set(1);
		setting.set(1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		appearance.set(2);
		setting.set(2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		appearance.set(3);
		setting.set(3);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(appearance), 3);
	});

	it("Changing a preservable does not affect canUndo without undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		appearance.set(1);
		assert.equal(get(canUndo), false);
	});

	it("Changing a preservable does not affect canUndo with undoable", function() {
		const { undoable, preservable, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		appearance.set(1);
		assert.equal(get(canUndo), false);
	});

	it("Changing a preservable in a group does not affect canUndo without undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		group(() => {
			appearance.set(1);
		});
		assert.equal(get(canUndo), false);
	});

	it("Changing a preservable in a group does not affect canUndo with undoable", function() {
		const { undoable, preservable, group, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		group(() => {
			appearance.set(1);
		});
		assert.equal(get(canUndo), false);
	});

	it("Changing a preservable in a coalescing group does not affect canUndo without undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		group(() => {
			appearance.set(1);
		}, "change", "change");
		group(() => {
			appearance.set(2);
		}, "change", "change");
		assert.equal(get(canUndo), false);
	});

	it("Changing a preservable in a coalescing group does not affect canUndo with undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		group(() => {
			appearance.set(1);
		}, "change", "change");
		group(() => {
			appearance.set(2);
		}, "change", "change");
		assert.equal(get(canUndo), false);
	});
});