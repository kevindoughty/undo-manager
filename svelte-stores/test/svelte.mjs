import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get, derived, writable } from "svelte/store";
import { Suite } from "mocha";

function isFunction(w) {
	return w && {}.toString.call(w) === "[object Function]";
}

describe("SVELTE", function() {
	it("Constructor exists", () => {
		assert.exists(UndoManager);
	});
	it("Instance exists", () => {
		const manager = new UndoManager();
		assert.exists(manager);
	});
	it("undoable exists", function() {
		const { undoable } = new UndoManager();
		assert(isFunction(undoable));
	});
	it("preservable exists", function() {
		const { preservable } = new UndoManager();
		assert(isFunction(preservable));
	});
	it("group exists", function() {
		const { group } = new UndoManager();
		assert(isFunction(group));
	});
	it("undo exists", function() {
		const { undo } = new UndoManager();
		assert(isFunction(undo));
	});
	it("redo exists", function() {
		const { redo } = new UndoManager();
		assert(isFunction(redo));
	});
	it("canUndo exists", function() {
		const { canUndo } = new UndoManager();
		assert(typeof canUndo !== "undefined" && canUndo !== undefined && canUndo !== null);
		//assert(canUndo.value === false);
	});
	it("canRedo exists", function() {
		const { canRedo } = new UndoManager();
		assert(typeof canRedo !== "undefined" && canRedo !== undefined && canRedo !== null);
		//assert(canRedo.value === false);
	});
	it("undoDescription exists", function() {
		const { undoDescription } = new UndoManager();
		assert(typeof undoDescription !== "undefined" && undoDescription !== undefined && undoDescription !== null);
		//assert.equal(undoDescription.value, null);
	});
	it("redoDescription exists", function() {
		const { redoDescription } = new UndoManager();
		assert(typeof redoDescription !== "undefined" && redoDescription !== undefined && redoDescription !== null);
		//assert.equal(redoDescription.value, null); 
	});


	it("canUndo initial", function() {
		const { canUndo } = new UndoManager();
		assert(get(canUndo) === false);
	});
	it("canRedo initial", function() {
		const { canRedo } = new UndoManager();
		assert(get(canRedo) === false);
	});
	it("undoDescription initial", function() {
		const { undoDescription } = new UndoManager();
		assert.equal(get(undoDescription), null);
	});
	it("redoDescription initial", function() {
		const { redoDescription } = new UndoManager();
		assert.equal(get(redoDescription), null); 
	});

	it("undoable create", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		assert.equal(get(setting), 1);
	});

	it("undoable set", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		setting.set(2);
		assert.equal(get(setting), 2);
	});

	it("preservable create", function() {
		const { preservable } = new UndoManager();
		const appearance = preservable(1);
		assert.equal(get(appearance), 1);
	});

	it("preservable set", function() {
		const { preservable } = new UndoManager();
		const appearance = preservable(1);
		appearance.set(2);
		assert.equal(get(appearance), 2);
	});

	it("writable create reactive", function() {
		const setting = writable(1);
		const second = derived(
			setting, 
			() => {
				return get(setting) + 10;
			}
		);
		assert.equal(get(second), 11);
	});

	it("writable set reactive", function() {
		const setting = writable(1);
		const second = derived(
			setting, 
			() => {
				return get(setting) + 10;
			}
		);
		setting.set(2);
		assert.equal(get(second), 12);
	});

	it("writable create reactive second order", function() {
		const setting = writable(1);
		const second = () => { 
			return derived(
				setting, 
				() => {
					return get(setting) + 10;
				}
			);
		}
		const third = second();
		assert.equal(get(third), 11);
	});

	it("writable set reactive second order", function() {
		const setting = writable(1);
		const second = () => { 
			return derived(
				setting, 
				() => {
					return get(setting) + 10;
				}
			);
		}
		const third = second();
		setting.set(2);
		assert.equal(get(third), 12);
	});

	it("undoable create reactive", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		const second = derived(
			setting, 
			() => {
				return get(setting) + 10;
			}
		);
		assert.equal(get(second), 11);
	});

	it("undoable set reactive before", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		setting.set(2);
		const second = derived(
			setting, 
			() => {
				return get(setting) + 10;
			}
		);
		assert.equal(get(second), 12);
	});

	it("undoable set reactive after", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		const second = derived(
			setting, 
			() => {
				return get(setting) + 10;
			}
		);
		setting.set(2);
		assert.equal(get(second), 12);
	});

	it("undoable canUndo initial", function() {
		const { undoable, canUndo } = new UndoManager();
		const setting = undoable(1);
		assert.equal(get(canUndo), false);
	});
	it("undoable canRedo initial", function() {
		const { undoable, canRedo } = new UndoManager();
		const setting = undoable(1);
		assert.equal(get(canRedo), false);
	});

	it("undoable canUndo first", function() {
		const { undoable, canUndo } = new UndoManager();
		const setting = undoable(1);
		setting.set(2);
		assert.equal(get(canUndo), true);
	});

	it("undoable undo", function() {
		const { undoable, undo } = new UndoManager();
		const setting = undoable(1);
		setting.set(2);
		undo();
		assert.equal(get(setting), 1);
	});

	it("undoable canRedo first", function() {
		const { undoable, undo, canRedo } = new UndoManager();
		const setting = undoable(1);
		setting.set(2);
		undo();
		assert.equal(get(canRedo), true);
	});

	it("undoable update", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		const updater = (value) => {
			return value + 10;
		};
		assert.equal(get(setting), 1);
		setting.update(updater);
		assert.equal(get(setting), 11);
	});

	it("undoable update undo", function() {
		const { undoable, undo } = new UndoManager();
		const setting = undoable(1);
		const updater = (value) => {
			return value + 10;
		};
		assert.equal(get(setting), 1);
		setting.update(updater);
		assert.equal(get(setting), 11);
		undo();
		assert.equal(get(setting), 1);
	});

	it("preservable update", function() {
		const { undoable, preservable } = new UndoManager();
		const setting = undoable("a");
		const appearance = preservable(1);
		const updater = (value) => {
			return value + 10;
		};
		assert.equal(get(appearance), 1);
		appearance.update(updater);
		assert.equal(get(appearance), 11);
	});

	it("preservable update undo", function() {
		const { undoable, preservable, undo } = new UndoManager();
		const setting = undoable("a");
		const appearance = preservable(1);
		const updater = (value) => {
			return value + 10;
		};
		assert.equal(get(appearance), 1);
		setting.set("b");
		appearance.update(updater);
		assert.equal(get(appearance), 11);
		undo();
		assert.equal(get(appearance), 1);
	});

	it("undoable unsubscribe", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		let subscribeSum = 0;
		let subscribeCount = 0;
		const unsubscriber = setting.subscribe(value => {
			subscribeSum += value;
			subscribeCount++;
		});
		assert.equal(get(setting), 1);
		assert.equal(subscribeSum, 1);
		assert.equal(subscribeCount, 1);
		setting.set(2);
		assert.equal(get(setting), 2);
		assert.equal(subscribeSum, 3);
		assert.equal(subscribeCount, 2);
		unsubscriber();
		setting.set(3);
		assert.equal(get(setting), 3);
		assert.equal(subscribeSum, 3);
		assert.equal(subscribeCount, 2);
	});

	it("undoable unsubscribe with undo", function() {
		const { undoable, undo } = new UndoManager();
		const setting = undoable(1);
		let subscribeSum = 0;
		let subscribeCount = 0;
		const unsubscriber = setting.subscribe(value => {
			subscribeSum += value;
			subscribeCount++;
		});
		assert.equal(get(setting), 1);
		assert.equal(subscribeSum, 1);
		assert.equal(subscribeCount, 1);
		setting.set(2);
		assert.equal(get(setting), 2);
		assert.equal(subscribeSum, 3);
		assert.equal(subscribeCount, 2);
		undo();
		assert.equal(get(setting), 1);
		assert.equal(subscribeSum, 4);
		assert.equal(subscribeCount, 3);
		unsubscriber();
		setting.set(3);
		assert.equal(get(setting), 3);
		assert.equal(subscribeSum, 4);
		assert.equal(subscribeCount, 3);
	});

	it("undoable invalidate", function() {
		const { undoable } = new UndoManager();
		const setting = undoable(1);
		let subscribeSum = 0;
		let subscribeCount = 0;
		let invalidateCount = 0;
		const invalidator = () => {
			invalidateCount++;
		}
		const unsubscriber = setting.subscribe(value => {
			subscribeSum += value;
			subscribeCount++;
		}, invalidator);
		assert.equal(get(setting), 1);
		assert.equal(subscribeSum, 1);
		assert.equal(subscribeCount, 1);
		assert.equal(invalidateCount, 0);
		setting.set(2);
		assert.equal(get(setting), 2);
		assert.equal(subscribeSum, 3);
		assert.equal(subscribeCount, 2);
		assert.equal(invalidateCount, 1);
		setting.set(3);
		assert.equal(get(setting), 3);
		assert.equal(subscribeSum, 6);
		assert.equal(subscribeCount, 3);
		assert.equal(invalidateCount, 2);
		unsubscriber();
		setting.set(4);
		assert.equal(get(setting), 4);
		assert.equal(subscribeSum, 6);
		assert.equal(subscribeCount, 3);
		assert.equal(invalidateCount, 2);
	});

	it("undoable invalidate with undo", function() {
		const { undoable, undo } = new UndoManager();
		const setting = undoable(1);
		let subscribeSum = 0;
		let subscribeCount = 0;
		let invalidateCount = 0;
		const invalidator = () => {
			invalidateCount++;
		}
		const unsubscriber = setting.subscribe(value => {
			subscribeSum += value;
			subscribeCount++;
		}, invalidator);
		assert.equal(get(setting), 1);
		assert.equal(subscribeSum, 1);
		assert.equal(subscribeCount, 1);
		assert.equal(invalidateCount, 0);
		setting.set(2);
		assert.equal(get(setting), 2);
		assert.equal(subscribeSum, 3);
		assert.equal(subscribeCount, 2);
		assert.equal(invalidateCount, 1);
		undo();
		assert.equal(get(setting), 1);
		assert.equal(subscribeSum, 4);
		assert.equal(subscribeCount, 3);
		assert.equal(invalidateCount, 2);
		setting.set(3);
		assert.equal(get(setting), 3);
		assert.equal(subscribeSum, 7);
		assert.equal(subscribeCount, 4);
		assert.equal(invalidateCount, 3);
		unsubscriber();
		setting.set(4);
		assert.equal(get(setting), 4);
		assert.equal(subscribeSum, 7);
		assert.equal(subscribeCount, 4);
		assert.equal(invalidateCount, 3);
	});

	it("preservable unsubscribe and invalidate with undo", function() {
		const { undoable, preservable, undo } = new UndoManager();
		const setting = undoable(1);
		const appearance = preservable(1);
		let subscribeSum = 0;
		let subscribeCount = 0;
		const unsubscriber = appearance.subscribe(value => {
			subscribeSum += value;
			subscribeCount++;
		});
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		assert.equal(subscribeSum, 1);
		assert.equal(subscribeCount, 1);
		setting.set(2);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 1);
		assert.equal(subscribeSum, 1);
		assert.equal(subscribeCount, 1);
		appearance.set(5);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 5);
		assert.equal(subscribeSum, 6);
		assert.equal(subscribeCount, 2);
		setting.set(3);
		assert.equal(get(setting), 3);
		assert.equal(get(appearance), 5);
		assert.equal(subscribeSum, 6);
		assert.equal(subscribeCount, 2);
		undo();
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 5);
		assert.equal(subscribeSum, 6);
		assert.equal(subscribeCount, 2);
		undo();
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		assert.equal(subscribeSum, 7);
		assert.equal(subscribeCount, 3);
		unsubscriber();
		appearance.set(10);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 10);
		assert.equal(subscribeSum, 7);
		assert.equal(subscribeCount, 3);
	});
});