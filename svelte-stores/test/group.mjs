import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get } from "svelte/store";


describe("GROUP", function() {
	it("one", function() { // Compare to first in test/batch.js
		const { undoable, group, undo, canUndo, canRedo } = new UndoManager();
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting1), 0);
		assert.equal(get(setting2), 0);
		group(()=> {
			setting1.set(1);
			setting2.set(2);
		});
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting1), 1);
		assert.equal(get(setting2), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting1), 0);
		assert.equal(get(setting2), 0);
	});
	
	it("two", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		group(()=> {
			setting.set(1);
			appearance.set(1);
		});
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group(()=> {
			setting.set(2);
			appearance.set(2);
		});
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		undo(); // one extra should have no effect
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		redo(); // one extra should have no effect
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("three", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		group(()=> {
			setting.set(1);
			appearance.set(1);
		});
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		appearance.set(3);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 3);
		group(()=> {
			setting.set(2);
			appearance.set(2);
		});
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 3);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		undo(); // one extra should have no effect
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		redo(); // one extra should have no effect
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("four", function() { // using coalesce key
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(get(canUndo), false);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, null, "setting"); // these two should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", "setting"); // these two should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("five", function() { // coalesce key can be an object reference
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(get(canUndo), false);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, null, setting); // these two should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", setting); // these two should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("six", function() { // does not coalesce if keys don't match
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(get(canUndo), false);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, null, "setting"); // these two should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change not setting", "not setting"); // these two should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change not setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change not setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change not setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change not setting");
		assert.equal(get(redoDescription), null); 
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("seven", function() { // can coalesce even if group signatures don't match
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		group( () => {
			setting1.set(1);
			appearance1.set(1);
		}, "change settings", "setting"); // these two should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change settings");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting1), 1);
		assert.equal(get(setting2), 0);
		assert.equal(get(appearance1), 1);
		assert.equal(get(appearance2), 0);
		group( () => {
			setting2.set(2);
			appearance2.set(2);
		}, "change settings", "setting"); // these two should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change settings");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting1), 1);
		assert.equal(get(setting2), 2);
		assert.equal(get(appearance1), 1);
		assert.equal(get(appearance2), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change settings");
		assert.equal(get(setting1), 0);
		assert.equal(get(setting2), 0);
		assert.equal(get(appearance1), 0);
		assert.equal(get(appearance2), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change settings");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting1), 1);
		assert.equal(get(setting2), 2);
		assert.equal(get(appearance1), 1);
		assert.equal(get(appearance2), 2);
	});

	it("eight", function() { // setting undoable between coalescing groups prevents coalescing
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		setting.set(3); // this should prevent coalescing
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 3);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 3);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
	});

	it("nine", function() { // setting preservable between coalescing groups prevents coalescing
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, true);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		appearance.set(3); // interrupting
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 3);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 3);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("nine alternate", function() {
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, false);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		appearance.set(3); // not interrupting
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 3);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("ten", function() { // should not coalesce after redo
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(get(canUndo), false);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, null, "setting"); // these two should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", "setting"); // these two should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		group( () => {
			setting.set(3);
			appearance.set(3);
		}, "change setting again", "setting"); // should not coalesce after redo
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting again");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 3);
		assert.equal(get(appearance), 3);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting again");
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("eleven", function() { // should not coalesce after undo
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, true);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, null, "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		appearance.set(4); // interrupting
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 4);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, null, "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 4);
		group( () => {
			setting.set(3);
			appearance.set(3);
		}, null, "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 3);
		assert.equal(get(appearance), 3);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 4);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
	});
	
	it("eleven alternate", function() {
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, false);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, null, "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		appearance.set(4); // not interrupting
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 4);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, null, "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		group( () => {
			setting.set(3);
			appearance.set(3);
		}, null, "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 3);
		assert.equal(get(appearance), 3);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
	});

	it("twelve", function() { // undefined coalesce key will not coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(get(canUndo), false);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, "change setting"); // should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting"); // should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("thirteen", function() { // null coalesce key will not coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(get(canUndo), false);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, "change setting", null); // should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", null); // should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});
	
	it("fourteen", function() { // boolean false coalesce key will not coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(get(canUndo), false);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, "change setting", false); // should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", false); // should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("fifteen", function() { // boolean true coalesce key will use changeDescription to coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, "change setting", true); // should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", true); // should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("sixteen", function() { // should not coalesce even with boolean true coalesce key if changeDescriptions don't match
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, "change setting", true);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting again", true); // should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting again");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting again");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting again");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting again");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
	});

	it("nested group call order one", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0);
		group( () => {
			setting.set(1);
			group( () => {
				setting.set(2);
			});
		});
		assert.equal(get(setting), 2);
		assert.equal(get(canUndo), true);
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
		
	});

	it("nested group call order two", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0);
		group( () => {
			group( () => {
				setting.set(1);
			});
			setting.set(2);
		});
		assert.equal(get(setting), 2);
		assert.equal(get(canUndo), true);
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
	});

	it("nesting outer group wins description", function() {
		const undoLocalizer = function(input) {
			return "Undo " + input;
		};
		const redoLocalizer = function(input) {
			return "Redo " + input;
		};
		const { undoable, group, undo, canUndo, undoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0,"change undoable");
		group( () => {
			group( () => {
				setting.set(2);
			}, "change inner");
			setting.set(1);
		}, "change outer");
		assert.equal(get(setting), 1);
		assert.equal(get(canUndo), true);
		assert.equal(get(undoDescription), "Undo change outer");
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
		assert.equal(get(undoDescription), null);
	});

	it("nesting outer group wins coalescing false", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, null, "undoable coalesce");
		group( () => {
			setting.set(1);
			group( () => {
				setting.set(2);
			}, null, "inner coalesce");
		}, null, false);
		group( () => {
			setting.set(3);
			group( () => {
				setting.set(4);
			}, null, "inner coalesce");
		}, null, false);
		assert.equal(get(setting), 4);
		assert.equal(get(canUndo), true);
		undo();
		assert.equal(get(setting), 2);
		assert.equal(get(canUndo), true);
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
	});

	it("nesting outer group wins coalescing true", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, null, false);
		group( () => {
			setting.set(1);
			group( () => {
				setting.set(2);
			}, null, false);
		}, null, "outer coalesce");
		group( () => {
			setting.set(3);
			group( () => {
				setting.set(4);
			}, null, false);
		}, null, "outer coalesce");
		assert.equal(get(setting), 4);
		assert.equal(get(canUndo), true);
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
	});

	it("nesting outer group wins coalescing false two", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, null, "undoable coalesce");
		group( () => {
			setting.set(1);
			group( () => {
				setting.set(2);
			}, null, "inner coalesce");
			group( () => {
				setting.set(3);
			}, null, "inner coalesce");
		}, null, false);
		group( () => {
			setting.set(4);
			group( () => {
				setting.set(5);
			}, null, "inner coalesce");
			group( () => {
				setting.set(6);
			}, null, "inner coalesce");
		}, null, false);
		assert.equal(get(setting), 6);
		assert.equal(get(canUndo), true);
		undo();
		assert.equal(get(setting), 3);
		assert.equal(get(canUndo), true);
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
	});

	it("nesting outer group wins coalescing true", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, null, false);
		group( () => {
			setting.set(1);
			group( () => {
				setting.set(2);
			}, null, false);
		}, null, "outer coalesce");
		group( () => {
			setting.set(3);
			group( () => {
				setting.set(4);
			}, null, false);
		}, null, "outer coalesce");
		assert.equal(get(setting), 4);
		assert.equal(get(canUndo), true);
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
	});

	it("nesting outer group wins coalescing true two", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, null, false);
		group( () => {
			setting.set(1);
			group( () => {
				setting.set(2);
			}, null, false);
			group( () => {
				setting.set(3);
			}, null, false);
		}, null, "outer coalesce");
		group( () => {
			setting.set(4);
			group( () => {
				setting.set(5);
			}, null, false);
			group( () => {
				setting.set(6);
			}, null, false);
		}, null, "outer coalesce");
		assert.equal(get(setting), 6);
		assert.equal(get(canUndo), true);
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
	});

	it("empty group does not register one", function() {
		const { group, canUndo } = new UndoManager();
		group( () => {} );
		assert.equal(get(canUndo), false);
	});

	it("empty group does not register two", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, "test", true);
		setting.set(2);
		group( () => {
		}, "test", true);
		undo();
		assert.equal(get(canUndo), false);
	});

	it("empty group does not register three", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, "test", true);
		setting.set(1);
		group( () => {
		});
		undo();
		assert.equal(get(canUndo), false);
	});

	it("empty group does not register four", function() {
		const { undoable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0);
		setting.set(1);
		group( () => {
		}, "test", true);
		undo();
		assert.equal(get(canUndo), false);
	});

	it("empty group does not register five", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0, true);
		group( () => {
			appearance.set(1);
		});
		assert.equal(get(canUndo), false);
	});

	it("empty group does not register six", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0, false);
		group( () => {
			appearance.set(1);
		});
		assert.equal(get(canUndo), false);
	});

	it("empty group does not register seven", function() {
		const { undoable, preservable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, "test", true);
		const appearance = preservable(1, true);
		setting.set(2);
		group( () => {
			appearance.set(3);
		});
		undo();
		assert.equal(get(canUndo), false);
	});

	it("empty group does not register eight", function() {
		const { undoable, preservable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0, "test", true);
		const appearance = preservable(1, false);
		setting.set(2);
		group( () => {
			appearance.set(3);
		});
		undo();
		assert.equal(get(canUndo), false);
	});

	it("empty group does not register nine", function() {
		const { undoable, preservable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(1, true);
		setting.set(2);
		group( () => {
			appearance.set(3);
		}, "test", true);
		undo();
		assert.equal(get(canUndo), false);
	});

	it("empty group does not register ten", function() {
		const { undoable, preservable, group, undo, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(1, false);
		setting.set(2);
		group( () => {
			appearance.set(3);
		}, "test", true);
		undo();
		assert.equal(get(canUndo), false);
	});

	it("register after one", function() {
		const { undoable, preservable, group, undo, redo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(1);
		group( () => {
			appearance.set(2);
		}, "test", true);
		group( () => {
			setting.set(3);
		}, "test", true);
		undo();
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(appearance), 2);
	});

	it("register after two", function() {
		const { undoable, preservable, group, undo, redo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(1);
		setting.set(2);
		group( () => {
			appearance.set(3);
		}, "test", true);
		group( () => {
			setting.set(4);
		}, "test", true);
		undo();
		assert.equal(get(appearance), 1);
		redo();
		assert.equal(get(appearance), 3);
	});
});