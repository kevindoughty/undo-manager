import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get } from "svelte/store";


describe("UNDOABLE", function() {
	it("one", function() {
		const { undoable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), false);
		setting.set(1);
		assert.equal(get(setting), 1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		setting.set(2);
		assert.equal(get(setting), 2);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		undo();
		assert.equal(get(setting), 1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		undo(); // one extra should have no effect
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(setting), 1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(setting), 1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(setting), 2);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		redo(); // one extra should have no effect
		assert.equal(get(setting), 2);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
	});

	it("two", function() { // description basic
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer,redoLocalizer);
		const setting = undoable(0, "change setting");
		setting.set(1);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		redo();
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
	});

	it("three", function() {
		const { undoable, undo, redo, canUndo, canRedo} = new UndoManager();
		const setting = undoable(0, "change setting");
		setting.set(1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		setting.set(2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		setting.set(3);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 3);
	});
});