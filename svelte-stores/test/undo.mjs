import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get } from "svelte/store";


describe("UNDO", function() {
	it("calling undo in a group will silently fail", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		
		const setting = undoable(0, "change setting", true);
		setting.set(1);
		
		const appearance = preservable(0, true);
		appearance.set(2);

		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 2);
		group( () => {
			undo();
		}, "change setting", false);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 2);

	});

	it("calling redo in a group will silently fail", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		
		const setting = undoable(0, "change setting", true);
		setting.set(1);
		
		const appearance = preservable(0, true);
		appearance.set(2);

		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 2);
		group( () => {
			redo();
		}, "change setting", false);
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 2);

	});
});