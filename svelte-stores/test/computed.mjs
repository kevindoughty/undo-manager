import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get, derived } from "svelte/store";


describe("DERIVED", function() { // formerly COMPUTED
	it("one", function() {
		const { undoable, preservable } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		const computed = derived(
			[setting, appearance],
			() => {
			return get(setting) + get(appearance);
		});
		assert.equal(get(computed), 0);
		setting.set(1);
		assert.equal(get(computed), 1);
		appearance.set(2);
		assert.equal(get(computed), 3);
	});
});