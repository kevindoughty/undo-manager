import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get } from "svelte/store";

describe("README", function() {
	it("the documentation is accurate", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		
		const setting = undoable(0, "change setting", true);
		setting.set(1);
		
		const appearance = preservable(0, true);
		appearance.set(2);
		setting.set(3);
		appearance.set(4);

		group( () => {
			setting.set(5);
			appearance.set(6);
		}, "change setting and more", true);
		group( () => {
			setting.set(7);
			appearance.set(8);
		}, "change setting and more", true);
		
		undo();
		assert.equal(get(setting), 3);
		assert.equal(get(appearance), 4);
		undo();
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 2);
		
		redo();
		assert.equal(get(setting), 3);
		assert.equal(get(appearance), 2);

		assert.equal(get(canUndo), true);

		assert.equal(get(canRedo), true);

		assert.equal(get(undoDescription), "Undo change setting");

		assert.equal(get(redoDescription), "Redo change setting and more");

		const dont = undoable("just"); // just don't
		dont.set("dont");
		undo();
		undo();// before it existed
		assert(get(dont), "just");
		redo();
		redo();

		group( () => {
			appearance.set(9); // will be registered as the after change value 
		}, "conceptual section changes", true);
		group( () => {
			setting.set(7); 
		}, "conceptual section changes", true); // coalesces
		undo();
		assert.equal(get(appearance), 2); // coalescing group did not affect before value
		redo();
		assert.equal(get(appearance), 9);

		group( () => {
			appearance.set(10);
		}, "conceptual section changes", true); // does not coalesce
		assert.equal(get(appearance), 10);
		undo();
		redo();
		assert.equal(get(appearance), 9); // changes are lost

	});
});