import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get } from "svelte/store";


describe("DESCRIPTION", function() {
	it("one", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0, "change setting1");
		const setting2 = undoable(0, "change setting2")
		const appearance1 = preservable("0");
		const appearance2 = preservable("1");
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		setting1.set(1);
		assert.equal(get(undoDescription), "undo change setting1");
		assert.equal(get(redoDescription), null);
		appearance1.set("1");
		assert.equal(get(undoDescription), "undo change setting1");
		assert.equal(get(redoDescription), null);
		setting2.set(2);
		assert.equal(get(undoDescription), "undo change setting2");
		assert.equal(get(redoDescription), null);
		appearance2.set("2");
		assert.equal(get(undoDescription), "undo change setting2");
		assert.equal(get(redoDescription), null);
		group( () => {
			setting1.set(11);
			setting2.set(22);
			appearance1.set("11");
			appearance2.set("22");
		}, "change both setting1 and setting2");
		assert.equal(get(undoDescription), "undo change both setting1 and setting2");
		assert.equal(get(redoDescription), null);
		group( () => {
			setting1.set(111);
			setting2.set(222);
			appearance1.set("111");
			appearance2.set("222");
		}, "change both setting1 and setting2");
		assert.equal(get(undoDescription), "undo change both setting1 and setting2");
		assert.equal(get(redoDescription), null);
		group( () => {
			setting1.set(1111);
			setting2.set(2222);
			appearance1.set("1111");
		}, "change both setting1 and setting2 differently", "coalesce one");
		assert.equal(get(undoDescription), "undo change both setting1 and setting2 differently");
		assert.equal(get(redoDescription), null);
		group( () => {
			setting1.set(11111);
			setting2.set(22222);
			appearance2.set("22222");
		}, "change both setting1 and setting2 differently", "coalesce one");
		assert.equal(get(undoDescription), "undo change both setting1 and setting2 differently");
		assert.equal(get(redoDescription), null);
		group( () => {
			setting1.set(111111);
			setting2.set(222222);
		}, "change both setting1 and setting2 without changing appearance", "coalesce two");
		assert.equal(get(undoDescription), "undo change both setting1 and setting2 without changing appearance");	
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), "undo change both setting1 and setting2 differently");
		assert.equal(get(redoDescription), "redo change both setting1 and setting2 without changing appearance");
		undo();
		assert.equal(get(undoDescription), "undo change both setting1 and setting2");
		assert.equal(get(redoDescription), "redo change both setting1 and setting2 differently");
		undo();
		assert.equal(get(undoDescription), "undo change both setting1 and setting2");
		assert.equal(get(redoDescription), "redo change both setting1 and setting2");
		undo();
		assert.equal(get(undoDescription), "undo change setting2");
		assert.equal(get(redoDescription), "redo change both setting1 and setting2");
		undo();
		assert.equal(get(undoDescription), "undo change setting1");
		assert.equal(get(redoDescription), "redo change setting2");
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting1");
		undo(); // one extra should have no effect
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting1");
		redo();
		assert.equal(get(undoDescription), "undo change setting1");
		assert.equal(get(redoDescription), "redo change setting2");
		redo();
		assert.equal(get(undoDescription), "undo change setting2");
		assert.equal(get(redoDescription), "redo change both setting1 and setting2");
		redo();
		assert.equal(get(undoDescription), "undo change both setting1 and setting2");
		assert.equal(get(redoDescription), "redo change both setting1 and setting2");
		redo();
		assert.equal(get(undoDescription), "undo change both setting1 and setting2");
		assert.equal(get(redoDescription), "redo change both setting1 and setting2 differently");
		redo();
		assert.equal(get(undoDescription), "undo change both setting1 and setting2 differently");
		assert.equal(get(redoDescription), "redo change both setting1 and setting2 without changing appearance");
		redo();
		assert.equal(get(undoDescription), "undo change both setting1 and setting2 without changing appearance");
		assert.equal(get(redoDescription), null);
		redo(); // one extra should have no effect
		assert.equal(get(undoDescription), "undo change both setting1 and setting2 without changing appearance");
		assert.equal(get(redoDescription), null);
	});

	it("二番", function() {
		const undoLocalizer = function(description) {
			return description + "を取り消す";
		};
		const redoLocalizer = function(description) {
			return description + "をやり直す";
		};
		const { undoable, preservable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0, "設定 1 の変更");
		const setting2 = undoable(0, "設定 2 の変更")
		const appearance1 = preservable("0");
		const appearance2 = preservable("1");
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		setting1.set(1);
		assert.equal(get(undoDescription), "設定 1 の変更を取り消す");
		assert.equal(get(redoDescription), null);
		appearance1.set("1");
		assert.equal(get(undoDescription), "設定 1 の変更を取り消す");
		assert.equal(get(redoDescription), null);
		setting2.set(2);
		assert.equal(get(undoDescription), "設定 2 の変更を取り消す");
		assert.equal(get(redoDescription), null);
		appearance2.set("2");
		assert.equal(get(undoDescription), "設定 2 の変更を取り消す");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), "設定 1 の変更を取り消す");
		assert.equal(get(redoDescription), "設定 2 の変更をやり直す");
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "設定 1 の変更をやり直す");
		redo();
		assert.equal(get(undoDescription), "設定 1 の変更を取り消す");
		assert.equal(get(redoDescription), "設定 2 の変更をやり直す");
		redo();
		assert.equal(get(undoDescription), "設定 2 の変更を取り消す");
		assert.equal(get(redoDescription), null);
	});

	it("language chooser declaration", function() {
		function undoLocalizer(description) {
			const value = get(language);
			const position = languageEnum[value];
			return undoPrepend[position] + description[position] + undoAppend[position];
		};
		function redoLocalizer(description) {
			const value = get(language);
			const position = languageEnum[value];
			return redoPrepend[position] + description[position] + redoAppend[position];
		};
		const languageEnum = {
			"English": 0,
			"日本語": 1
		};
		const undoPrepend = [
			"Undo ",
			""
		];
		const undoAppend = [
			"",
			"を取り消す"
		];
		const redoPrepend = [
			"Redo ",
			""
		];
		const redoAppend = [
			"",
			"をやり直す"
		];
		const { undoable, preservable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, ["change setting", "設定の変更"]);
		const language = preservable("English");
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		setting.set(1);
		assert.equal(get(setting), 1);
		assert.equal(get(language), "English");
		assert.equal(get(undoDescription), "Undo change setting"); // ? NaN
		assert.equal(get(redoDescription), null);
		language.set("日本語");
		assert.equal(get(setting), 1);
		assert.equal(get(language), "日本語");
		assert.equal(get(undoDescription), "設定の変更を取り消す");
		assert.equal(get(redoDescription), null);
		setting.set(2);
		assert.equal(get(setting), 2);
		assert.equal(get(language), "日本語");
		assert.equal(get(undoDescription), "設定の変更を取り消す");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(setting), 1);
		assert.equal(get(language), "日本語");
		assert.equal(get(undoDescription), "設定の変更を取り消す");
		assert.equal(get(redoDescription), "設定の変更をやり直す");
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(language), "English");
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo change setting");
		redo();
		assert.equal(get(setting), 1);
		assert.equal(get(language), "English");
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), "Redo change setting");
		redo();
		assert.equal(get(setting), 2);
		assert.equal(get(language), "日本語");
		assert.equal(get(undoDescription), "設定の変更を取り消す");
		assert.equal(get(redoDescription), null);
	});

	it("language chooser expression", function() {
		const undoLocalizer = function(description) {
			const value = get(language);
			const position = languageEnum[value];
			return undoPrepend[position] + description[position] + undoAppend[position];
		};
		const redoLocalizer = function(description) {
			const value = get(language);
			const position = languageEnum[value];
			return redoPrepend[position] + description[position] + redoAppend[position];
		};
		const languageEnum = {
			"English": 0,
			"日本語": 1
		};
		const undoPrepend = [
			"Undo ",
			""
		];
		const undoAppend = [
			"",
			"を取り消す"
		];
		const redoPrepend = [
			"Redo ",
			""
		];
		const redoAppend = [
			"",
			"をやり直す"
		];
		const { undoable, preservable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, ["change setting", "設定の変更"]);
		const language = preservable("English");
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		setting.set(1);
		assert.equal(get(setting), 1);
		assert.equal(get(language), "English");
		assert.equal(get(undoDescription), "Undo change setting"); // ? NaN
		assert.equal(get(redoDescription), null);
		language.set("日本語");
		assert.equal(get(setting), 1);
		assert.equal(get(language), "日本語");
		assert.equal(get(undoDescription), "設定の変更を取り消す");
		assert.equal(get(redoDescription), null);
		setting.set(2);
		assert.equal(get(setting), 2);
		assert.equal(get(language), "日本語");
		assert.equal(get(undoDescription), "設定の変更を取り消す");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(setting), 1);
		assert.equal(get(language), "日本語");
		assert.equal(get(undoDescription), "設定の変更を取り消す");
		assert.equal(get(redoDescription), "設定の変更をやり直す");
		undo();
		assert.equal(get(setting), 0);
		assert.equal(get(language), "English");
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo change setting");
		redo();
		assert.equal(get(setting), 1);
		assert.equal(get(language), "English");
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), "Redo change setting");
		redo();
		assert.equal(get(setting), 2);
		assert.equal(get(language), "日本語");
		assert.equal(get(undoDescription), "設定の変更を取り消す");
		assert.equal(get(redoDescription), null);
	});

	it("six", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const string = undoable("");
		const setString = function(what) {
			group( () => {
				string.set(what);
			}, "change string to " + what, "string");
		};
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		setString("a");
		assert.equal(get(undoDescription), "undo change string to a");
		assert.equal(get(redoDescription), null);
		setString("as");
		assert.equal(get(undoDescription), "undo change string to as");
		assert.equal(get(redoDescription), null);
		setString("asd");
		assert.equal(get(undoDescription), "undo change string to asd");
		assert.equal(get(redoDescription), null);
		setString("asdf");
		assert.equal(get(undoDescription), "undo change string to asdf");
		assert.equal(get(redoDescription), null);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change string to asdf");
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		redo();
		assert.equal(get(undoDescription), "undo change string to asdf");
		assert.equal(get(redoDescription), null);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		setString("asdf ");
		assert.equal(get(undoDescription), "undo change string to asdf ");
		assert.equal(get(redoDescription), null);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		setString("asdf z");
		assert.equal(get(undoDescription), "undo change string to asdf z");
		assert.equal(get(redoDescription), null);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		setString("asdf zx");
		assert.equal(get(undoDescription), "undo change string to asdf zx");
		assert.equal(get(redoDescription), null);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		setString("asdf zxc");
		assert.equal(get(undoDescription), "undo change string to asdf zxc");
		assert.equal(get(redoDescription), null);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		setString("asdf zxcv");
		assert.equal(get(undoDescription), "undo change string to asdf zxcv");
		assert.equal(get(redoDescription), null);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		undo();
		assert.equal(get(undoDescription), "undo change string to asdf");
		assert.equal(get(redoDescription), "redo change string to asdf zxcv");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change string to asdf");
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
	});

	it("seven", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		const setSettingOne = function(what) {
			group( () => {
				setting1.set(what);
			}, "change setting one to " + what, "setting1");
		};
		const setSettingTwo = function(what) {
			group( () => {
				setting2.set(what);
			}, "change setting two to " + what, "setting2");
		};
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		setSettingOne(1);
		assert.equal(get(undoDescription), "undo change setting one to 1");
		assert.equal(get(redoDescription), null);
		setSettingTwo(2);
		assert.equal(get(undoDescription), "undo change setting two to 2");
		assert.equal(get(redoDescription), null);
		setSettingOne(3);
		assert.equal(get(undoDescription), "undo change setting one to 3");
		assert.equal(get(redoDescription), null);
		setSettingOne(4);
		assert.equal(get(undoDescription), "undo change setting one to 4");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), "undo change setting two to 2");
		assert.equal(get(redoDescription), "redo change setting one to 4");
		undo();
		assert.equal(get(undoDescription), "undo change setting one to 1");
		assert.equal(get(redoDescription), "redo change setting two to 2");
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting one to 1");
		redo();
		assert.equal(get(undoDescription), "undo change setting one to 1");
		assert.equal(get(redoDescription), "redo change setting two to 2");
		redo();
		assert.equal(get(undoDescription), "undo change setting two to 2");
		assert.equal(get(redoDescription), "redo change setting one to 4");
		redo();
		assert.equal(get(undoDescription), "undo change setting one to 4");
		assert.equal(get(redoDescription), null);
	});

	it("eight", function() { // Should not coalesce after undo, but this one ensures that input stack is also reset
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, true);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		appearance.set(4); // interrupting
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 4);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 4);
		group( () => {
			setting.set(3);
			appearance.set(3);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 3);
		assert.equal(get(appearance), 3);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 4);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
	});

	it("eight alternate", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, false);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		appearance.set(4); // not interrupting
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 4);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
		group( () => {
			setting.set(3);
			appearance.set(3);
		}, "change setting", "setting");
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 3);
		assert.equal(get(appearance), 3);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
	});

	it("nine", function() { // null changeDescription will produce null undoDescription
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, null, true);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, null, true); // should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
	});

	it("ten", function() { // undefined changeDescription will produce null undoDescription
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, undefined, true);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, undefined, true); // should not coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
	});

	it("group false change description is valid and coalesces if key is true", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, false, true);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo false");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, false, true); // should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo false");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo false");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
	});

	it("undoable false change description is valid and coalesces if key is true", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, false, true);
		setting.set(1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo false");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		setting.set(2); // should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo false");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo false");
		assert.equal(get(setting), 0);
	});

	it("group zero change description is valid and coalesces if key is true", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.set(1);
			appearance.set(1);
		}, 0, true);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance), 1);
		group( () => {
			setting.set(2);
			appearance.set(2);
		}, 0, true); // should coalesce
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo 0");
		assert.equal(get(setting), 0);
		assert.equal(get(appearance), 0);
	});

	it("maxCount and undoDescription", function() {
		const maxCount = 2;
		const undoLocalizer = function(description) {
			return "Undo " + description;
		} 
		const { undoable, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, null, maxCount);
		const setting = undoable(0, "change setting");
		setting.set(1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		setting.set(2);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		setting.set(3);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		undo(); // extra does nothing
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 2);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 3);
		redo(); // extra does nothing
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 3);
	});

	it("maxCount and redoDescription", function() {
		const maxCount = 2;
		const redoLocalizer = function(description) {
			return "Redo " + description;
		} 
		const { undoable, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(null, redoLocalizer, maxCount);
		const setting = undoable(0, "change setting");
		setting.set(1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		setting.set(2);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		setting.set(3);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo change setting");
		assert.equal(get(setting), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo change setting");
		assert.equal(get(setting), 1);
		undo(); // extra does nothing
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo change setting");
		assert.equal(get(setting), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo change setting");
		assert.equal(get(setting), 2);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 3);
		redo(); // extra does nothing
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 3);
	});

	it("maxCount and undoDescription and redoDescription", function() {
		const maxCount = 2;
		const undoLocalizer = function(description) {
			return "Undo " + description;
		}
		const redoLocalizer = function(description) {
			return "Redo " + description;
		}
		const { undoable, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer, maxCount);
		const setting = undoable(0, "change setting");
		setting.set(1);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		setting.set(2);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		setting.set(3);
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), "Redo change setting");
		assert.equal(get(setting), 2);
		undo();
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo change setting");
		assert.equal(get(setting), 1);
		undo(); // extra does nothing
		assert.equal(get(canUndo), false);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo change setting");
		assert.equal(get(setting), 1);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), true);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), "Redo change setting");
		assert.equal(get(setting), 2);
		redo();
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 3);
		redo(); // extra does nothing
		assert.equal(get(canUndo), true);
		assert.equal(get(canRedo), false);
		assert.equal(get(undoDescription), "Undo change setting");
		assert.equal(get(redoDescription), null);
		assert.equal(get(setting), 3);
	});

	it("null undoLocalizer", function() {
		const undoLocalizer = null;
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting.set(1);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		redo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
	});

	it("null redoLocalizer", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = null;
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting.set(1);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		redo();
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
	});

	it("undefined undoLocalizer", function() { // not enumerable so have to set and check
		const undoLocalizer = undefined;
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting.set(1);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "redo change setting");
		redo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
	});

	it("undefined redoLocalizer", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = undefined;
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting.set(1);
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		redo();
		assert.equal(get(undoDescription), "undo change setting");
		assert.equal(get(redoDescription), null);
	});

	it("undoable zero change description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, 0);
		setting.set(1);
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo 0");
		redo();
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
	});

	it("undoable null description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, null);
		setting.set(1);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		redo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
	});

	it("undoable undefined description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, undefined);
		setting.set(1);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		redo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
	});

	it("undoable false description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, false);
		setting.set(1);
		assert.equal(get(undoDescription), "Undo false");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo false");
		redo();
		assert.equal(get(undoDescription), "Undo false");
		assert.equal(get(redoDescription), null);
	});

	it("undoable non-falsy description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "0");
		setting.set(1);
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo 0");
		redo();
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
	});

	it("group overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.set(1);
		}, "change setting override");
		assert.equal(get(undoDescription), "Undo change setting override");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo change setting override");
		redo();
		assert.equal(get(undoDescription), "Undo change setting override");
		assert.equal(get(redoDescription), null);
	});

	it("group zero description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting.set(1);
		}, 0);
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo 0");
		redo();
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
	});

	it("group null description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting.set(1);
		}, null);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		redo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
	});

	it("group false description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting.set(1);
		}, false);
		assert.equal(get(undoDescription), "Undo false");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo false");
		redo();
		assert.equal(get(undoDescription), "Undo false");
		assert.equal(get(redoDescription), null);
	});

	it("group non-falsy description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting.set(1);
		}, "0");
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo 0");
		redo();
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
	});

	it("group undefined description overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.set(1);
		});
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		redo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
	});

	it("group zero description is valid and overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.set(1);
		}, 0);
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo 0");
		redo();
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
	});

	it("group null description overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.set(1);
		}, null);
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
		redo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), null);
	});

	it("group false description is valid and overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.set(1);
		}, false);
		assert.equal(get(undoDescription), "Undo false");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo false");
		redo();
		assert.equal(get(undoDescription), "Undo false");
		assert.equal(get(redoDescription), null);
	});

	it("group non-falsy description overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.set(1);
		}, "0");
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
		undo();
		assert.equal(get(undoDescription), null);
		assert.equal(get(redoDescription), "Redo 0");
		redo();
		assert.equal(get(undoDescription), "Undo 0");
		assert.equal(get(redoDescription), null);
	});
});