import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get, derived } from "svelte/store";

describe("USAGE", function() {
	it("conditional", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		}
		const redoLocalizer = function(description) {
			return "Undo " + description;
		}
		const { undoable, preservable, group, undoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		const setter = function(a,b,c) {
			const message = "change setting to " + a;
			const coalescer = Symbol();
			group( () => {
				setting.set(a);
				appearance1.set(b);
			}, message, coalescer);
			if (c !== undefined) {
				group( () => {
					appearance2.set(c);
				}, null, coalescer); // does not erase undo description
			}
		};
		setter(1,1);
		assert.equal(get(setting), 1);
		assert.equal(get(appearance1), 1);
		assert.equal(get(appearance2), 0);
		assert.equal(get(undoDescription), "Undo change setting to 1");
		
		setter(2,2,2);
		assert.equal(get(setting), 2);
		assert.equal(get(appearance1), 2);
		assert.equal(get(appearance2), 2);
		assert.equal(get(undoDescription), "Undo change setting to 2");

		setter(3,3);
		assert.equal(get(setting), 3);
		assert.equal(get(appearance1), 3);
		assert.equal(get(appearance2), 2);
		assert.equal(get(undoDescription), "Undo change setting to 3");		
	});


	it("extra", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);

		const volume = undoable(0);
		const volumeSymbol = Symbol();
		const crankVolume = function(what) {
			const description = "crank the volume to " + what;
			group( () => {
				volume.set(what);
			}, description, volumeSymbol);
		}
		crankVolume(10);
		assert.equal(get(undoDescription), "Undo crank the volume to 10");

		group( () => {
			appearance.set(-1);
			setting.set(-1);
			group( () => {
				setting.set(-2);
				appearance.set(-2);
			}, "ignored", Symbol());
		}, null, volumeSymbol); // does not null out undo description
		assert.equal(get(undoDescription), "Undo crank the volume to 10");
		crankVolume(11); // coalesces
		assert.equal(get(undoDescription), "Undo crank the volume to 11");
		undo();
		assert.equal(get(volume), 0);
		assert.equal(get(redoDescription), "Redo crank the volume to 11");

		appearance.set(-3); // lost
		redo();
		assert.equal(get(appearance), -2);
		appearance.set(-4); // lost
		undo();
		redo();
		assert.equal(get(appearance), -2);

		const color = derived(
			volume,
			() => {
				if (get(volume) < 0) return "black";
				if (get(volume) < 6) return "green";
				if (get(volume) < 9) return "yellow";
				if (get(volume) < 11) return "red";
				return "purple";
			}
		);
		assert.equal(get(color), "purple");
		volume.subscribe(value => {
			assert.equal(value, 11, "no way don't touch the volume");
		});
		// effect( () => { // different
		// 	assert.equal(get(volume), 11, "no way don't touch the volume");
		// });
		// // undo() // don't do it
	});
});