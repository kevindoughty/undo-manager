import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { get } from "svelte/store";


describe("BATCH", function() {
	it("wontfix", function() { // Can't fix. Expected behavior. Don't use batch, use group. See first in test/group.js
		// const { undoable, undo, canUndo, canRedo } = new UndoManager();
		// const setting1 = undoable(0);
		// const setting2 = undoable(0);
		// assert.equal(get(canUndo), false);
		// assert.equal(get(canRedo), false);
		// assert.equal(get(setting1), 0);
		// assert.equal(get(setting2), 0);
		// batch(()=> {
		// 	setting1.set(1);
		// 	setting2.set(2);
		// });
		// assert.equal(get(canUndo), true);
		// assert.equal(get(canRedo), false);
		// assert.equal(get(setting1), 1);
		// assert.equal(get(setting2), 2);
		// undo();
		// assert.equal(get(canUndo), true);
		// assert.equal(get(canRedo), true);
		// assert.equal(get(setting1), 1);
		// assert.equal(get(setting2), 0);
		// undo();
		// assert.equal(get(canUndo), false);
		// assert.equal(get(canRedo), true);
		// assert.equal(get(setting1), 0);
		// assert.equal(get(setting2), 0);
		assert.equal("not applicable", "not applicable");
	});

	it("batching multiple undos", function() {
		// const { undoable, preservable, group, undo, redo } = new UndoManager();
		// const setting = undoable(1);
		// let sum = 0;
		// //effect(() => sum += setting.value);
		// setting.subscribe( value => {
		// 	sum += get(setting);
		// });
		// setting.set(2);
		// setting.set(3);
		// batch( () => { // must use batch, not group
		// 	undo();
		// 	undo();
		// });
		// assert.equal(get(setting), 1);
		// assert.equal(sum, 7);
		assert.equal("not applicable", "not applicable");
	});

	it("not batching multiple undos", function() {
		const { undoable, preservable, group, undo, redo } = new UndoManager();
		const setting = undoable(1);
		let sum = 0;
		setting.subscribe( value => {
			sum += get(setting);
		});
		setting.set(2);
		setting.set(3);
		undo();
		undo();
		assert.equal(get(setting), 1);
		assert.equal(sum, 9); // same as Preact Signals
	});
});