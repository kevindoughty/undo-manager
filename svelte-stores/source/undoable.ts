import { get, Writable, writable, Updater, Subscriber } from "svelte/store";
import { Context } from "./context.js";

/**
 * An undoable object
 * @typedef {function} Undoable
 * @param {*} value
 * @param {*} description
 * @param {*} coalescing
 */
class Undoable<T> {
	private context:Context;
	private description:any;
	private coalescing:any;
	private id:number;
	writable:Writable<T>;

	constructor(context:Context, value:T, description?:any, coalescing?:any) {
		this.context = context;
		this.description = description;
		this.coalescing = coalescing;
		this.id = context.registerUndoable();
		this.writable = writable(value);
	}

	private registerUndo(instance:Undoable<T>, value:T) {
		const undoer = () => {
			instance.context.group( () => {
				const original:T = get(instance.writable);
				instance.writable.set(value);
				instance.registerUndo(instance, original);
			}, instance.description);
		};
		this.context.stack.registerUndoerInvocation(undoer, instance.id);
	}
	
	subscribe(callback:Subscriber<T>, invalidate?:()=>void) {
		return this.writable.subscribe(callback, invalidate);
	}

	set(value:T) {
		let actualCoalescing = this.coalescing;
		if (this.coalescing === true && (this.description === null || this.description === undefined)) {
			actualCoalescing = this;
		}
		const original = get(this.writable);
		if (value !== original) {
			this.context.group( () => {
				this.writable.set(value);
				this.registerUndo(this, original);
			}, this.description, actualCoalescing);
		}
	}

	update(updater: Updater<T>) {
		let actualCoalescing = this.coalescing;
		if (this.coalescing === true && (this.description === null || this.description === undefined)) {
			actualCoalescing = this;
		}
		const original = get(this.writable);
		this.writable.update(updater);
		const value = get(this.writable);
		if (value !== original) {
			this.context.group( () => {
				this.writable.set(value);
				this.registerUndo(this, original);
			}, this.description, actualCoalescing);
		}
	}
}

export { Undoable };