import { Writable, writable, Updater, Subscriber } from "svelte/store";
import { Context } from "./context.js";

/**
 * A preservable object
 * @typedef {function} Preservable
 * @param {*} value
 * @param {boolean} interrupting
 */
class Preservable<T> {
	private context:Context;
	private interrupting:boolean;
	writable:Writable<T>;

	constructor(context:Context, value:T, interrupting?:boolean) {
		this.context = context;
		this.interrupting = interrupting || false;
		this.writable = writable(value);
		context.registerPreservable(this, this.registerBefore.bind(this), this.registerAfter.bind(this));
	}

	private registerBefore(value:T) {
		const invocation = (direction:number) => {
			if (direction < 0) { // undo
				this.set(value);
			}
			return this.registerBefore(value);
		};
		return invocation;
	}
	
	private registerAfter(value:T) {
		const invocation = (direction:number) => {
			if (direction > 0) { // redo
				this.set(value);
			}
			return this.registerAfter(value);
		};
		return invocation;
	}

	subscribe(callback:Subscriber<T>, invalidate?:()=>void) {
		return this.writable.subscribe(callback, invalidate);
	}

	set(value:T) {
		if (this.interrupting) this.context.makeNonCoalescingChange();
		this.writable.set(value);
	}

	update(updater: Updater<T>) {
		if (this.interrupting) this.context.makeNonCoalescingChange();
		this.writable.update(updater);
	}
}

export { Preservable };