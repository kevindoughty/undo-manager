import type { Readable, Updater, Subscriber, Unsubscriber } from "svelte/store";
import { Undoable as UndoableClass } from "./undoable.js";
import { Preservable as PreservableClass } from "./preservable.js";
import { Context } from "./context.js";
import type { Localizer, Undoable, Preservable } from "./types.js";

/**
 * An object that provides change and undo stack management
 * @constructor
 * @param {Localizer} undoLocalizer
 * @param {Localizer} redoLocalizer
 * @param {number} maxCount
 * @returns Primitives used for managing the undo stack
 */
class UndoManager {
	undoable:<T=any>(value:T, description?:any, coalescing?:any) => Undoable<T>;
	preservable:<T=any>(value:T, interrupting?:any) => Preservable<T>;
	group:(callback:()=>void, description?:any, coalescing?:any) => void;
	undo:()=>void;
	redo:()=>void;
	readonly canUndo:Readable<boolean>;
	readonly canRedo:Readable<boolean>;
	readonly undoDescription:Readable<string | undefined>;
	readonly redoDescription:Readable<string | undefined>;
	
	constructor(undoLocalizer?:Localizer, redoLocalizer?:Localizer, maxCount?:number) {
		const context = new Context(undoLocalizer, redoLocalizer, maxCount);
		this.undoable = undoable(context);
		this.preservable = preservable(context);
		this.group = group(context);
		this.undo = undo(context);
		this.redo = redo(context);
		this.canUndo = canUndo(context);
		this.canRedo = canRedo(context);
		this.undoDescription = undoDescription(context);
		this.redoDescription = redoDescription(context);
	}
}

function undoable(context:Context) { // private
	return function<T=any>(value:T, description?:any, coalescing?:any) { // public
		const instance = new UndoableClass(context, value, description, coalescing);
		const subscribe:(run:Subscriber<T>, invalidate?:()=>void) => Unsubscriber = instance.subscribe.bind(instance);
		const set:(value:T)=>void = instance.set.bind(instance);
		const update:(updater:Updater<T>)=>void = instance.update.bind(instance);
		const object: Undoable = { subscribe, set, update };
		return object;
	}
}

function preservable(context:Context) { // private
	return function<T=any>(value:T, interrupting?:boolean) { // public
		const instance = new PreservableClass(context, value, interrupting);
		const subscribe:(run:Subscriber<T>, invalidate?:()=>void) => Unsubscriber = instance.subscribe.bind(instance);
		const set:(value:T)=>void = instance.set.bind(instance);
		const update:(updater:Updater<T>)=>void = instance.update.bind(instance);
		const object: Preservable = { subscribe, set, update };
		return object;
	}
}

function group(context:Context) { // private
/**
 * A function that permits batch changes
 * @param {function} callback
 * @param {*} description
 * @param {*} coalescing
 */
	return function(callback:()=>void, description?:any, coalescing?:any) { // public
		context.group(callback, description, coalescing);
	}
}

const undo = function(context:Context) { // private
/**
 * A function which restores state to its previous value
 */
	return function() { // public
		context.undo();
	}
};

const redo = function(context:Context) { // private
/**
 * A function which restores state to its next value
 */
	return function() { // public
		context.redo();
	}
};

const canUndo = function(context:Context) { // private
/**
 * A Readable store whose getter provides if undo is possible
 */
	return context.stack.canUndo; // public
};

const canRedo = function(context:Context) { // private
/**
 * A Readable store whose getter provides if redo is possible
 */
	return context.stack.canRedo; // public
};

const undoDescription = function(context:Context) { // private
/**
 * A Readable store whose getter provides a change description
 */
	return context.stack.undoDescription; // public
};

const redoDescription = function(context:Context) { // private
/**
 * A Readable store whose getter provides a change description
 */
	return context.stack.redoDescription; // public
};


export { UndoManager };