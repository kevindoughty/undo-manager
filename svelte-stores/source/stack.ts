import { get, derived, Readable, Writable, writable } from "svelte/store";
import { Entry } from "./entry.js";
import type { Localizer } from "./types.js";
import type { Invocation, Undoer } from "./entry.js";

class Stack {
	private maxCount:number;
	private next: Entry;
	private staging: Entry | undefined;
	private undoStack:Writable<Array<Entry>>;
	private redoStack:Writable<Array<Entry>>;
	private undoDescriptions:Writable<Array<any>>;
	private redoDescriptions:Writable<Array<any>>;
	canUndo:Readable<boolean>;
	canRedo:Readable<boolean>;
	undoDescription:Readable<string | undefined>;
	redoDescription:Readable<string | undefined>;

	constructor(undoLocalizer?:Localizer, redoLocalizer?:Localizer, maxCount?: number) {
		if (!undoLocalizer) undoLocalizer = () => undefined;
		if (!redoLocalizer) redoLocalizer = () => undefined;
		if (!Number.isInteger(maxCount) || !maxCount || maxCount < 0) maxCount = Infinity;
		this.maxCount = maxCount;
		this.next = this.empty();
		this.staging = this.empty();
		this.undoStack = writable([]);
		this.redoStack = writable([]);
		this.undoDescriptions = writable([]);
		this.redoDescriptions = writable([]);
		
		this.canUndo = derived(
			this.undoStack,
			() => {
				const undoStackValue:Array<Entry> = get(this.undoStack);
				return undoStackValue.length > 0;
			}
		);

		this.canRedo = derived(
			this.redoStack,
			() => {
				const redoStackValue:Array<Entry> = get(this.redoStack);
				return redoStackValue.length > 0;
			}
		);

		this.undoDescription = derived(
			this.undoDescriptions,
			() => {
				const input:Array<any> = get(this.undoDescriptions);
				const length = input.length;
				const value = length ? input[length-1] : undefined;
				if (value === null || value === undefined) return undefined;
				return undoLocalizer(value);
			}
		);

		this.redoDescription = derived(
			this.redoDescriptions,
			() => {
				const input:Array<any> = get(this.redoDescriptions);
				const length = input.length;
				const value = length ? input[length-1] : undefined;
				if (value === null || value === undefined) return undefined;
				return redoLocalizer(value);
			}
		);
	}

	private empty() {
		return new Entry();
	}

	willUndo() { // pop undo stack to staging
		const undoPreviousValue:Array<Entry> = get(this.undoStack);
		const undoNext = undoPreviousValue.slice(0);
		this.staging = undoNext.pop();
		this.undoStack.set(undoNext);
		this.next = this.empty();
		const redoStackValue:Array<Entry> = get(this.redoStack);
		const redoNext = [ ...redoStackValue, this.next ];
		this.redoStack.set(redoNext);

		const undoDescriptionsValue:Array<any> = get(this.undoDescriptions);
		const undoDescriptionsNext = undoDescriptionsValue.slice(0);
		const currentUndoDescription = undoDescriptionsNext.pop();
		this.undoDescriptions.set(undoDescriptionsNext);
		const redoDescriptionsValue:Array<any> = get(this.redoDescriptions);
		this.redoDescriptions.set([ ...redoDescriptionsValue, currentUndoDescription ]);
	}

	willRedo() { // pop redo stack to staging
		const redoPreviousValue:Array<Entry> = get(this.redoStack);
		const redoNext = redoPreviousValue.slice(0);
		this.staging = redoNext.pop();
		this.redoStack.set(redoNext);
		this.next = this.empty();
		const undoPreviousValue:Array<Entry> = get(this.undoStack);
		const undoNext = [ ...undoPreviousValue, this.next ];
		this.undoStack.set(undoNext);
		
		const redoDescriptionsValue:Array<any> = get(this.redoDescriptions);
		const redoDescriptionsNext = [ ...redoDescriptionsValue ];
		const currentRedoDescription = redoDescriptionsNext.pop();
		this.redoDescriptions.set(redoDescriptionsNext);
		const undoDescriptionsValue:Array<any> = get(this.undoDescriptions);
		this.undoDescriptions .set([ ...undoDescriptionsValue, currentRedoDescription ]);
	}

	changeReset() { // before change
		this.next = this.empty();
		this.redoDescriptions.set([]);
	}

	coalesceReset() { // wipe existing after state for recapture
		const nextNext = Entry.clone(this.next);
		nextNext.after = [];
		this.next = nextNext;
	}

	afterChange(changeDescription?:any, coalesce?:any) {
		if (this.maxCount > 0 && Object.keys(this.next.invocations).length) {
			if (coalesce) {
				const undoStackValue:Array<Entry> = get(this.undoStack);
				const nextStack = undoStackValue.slice(0);
				nextStack.pop();
				nextStack.push(this.next);
				this.undoStack.set(nextStack);
				const undoDescriptionsValue:Array<any> = get(this.undoDescriptions);
				const nextInput = undoDescriptionsValue.slice(0);
				if (changeDescription !== undefined && changeDescription !== null) {
					nextInput.pop();
					nextInput.push(changeDescription);
				}
				this.undoDescriptions.set(nextInput);
			} else {
				const undoStackValue:Array<Entry> = get(this.undoStack);
				const undoStackNext = undoStackValue.slice(0);
				const undoDescriptionsValue:Array<any> = get(this.undoDescriptions);
				const undoDescriptionsNext = undoDescriptionsValue.slice(0);
				if (undoStackNext.length === this.maxCount) {
					undoStackNext.shift();
					undoDescriptionsNext.shift();
				}
				if (undoStackNext.length < this.maxCount) {
					undoStackNext.push(this.next);
					undoDescriptionsNext.push(changeDescription);
				}
				this.undoStack.set(undoStackNext);
				this.redoStack.set([]);
				this.undoDescriptions.set(undoDescriptionsNext);
				this.redoDescriptions.set([]);
			}
		}
	}

	registerBeforeInvocation(invocation:Invocation) {
		this.next.before.push(invocation);
	}

	registerUndoerInvocation(undoer:Undoer, key:number) {
		const existing = this.next.invocations[key]; // don't overwrite existing when coalescing
		if (existing === undefined) {
			this.next.invocations[key] = undoer;
		}
	}

	registerAfterInvocation(invocation:Invocation) {
		this.next.after.push(invocation);
	}

	applier(direction:number) {
		if (this.staging !== undefined) {
			const before = this.staging.before;
			before.forEach( invocation => {
				this.next.before.push(invocation(direction));
			});
			const invocations = this.staging.invocations;
			Object.keys(this.staging.invocations).forEach( key => {
				const invocation = invocations[Number(key)];
				invocation(direction);
			});
			const after = this.staging.after;
			after.forEach( invocation => {
				this.next.after.push(invocation(direction));
			});
		}
	}
}

export { Stack };