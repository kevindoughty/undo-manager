type Invocation = (direction:number) => Invocation;
type Undoer = (direction:number) => void;

class Entry {
	before: Array<Invocation>;
	invocations: Record<number, Undoer>;
	after: Array<Invocation>;

	constructor() {
		this.before = [];
		this.invocations = Object.create(null);
		this.after = [];
	}
	static clone(entry:Entry) {
		const copy = new Entry();
		copy.before = entry.before;
		copy.invocations = entry.invocations;
		copy.after = entry.after;
		return copy;
	}
}

export { Entry, type Invocation, type Undoer };