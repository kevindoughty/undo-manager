import { Readable, Writable } from "svelte/store";

/**
 * An optional callback passed to the UndoManager contstructor.
 * Generates localized strings to describe changes.
 * @typedef {function} Localizer
 * @param {*} description
 * @returns {(string | undefined)} A localized string describing a change
 */
type Localizer = (description:any) => string | undefined;

interface Undoable<T = any> extends Writable<T> {};

interface Preservable<T = any> extends Writable<T> {};

/**
 * An object that provides change and undo stack management
 * @constructor
 * @param {Localizer} undoLocalizer
 * @param {Localizer} redoLocalizer
 * @param {number} maxCount
 * @returns Primitives used for managing the undo stack
 */
declare class UndoManager {
	constructor(undoLocalizer?:Localizer, redoLocalizer?:Localizer, maxCount?:number);
	undoable:<T=any>(value:any, description?:any, coalescing?:any) => Undoable<T>;
	preservable:<T=any>(value:any, interrupting?:any)=> Preservable<T>;
	group:(callback:()=>void, description?:any, coalescing?:any) => void;
	undo:()=>void;
	redo:()=>void;
	readonly canUndo:Readable<boolean>;
	readonly canRedo:Readable<boolean>;
	readonly undoDescription:Readable<string | undefined>;
	readonly redoDescription:Readable<string | undefined>;
}

export type { UndoManager, Undoable, Preservable, Localizer };