import { FunctionComponent, useState } from "react";
import { UndoManager, Localizer } from "@kvndy/undo-manager/react-hooks";


type Props = {
	manager: UndoManager;
};
const Controls: FunctionComponent<Props> = ({
	manager
}: Props) => {
	const { undo, redo, group, useUndoable, usePreservable, useCanUndo, useCanRedo, useUndoDescription, useRedoDescription } = manager;
	const [plain, setPlain] = useState(0);
	const [setting, setSetting] = useUndoable(0, "setting_unique_id");
	const [appearance, setAppearance] = usePreservable(0, "appearance_unique_id");
	const incrementPlain = () => setPlain(plain + 1);
	const incrementSetting = () => {
		group(()=> {
			setSetting(setting + 1);
		}, "increment setting");
	};
	const incrementAppearance = () => setAppearance(appearance + 1);
	const incrementSettingTwice = () => {
		group(()=> {
			setSetting(setting + 2);
		}, "increment setting twice");
	};
	const incrementBoth = () => {
		group(()=> {
			setSetting(setting + 1);
			setAppearance(appearance + 1);
		}, "increment both");
	};
	const canUndo = useCanUndo();
	const canRedo = useCanRedo();
	const undoDescription = useUndoDescription();
	const redoDescription = useRedoDescription();
	return (
		<div>
			<button type="button" onClick={incrementPlain}>Hook</button>
			<button type="button" onClick={incrementSetting}>Undoable</button>
			<button type="button" onClick={incrementSettingTwice}>Twice</button>
			<button type="button" onClick={incrementAppearance}>Preservable</button>
			<button type="button" onClick={incrementBoth}>Both</button>
			<button type="button" onClick={undo} disabled={!canUndo} title={undoDescription}>
				Undo
			</button>
			<button type="button" onClick={redo} disabled={!canRedo} title={redoDescription}>
				Redo
			</button>
			<p>hook:{ plain }</p>
			<p>undoable:{ setting }</p>
			<p>preservable:{ appearance }</p>
		</div>
	);
}

type PanesProps = {
	manager: UndoManager;
};
const Panes: FunctionComponent<PanesProps> = ({
	manager
}: PanesProps) => {
	console.log("Panes render");
	const [show, setShow] = useState(false);
	const change = (event:React.ChangeEvent<HTMLInputElement>) => {
		setShow(event.target.checked);
	}
	const children = [
		<Controls
			manager={ manager }
			key="one"
		/>,
		<label
			key="label"
		>
			<input type="checkbox" checked={show} onChange={ change }/>show duplicate
		</label>
	];
	if (show) children.push(
		<Controls
			manager={ manager }
			key="two"
		/>
	);
	return (
		<div>
			{ children }
		</div>
	);
}

function App() {
	const undoLocalizer:Localizer = (description:string) => {
		return "Undo " + description;
	}
	const redoLocalizer:Localizer = (description:string) => {
		return "Redo " + description;
	}
	const manager = new UndoManager(undoLocalizer, redoLocalizer);
	console.log("App render");
	return (
		<Panes
			manager={ manager }
		/>
	);
}

export { App }
