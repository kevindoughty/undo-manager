import { h, render } from "preact";
import { effect } from "@preact/signals";
import { UndoManager }from "@kvndy/undo-manager";


const { undoable, undo } = new UndoManager();
const setting = undoable(0);

const App = () => {
	const value = setting.value;
	console.log("render",value);
	return (
		<p>
			{ value }
		</p>
	);
};

effect( ()=> {
	console.log("effect:",setting.value);
});

render(
	<App />,
	document.body
);

setting.value = 1;
setTimeout( () => {
	undo();
});