import { h, render } from "preact";
import { effect } from "@preact/signals";
import { UndoManager, type Undoable, type Preservable, type Localizer } from "@kvndy/undo-manager/preact-signals";

const undoLocalizer:Localizer = function(description:string) {
	return "Undo " + description;
};
const redoLocalizer:Localizer = function(description:string) {
	return "Redo " + description;
};
const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);

const setting: Undoable = undoable(0);
const appearance: Preservable = preservable(0);

const App = () => {
	const value = setting.value;
	console.log("render",value);
	return (
		<p>
			{ value }
		</p>
	);
};

effect( ()=> {
	console.log("effect:",setting.value);
});

render(
	<App />,
	document.body
);

group( () => {
	setting.value = 1;
	appearance.value = 1;
}, "grouped changes");
console.log("description:",undoDescription.value);

setTimeout( () => {
	undo();
	console.log("description:",undoDescription.value);
});