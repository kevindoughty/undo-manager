import { createEffect, createSignal } from "solid-js";
import { UndoManager } from "@kvndy/undo-manager/solid-signals";

function App() {
	const undoLocalizer = (description) => {
		return "Undo " + description;
	}
	const redoLocalizer = (description) => {
		return "Redo " + description;
	}
	const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);

	const [plain, setPlain] = createSignal(0);
	const [setting, setSetting] = undoable(0, "increment undoable");
	const [appearance, setAppearance] = preservable(0);
	const incrementPlain = () => setPlain((prev) => prev + 1);
	const incrementSetting = () => setSetting((prev) => prev + 1);
	const incrementAppearance = () => setAppearance((prev) => prev + 1);
	const incrementBoth = () => {
		group(()=> {
			incrementSetting();
			incrementAppearance();
		}, "increment both");
	}
	const sum = () => {
		return plain() + setting() + appearance();
	};
	createEffect(() => {
		console.log("signal:%s;",plain());
	});
	createEffect(() => {
		console.log("undoable:%s;",setting());
	});
	createEffect(() => {
		console.log("preservable:%s;",appearance());
	});

	console.log("App render");

	return (
	  <div>
		<span>Signal: {plain()}</span>
		<br/>
		<span>Undoable: {setting()}</span>
		<br/>
		<span>Preservable: {appearance()}</span>
		<br/>
		<span>Sum: {sum()}</span>
		<br/>
		<button type="button" onClick={incrementPlain}>Signal</button>
		<button type="button" onClick={incrementSetting}>Undoable</button>
		<button type="button" onClick={incrementAppearance}>Preservable</button>
		<button type="button" onClick={incrementBoth}>Both</button>
		<button type="button" onClick={undo} disabled={!canUndo()} title={undoDescription()}>
		  Undo
		</button>
		<button type="button" onClick={redo} disabled={!canRedo()} title={redoDescription()}>
		  Redo
		</button>
	  </div>
	);
}

export default App;
