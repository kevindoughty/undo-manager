console.log("webpack new import");

import { h, render } from "preact";
import { effect } from "@preact/signals";
import { UndoManager } from "@kvndy/undo-manager/preact-signals";

const { undoable, undo } = new UndoManager();
const setting = undoable(0);

const App = () => {
	const value = setting.value;
	return (
		<p>
			{ value }
		</p>
	);
};

effect( ()=> {
	console.log("setting.value:",setting.value);
});

render(
	<App />,
	document.body
);

setting.value = 1;
undo();