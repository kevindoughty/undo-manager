import { render } from "preact";
import { signal, computed, effect } from "@preact/signals";
import { UndoManager, type Localizer } from "@kvndy/undo-manager/preact-signals";


const undoLocalizer:Localizer = (description:string) => {
	return "Undo " + description;
}
const redoLocalizer:Localizer = (description:string) => {
	return "Redo " + description;
}
const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);

const setting = undoable(0, "increment undoable");
const appearance = preservable(0);
const plain = signal(0);

effect(() => {
	console.log("signal:%s;", plain.value);
});
effect(() => {
	console.log("undoable:%s;", setting.value);
});
effect(() => {
	console.log("preservable:%s;", appearance.value);
});

function App({ plain, setting, appearance }) {
	
	const incrementPlain = () => {
		plain.value = plain.value + 1;
	};
	const incrementSetting = () => {
		setting.value = setting.value + 1;
	};
	const incrementAppearance = () => {
		appearance.value = appearance.value + 1;
	};
	const incrementBoth = () => {
		group(()=> {
			incrementSetting();
			incrementAppearance();
		}, "increment both");
	}
	const sum = computed(()=> {
		return plain.value + setting.value + appearance.value;
	});
	console.log("App render");

	return (
	  <div>
		<span>Signal: {plain}</span>
		<br/>
		<span>Undoable {setting.value}</span>
		<br/>
		<span>Preservable: {appearance.value}</span>
		<br/>
		<span>Sum: {sum.value}</span>
		<br/>
		<button type="button" onClick={incrementPlain}>Signal</button>
		<button type="button" onClick={incrementSetting}>Undoable</button>
		<button type="button" onClick={incrementAppearance}>Preservable</button>
		<button type="button" onClick={incrementBoth}>Both</button>
		<button type="button" onClick={undo} disabled={!canUndo.value} title={undoDescription.value}>
		  Undo
		</button>
		<button type="button" onClick={redo} disabled={!canRedo.value} title={redoDescription.value}>
		  Redo
		</button>
	  </div>
	);
}


render(<App
	plain={ plain }
	setting={ setting }
	appearance={ appearance }
/>, document.getElementById('app'));
