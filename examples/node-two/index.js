console.log("node new import with cjs build");

const { effect } = require("@preact/signals");
const { UndoManager } = require("@kvndy/undo-manager/preact-signals");

const { undoable, undo } = new UndoManager();
const setting = undoable(0);

effect( ()=> {
	console.log("setting.value:",setting.value);
});

setting.value = 1;
undo();