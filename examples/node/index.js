const { UndoManager } = require("@kvndy/undo-manager");
const { effect } = require("@preact/signals");

const { undoable, undo } = new UndoManager();
const setting = undoable(0);
effect( ()=> {
	console.log("setting.value:",setting.value);
});
setting.value = 1;
undo();