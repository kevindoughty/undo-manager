import { Signal, signal, effect, computed } from "@preact/signals-react";
import { useSignals } from "@preact/signals-react/runtime";
import { FunctionComponent } from "react";
import { UndoManager, Localizer, Undoable, Preservable } from "@kvndy/undo-manager/preact-signals-react";


type Props = {
	plain: Signal;
	setting: Undoable;
	appearance: Preservable;
	manager: UndoManager;
};
const Controls: FunctionComponent<Props> = ({
	plain,
	setting,
	appearance,
	manager
}: Props) => {
	useSignals();
	const { group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = manager;
	const incrementPlain = () => {
		plain.value = plain.value + 1;
	};
	const incrementSetting = () => {
		setting.value = setting.value + 1;
	};
	const incrementAppearance = () => {
		appearance.value = appearance.value + 1;
	};
	const incrementBoth = () => {
		group(()=> {
			incrementSetting();
			incrementAppearance();
		}, "increment both");
	}
	const sum = computed(()=> {
		return plain.value + setting.value + appearance.value;
	});
	console.log("Controls render");
	return (
	  <div>
		<span>Signal: {plain}</span>
		<br/>
		<span>Undoable: {setting.value}</span>
		<br/>
		<span>Preservable: {appearance.value}</span>
		<br/>
		<span>Sum: {sum.value}</span>
		<br/>
		<button type="button" onClick={incrementPlain}>Signal</button>
		<button type="button" onClick={incrementSetting}>Undoable</button>
		<button type="button" onClick={incrementAppearance}>Preservable</button>
		<button type="button" onClick={incrementBoth}>Both</button>
		<button type="button" onClick={undo} disabled={!canUndo.value} title={undoDescription.value}>
			Undo
		</button>
		<button type="button" onClick={redo} disabled={!canRedo.value} title={redoDescription.value}>
			Redo
		</button>
	  </div>
	);
}

function App() {
	useSignals();
	const undoLocalizer:Localizer = (description:string) => {
		return "Undo " + description;
	}
	const redoLocalizer:Localizer = (description:string) => {
		return "Redo " + description;
	}
	const manager = new UndoManager(undoLocalizer, redoLocalizer);
	const { undoable, preservable } = manager;
	const plain = signal(0);
	const setting = undoable(0, "increment setting");
	const appearance = preservable(0);
	effect(() => {
		console.log("signal:%s;", plain.value);
	});
	effect(() => {
		console.log("undoable:%s;", setting.value);
	});
	effect(() => {
		console.log("preservable:%s;", appearance.value);
	});
	console.log("App render");
	return (
		<Controls
			plain={ plain }
			setting={ setting }
			appearance={ appearance }
			manager={ manager }
		/>
	);
}

export { App }
