import terser from "@rollup/plugin-terser";
import { dts } from "rollup-plugin-dts";

export default [

	{ /* PREACT SIGNALS */
		input: "preact-signals/temp/index.js",
		output: [
			{
				file: "preact-signals/dist/undo-manager.cjs",
				format: "cjs",
				name: "UndoManager"
			},
			{
				file: "preact-signals/dist/undo-manager.mjs",
				format: "es",
				name: "UndoManager"
			}
		],
		external: ["@preact/signals"]
	},
	{
		input: "preact-signals/temp/index.d.ts",
		output: [
			{
				file: "preact-signals/dist/undo-manager.d.ts",
				format: "es"
			}
		],
		plugins: [dts()],
		external: ["@preact/signals"]
	},


	{ /* PREACT SIGNALS REACT */
		input: "preact-signals-react/temp/index.js",
		output: [
			{
				file: "preact-signals-react/dist/undo-manager.mjs",
				format: "es",
				name: "UndoManager"
			}
		],
		external: ["@preact/signals-react", "react"]
	},
	{
		input: "preact-signals-react/temp/index.d.ts",
		output: [
			{
				file: "preact-signals-react/dist/undo-manager.d.ts",
				format: "es"
			}
		],
		plugins: [dts()],
		external: ["@preact/signals-react", "react"]
	},


	{ /* SVELTE STORES */
		input: "svelte-stores/temp/index.js",
		output: [
			{
				file: "svelte-stores/dist/undo-manager.mjs",
				format: "es",
				name: "UndoManager"
			}
		],
		external: ["svelte/store"]
	},
	{
		input: "svelte-stores/temp/index.d.ts",
		output: [
			{
				file: "svelte-stores/dist/undo-manager.d.ts",
				format: "es"
			}
		],
		plugins: [dts()],
		external: ["svelte/store"]
	},


	{ /* SOLID SIGNALS */
		input: "solid-signals/temp/index.js",
		output: [
			{
				file: "solid-signals/dist/undo-manager.mjs",
				format: "es",
				name: "UndoManager"
			}
		],
		external: ["solid-js"]
	},
	{
		input: "solid-signals/temp/index.d.ts",
		output: [
			{
				file: "solid-signals/dist/undo-manager.d.ts",
				format: "es"
			}
		],
		plugins: [dts()],
		external: ["solid-js"]
	},


	{ /* VUE SHALLOW REFS */
		input: "vue-shallow-refs/temp/index.js",
		output: [
			{
				file: "vue-shallow-refs/dist/undo-manager.mjs",
				format: "es",
				name: "UndoManager"
			}
		],
		external: ["vue"]
	},
	{
		input: "vue-shallow-refs/temp/index.d.ts",
		output: [
			{
				file: "vue-shallow-refs/dist/undo-manager.d.ts",
				format: "es"
			}
		],
		plugins: [dts()],
		external: ["vue"]
	},


	{ /* REACT HOOKS */
		input: "react-hooks/temp/index.js",
		output: [
			{
				file: "react-hooks/dist/undo-manager.mjs",
				format: "es",
				name: "UndoManager"
			}
		],
		external: ["react"]
	},
	{
		input: "react-hooks/temp/index.d.ts",
		output: [
			{
				file: "react-hooks/dist/undo-manager.d.ts",
				format: "es"
			}
		],
		plugins: [dts()],
		external: ["react"]
	},


	{ /* LEGACY */
		input: "preact-signals/temp/index.js",
		output: [
			{
				file: "dist/undo-manager.common.js",
				format: "cjs",
				name: "UndoManager"
			},
			{
				file: "dist/undo-manager.min.js",
				format: "iife",
				name: "UndoManager",
				plugins: [terser()],
				globals: {
					"@preact/signals": "signals"
				}
			},
			{
				file: "dist/undo-manager.umd.js",
				format: "umd",
				name: "UndoManager",
				globals: {
					"@preact/signals": "signals"
				}
			},
			{
				file: "dist/undo-manager.module.js",
				format: "es",
				name: "UndoManager"
			}
		],
		external: ["@preact/signals"]
	},
	{
		input: "preact-signals/temp/index.d.ts",
		output: [
			{
				file: "dist/undo-manager.d.ts",
				format: "es"
			}
		],
		plugins: [dts()],
		external: ["@preact/signals"]
	}
]