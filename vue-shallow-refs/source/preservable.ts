import { ShallowRef, shallowRef } from "vue";
import { Context } from "./context.js";

/**
 * A preservable object
 * @typedef {function} Preservable
 * @param {*} value
 * @param {boolean} interrupting
 */
class Preservable<T> {
	private _signal:ShallowRef<T>;
	value:T;
	constructor(context:Context, value:T, interrupting?:boolean) {
		this._signal = shallowRef(value);
		this.value = value; // required for typescript
		Object.defineProperty(this, "value", { // public
			get() {
				return this._signal.value;
			},
			set(value) {
				if (interrupting) context.makeNonCoalescingChange();
				this._signal.value = value;
			},
			enumerable: true,
			configurable: false
		});	
		context.registerPreservable(this, this.registerBefore.bind(this), this.registerAfter.bind(this));
	}

	private registerBefore(value:T) {
		const invocation = (direction:number) => {
			if (direction < 0) { // undo
				this._signal.value = value;
			}
			return this.registerBefore(value);
		};
		return invocation;
	}
	
	private registerAfter(value:T) {
		const invocation = (direction:number) => {
			if (direction > 0) { // redo
				this._signal.value = value;
			}
			return this.registerAfter(value);
		};
		return invocation;
	}
}

export { Preservable };