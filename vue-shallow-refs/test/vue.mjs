import { assert } from "chai";
import { computed, reactive, toRef, watchEffect, shallowRef } from "vue";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("VUE", function() {
	it("zero", function() {
		const state = reactive({
			foo: 1,
			bar: 2
		});
		const fooRef = toRef(state, "foo");
		let fooCount = 0;
		let stateCount = 0;
		watchEffect(() => {
			fooCount = fooRef.value;
		});
		watchEffect(() => {
			stateCount = state.foo;
		});
		fooRef.value++;
		assert.equal(state.foo, 2);
		state.foo++;

		const derived = computed( () => {
			return fooRef.value + state.foo;
		});

		assert.equal(fooCount, 1);
		assert.equal(stateCount, 1);
		assert.equal(state.foo, 3);
		assert.equal(fooRef.value, 3);
		assert.equal(state.foo, fooRef.value);
		assert.equal(derived.value, 6);

		const shallow = shallowRef(0);
		shallow.value++;
		assert.equal(shallow.value, 1);
	});

	it("one", function() {
		const { undoable, preservable, undo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		const derived = computed( () => {
			return setting.value + appearance.value;
		});
		assert.equal(derived.value, 0);
		setting.value = 1;
		assert.equal(derived.value, 1);
		appearance.value = 2;
		assert.equal(derived.value, 3);
		undo();
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		assert.equal(derived.value, 0);
	});
});