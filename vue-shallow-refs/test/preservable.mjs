import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("PRESERVABLE", function() {
	it("one", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		appearance.value = 1;
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 1);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		appearance.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 2);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		appearance.value = 3;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 3);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 1);
		undo(); // one extra should have no effect
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		redo(); // one extra should have no effect
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it("two", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it("three", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo} = new UndoManager();
		const setting = undoable(0, "change setting");
		const appearance = preservable(0);
		appearance.value = 1;
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		appearance.value = 2;
		setting.value = 2;
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		appearance.value = 3;
		setting.value = 3;
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(appearance.value, 3);
	});

	it("Changing a preservable does not affect canUndo without undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		appearance.value = 1;
		assert.equal(canUndo.value, false);
	});

	it("Changing a preservable does not affect canUndo with undoable", function() {
		const { undoable, preservable, group, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		appearance.value = 1;
		assert.equal(canUndo.value, false);
	});

	it("Changing a preservable in a group does not affect canUndo without undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		group(() => {
			appearance.value = 1;
		});
		assert.equal(canUndo.value, false);
	});

	it("Changing a preservable in a group does not affect canUndo with undoable", function() {
		const { undoable, preservable, group, canUndo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		group(() => {
			appearance.value = 1;
		});
		assert.equal(canUndo.value, false);
	});

	it("Changing a preservable in a coalescing group does not affect canUndo without undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		group(() => {
			appearance.value = 1;
		}, "change", "change");
		group(() => {
			appearance.value = 2;
		}, "change", "change");
		assert.equal(canUndo.value, false);
	});

	it("Changing a preservable in a coalescing group does not affect canUndo with undoable", function() {
		const { preservable, group, canUndo } = new UndoManager();
		const appearance = preservable(0);
		group(() => {
			appearance.value = 1;
		}, "change", "change");
		group(() => {
			appearance.value = 2;
		}, "change", "change");
		assert.equal(canUndo.value, false);
	});
});