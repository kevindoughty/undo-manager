import { Stack } from "./stack.js";
import { Preservable } from "./preservable.js";
import type { Localizer } from "./types.js";
import type { Invocation } from "./entry.js";

class Context {
	private preservables:Array<Preservable<any>>;
	private befores:Array<(value:any)=>Invocation>;
	private afters:Array<(value:any)=>Invocation>;
	private isCoalescingGroup:boolean;
	private currentCoalesceKey:null|any;
	private previousCoalesceKey:null|any;
	private counter:number;
	private depth:number;
	stack:Stack;

	constructor(undoLocalizer?:Localizer, redoLocalizer?:Localizer, maxCount?:number) {
		this.preservables = [];
		this.befores = [];
		this.afters = [];
		this.isCoalescingGroup = false;
		this.currentCoalesceKey = null;
		this.previousCoalesceKey = null;
		this.counter = 0;
		this.depth = 0;
		this.stack = new Stack(undoLocalizer, redoLocalizer, maxCount);
	}

	registerPreservable<T=any>(preservable:Preservable<T>, registerBefore:(value:T)=>Invocation, registerAfter:(value:T)=>Invocation) {
		this.preservables.push(preservable);
		this.befores.push(registerBefore);
		this.afters.push(registerAfter);
	}

	registerUndoable() {
		return this.counter++;
	}

	private beforeChange(direction:number) { // undoables only
		if (direction < 0) { // undo
			this.currentCoalesceKey = null;
			this.previousCoalesceKey = null;
			this.stack.willUndo();
		} else if (direction > 0) { // redo
			this.currentCoalesceKey = null;
			this.previousCoalesceKey = null;
			this.stack.willRedo();
		} else if (!this.shouldCoalesce()) { // called the first time when coalescing because of previous key
			this.stack.changeReset();
			this.preservables.forEach( (item, index) => {
				const registerBefore = this.befores[index];
				const value = item.getSnapshot();
				const invocation = registerBefore(value);
				this.stack.registerBeforeInvocation(invocation); // pushes to next
			});
		}
	}
	
	private afterChange(direction:number, changeDescription?:any) {
		if (direction === 0) { // neither undo nor redo
			if (this.shouldCoalesce()) {
				this.stack.coalesceReset(); // wipe existing after state for recapture
			}
			this.preservables.forEach( (item, index) => {
				const registerAfter = this.afters[index];
				const value = item.getSnapshot();
				const invocation = registerAfter(value);
				this.stack.registerAfterInvocation(invocation);
			});
			this.stack.afterChange(changeDescription, this.shouldCoalesce());
		}
	}

	private wrapper(direction:number, callback:()=>void, changeDescription?:any) {
		if (this.depth === 0) {
			this.beforeChange(direction);
		}
		this.depth++;
		callback();
		this.depth--;
		if (this.depth === 0) {
			this.afterChange(direction, changeDescription);
		}
	}

	group(callback:()=>void, changeDescription?:any, coalescing?:any) {
		if (coalescing === true && (changeDescription === null || changeDescription === undefined)) {
			coalescing = callback;
		}
		const isCoalescing = coalescing !== null && coalescing !== undefined && coalescing !== false && (coalescing !== true || (changeDescription !== null && changeDescription !== undefined));
		if (this.depth === 0) {
			if (isCoalescing) {
				this.beginCoalescingGroup();
				this.previousCoalesceKey = this.currentCoalesceKey;
				if (coalescing === true) this.currentCoalesceKey = changeDescription;
				else this.currentCoalesceKey = coalescing;
			} else {
				this.currentCoalesceKey = null;
			}
		}
		this.wrapper(0, callback, changeDescription);
		if (this.depth === 0) {
			if (isCoalescing) {
				this.endCoalescingGroup();
			}
		}
	}

	private shouldCoalesce() {
		if (this.isCoalescingGroup && this.currentCoalesceKey !== null && this.currentCoalesceKey === this.previousCoalesceKey) {
			return true;
		}
		return false;
	}

	private beginCoalescingGroup() {
		this.isCoalescingGroup = true;
	}

	private endCoalescingGroup() {
		this.isCoalescingGroup = false;
	}

	makeNonCoalescingChange() {
		if (!this.isCoalescingGroup) {
			this.currentCoalesceKey = null;
		}
	}

	undo() {
		if (this.depth === 0 && this.stack.canUndo()) { // second check needed for TS
			const stack = this.stack;
			const direction = -1;
			this.wrapper(direction, () => {
				stack.applier(direction);
			}, null);
		}
	}

	redo() {
		if (this.depth === 0 && this.stack.canRedo()) { // second check needed for TS
			const stack = this.stack;
			const direction = 1;
			this.wrapper(direction, () => {
				stack.applier(direction);
			}, null);
		}
	}
}

export { Context };