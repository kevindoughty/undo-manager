import { Context } from "./context.js";

/**
 * An undoable object
 * @typedef {function} Undoable
 * @param {*} value
 * @param {*} description
 * @param {*} coalescing
 */
class Undoable<T> {
	private context:Context;
	private description:any;
	private coalescing:any;
	private id:number;
	private subscribers:Set<Function>;
	private _value:T;
	
	constructor(context:Context, value:T, description?:any, coalescing?:any) {
		this.context = context;
		this.description = description;
		this.coalescing = coalescing;
		this.id = context.registerUndoable();
		this.subscribers = new Set();
		this._value = value;
	}

	private registerUndo(instance:Undoable<T>, value:T) {
		const undoer = () => {
			instance.context.group( () => {
				const original:T = instance._value;
				instance.set(value);
				instance.registerUndo(instance, original);
			}, instance.description);
		};
		this.context.stack.registerUndoerInvocation(undoer, instance.id);
	}
	
	subscribe(callback:()=>void) {
		this.subscribers.add(callback);
		return () => this.subscribers.delete(callback);
	};

	getSnapshot() {
		const value = this._value;
		return value;
	}

	set(value:T) {
		this.setter(value);
		for (const callback of this.subscribers) {
			callback(value);
		}
	}

	private setter(value:T) {
		let actual_coalescing = this.coalescing;
		if (this.coalescing === true && (this.description === null || this.description === undefined)) {
			actual_coalescing = this;
		}
		const original = this._value;
		const instance = this;
		if (value !== original) {
			this.context.group( () => {
				instance._value = value;
				this.registerUndo(instance, original);
			}, this.description, actual_coalescing);
		}
	};
}

export { Undoable };