class Writable<T> {
	subscribers:Set<Function>;
	private _value:T;
	constructor(value:T) {
		this.subscribers = new Set();
		this._value = value;
	}
	subscribe(callback:(value:T)=>void) {
		this.subscribers.add(callback);
		return () => this.subscribers.delete(callback);
	};
	set(value:T) {
		this._value = value;
		for (const callback of this.subscribers) {
			callback(value);
		}
	};
	get() {
		return this._value;
	}
}

function writable<T>(value:T) {
	return new Writable(value);
}

export { Writable, writable };
