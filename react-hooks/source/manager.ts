import { useSyncExternalStore } from "react";
import { Undoable as UndoableClass } from "./undoable.js";
import { Preservable as PreservableClass } from "./preservable.js";
import { Context } from "./context.js";
import type { Localizer, Undoable, Preservable } from "./types.js";

/**
 * An object that provides change and undo stack management
 * @constructor
 * @param {Localizer} undoLocalizer
 * @param {Localizer} redoLocalizer
 * @param {number} maxCount
 * @returns Primitives used for managing the undo stack
 */
class UndoManager {
	useUndoable:<T=any>(value:T, description:any, coalescing?:any) => Undoable<T>;
	usePreservable:<T=any>(value:T, description:any, interrupting?:any) => Preservable<T>;
	group:(callback:()=>void, description?:any, coalescing?:any) => void;
	undo:()=>void;
	redo:()=>void;
	readonly useCanUndo:()=>boolean;
	readonly useCanRedo:()=>boolean;
	readonly useUndoDescription:()=> string | undefined;
	readonly useRedoDescription:()=> string | undefined;

	constructor(undoLocalizer?:Localizer, redoLocalizer?:Localizer, maxCount?:number) {
		const context = new Context(undoLocalizer, redoLocalizer, maxCount);
		this.useUndoable = undoable(context);
		this.usePreservable = preservable(context);
		this.group = group(context);
		this.undo = undo(context);
		this.redo = redo(context);
		this.useCanUndo = canUndo(context);
		this.useCanRedo = canRedo(context);
		this.useUndoDescription = undoDescription(context);
		this.useRedoDescription = redoDescription(context);
	}
}

function undoable(context:Context) { // private
	const entries:Record<string,any> = new Map();
	return function<T=any>(value:T, description:any, coalescing?:any) : Undoable<T> { // public
		let signal = entries.get(description);
		if (!signal) {
			signal = new UndoableClass(context, value, description, coalescing);
			entries.set(description,signal);
		}
		const sync:T = useSyncExternalStore(signal.subscribe.bind(signal), signal.getSnapshot.bind(signal), signal.getSnapshot.bind(signal));
		const setter = (value: any) => {
			signal.set(value);
			return value;
		};
		const array: Undoable<T> = [sync, setter];
		return array;
	}
}

function preservable(context:Context) { // private
	const entries:Record<string,any> = new Map();
	return function<T=any>(value:T, description:any, interrupting?:boolean) : Preservable<T> { // public
		let signal = entries.get(description);
		if (!signal) {
			signal = new PreservableClass(context, value, interrupting);
			entries.set(description,signal);
		}
		const sync:T = useSyncExternalStore(signal.subscribe.bind(signal), signal.getSnapshot.bind(signal), signal.getSnapshot.bind(signal));
		const setter = (value: any) => {
			signal.set(value);
			return value;
		};
		const array: Preservable<T> = [sync, setter];
		return array;
	}
}

function group(context:Context) { // private
/**
 * A function that permits batch changes
 * @param {function} callback
 * @param {*} description
 * @param {*} coalescing
 */
	return function(callback:()=>void, description?:any, coalescing?:any) { // public
		context.group(callback, description, coalescing);
	}
}

const undo = function(context:Context) { // private
/**
 * A function which restores state to its previous value
 */
	return function() { // public
		context.undo();
	}
};

const redo = function(context:Context) { // private
/**
 * A function which restores state to its next value
 */
	return function() { // public
		context.redo();
	}
};

const canUndo = function(context:Context) { // private
/**
 * A Readable store whose getter provides if undo is possible
 */
	return context.stack.canUndo; // public
};

const canRedo = function(context:Context) { // private
/**
 * A Readable store whose getter provides if redo is possible
 */
	return context.stack.canRedo; // public
};

const undoDescription = function(context:Context) { // private
/**
 * A Readable store whose getter provides a change description
 */
	return context.stack.undoDescription; // public
};

const redoDescription = function(context:Context) { // private
/**
 * A Readable store whose getter provides a change description
 */
	return context.stack.redoDescription; // public
};


export { UndoManager };