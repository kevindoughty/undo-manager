/**
 * An optional callback passed to the UndoManager contstructor.
 * Generates localized strings to describe changes.
 * @typedef {function} Localizer
 * @param {*} description
 * @returns {(string | undefined)} A localized string describing a change
 */
type Localizer = (description:any) => string | undefined;

type Undoable<T> = [value:T, setter:(value:T)=>void];

type Preservable<T> = [value:T, setter:(value:T)=>void];

/**
 * An object that provides change and undo stack management
 * @constructor
 * @param {Localizer} undoLocalizer
 * @param {Localizer} redoLocalizer
 * @param {number} maxCount
 * @returns Primitives used for managing the undo stack
 */
declare class UndoManager {
	constructor(undoLocalizer?:Localizer, redoLocalizer?:Localizer, maxCount?:number);
	useUndoable:<T=any>(value:any, description:any, coalescing?:any) => Undoable<T>;
	usePreservable:<T=any>(value:any, description:any, interrupting?:any)=> Preservable<T>;
	group:(callback:()=>void, description?:any, coalescing?:any) => void;
	undo:()=>void;
	redo:()=>void;
	readonly useCanUndo:()=>boolean;
	readonly useCanRedo:()=>boolean;
	readonly useUndoDescription:()=> string | undefined;
	readonly useRedoDescription:()=> string | undefined;
}

export type { UndoManager, Undoable, Preservable, Localizer };