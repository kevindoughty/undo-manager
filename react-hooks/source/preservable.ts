import { Context } from "./context.js";

/**
 * A preservable object
 * @typedef {function} Preservable
 * @param {*} value
 * @param {boolean} interrupting
 */
class Preservable<T> {
	private context:Context;
	private interrupting:boolean;
	private subscribers:Set<Function>;
	private _value:T;

	constructor(context:Context, value:T, interrupting?:boolean) {
		this.context = context;
		this.interrupting = interrupting || false;
		this.subscribers = new Set();
		this._value = value;
		context.registerPreservable(this, this.registerBefore.bind(this), this.registerAfter.bind(this));
	}

	private registerBefore(value:T) {
		const invocation = (direction:number) => {
			if (direction < 0) { // undo
				this.set(value);
			}
			return this.registerBefore(value);
		};
		return invocation;
	}
	
	private registerAfter(value:T) {
		const invocation = (direction:number) => {
			if (direction > 0) { // redo
				this.set(value);
			}
			return this.registerAfter(value);
		};
		return invocation;
	}

	subscribe(callback:()=>void) {
		this.subscribers.add(callback);
		return () => this.subscribers.delete(callback);
	};

	getSnapshot() {
		return this._value;
	}

	set(value:T) {
		if (this.interrupting) this.context.makeNonCoalescingChange();
		this._value = value;
		for (const callback of this.subscribers) {
			callback(value);
		}
	}
}

export { Preservable };