import { assert } from "chai";

import React from "react";
import { useState, useCallback } from "react";
import { render, screen, renderHook, act } from "@testing-library/react";
import { UndoManager } from "../dist/undo-manager.mjs";

function isFunction(w) {
	return w && {}.toString.call(w) === "[object Function]";
}


describe("API", function() {

	it("isFunction", function() {
		const test = function() {};
		assert(isFunction(test));
		assert(isFunction(function() {}));
		assert(isFunction(() => {}));
		assert(!isFunction({}));
		assert(!isFunction("[object Function]"));
	});

	it("useUndoable", function() {
		const { useUndoable } = new UndoManager();
		assert(isFunction(useUndoable));
	});
	it("usePreservable", function() {
		const { usePreservable } = new UndoManager();
		assert(isFunction(usePreservable));
	});
	it("group", function() {
		const { group } = new UndoManager();
		assert(isFunction(group));
	});
	it("undo", function() {
		const { undo } = new UndoManager();
		assert(isFunction(undo));
	});
	it("redo", function() {
		const { redo } = new UndoManager();
		assert(isFunction(redo));
	});
	it("useCanUndo", function() {
		const { useCanUndo } = new UndoManager();
		assert(isFunction(useCanUndo));
	});
	it("useCanRedo", function() {
		const { useCanRedo } = new UndoManager();
		assert(isFunction(useCanRedo));
	});
	it("useUndoDescription", function() {
		const { useUndoDescription } = new UndoManager();
		assert(isFunction(useUndoDescription));
	});
	it("useRedoDescription", function() {
		const { useRedoDescription } = new UndoManager();
		assert(isFunction(useRedoDescription));
	});
	it("Object.keys", function() {
		const manager = new UndoManager();
		assert.equal(Object.keys(manager).length, 9);
	});
	it("Object.getOwnPropertyNames", function() {
		const manager = new UndoManager();
		assert.equal(Object.getOwnPropertyNames(manager).length, 9);
	});

	it("Hook", function() {
		const useCounter = function() {
			const [count, setCount] = useState(0)
			const increment = useCallback(() => setCount((x) => x + 1), [])
			return { count, increment }
		};
		const { result } = renderHook(() => useCounter());
		assert.equal(result.current.count, 0);
		assert.equal(typeof result.current.increment,"function");
	});
});