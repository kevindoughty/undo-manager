import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("UNDO", function() {
	it("calling undo in a group will silently fail", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		
		const setting = undoable(0, "change setting", true);
		setting.value = 1;
		
		const appearance = preservable(0, true);
		appearance.value = 2;

		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 2);
		group( () => {
			undo();
		}, "change setting", false);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 2);

	});

	it("calling redo in a group will silently fail", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		
		const setting = undoable(0, "change setting", true);
		setting.value = 1;
		
		const appearance = preservable(0, true);
		appearance.value = 2;

		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 2);
		group( () => {
			redo();
		}, "change setting", false);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 2);

	});
});