import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";


describe("README", function() {
	it("the documentation is accurate", function() {
		const undoLocalizer = (description) => {
			return "Undo " + description;
		}
		const redoLocalizer = (description) => {
			return "Redo " + description;
		}
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new UndoManager(undoLocalizer, redoLocalizer);
		
		const setting = undoable(0, "change setting", true);
		setting.value = 1;
		
		const appearance = preservable(0, true);
		appearance.value = 2;
		setting.value = 3;
		appearance.value = 4;

		group( () => {
			setting.value = 5;
			appearance.value = 6;
		}, "change setting and more", true);
		group( () => {
			setting.value = 7;
			appearance.value = 8;
		}, "change setting and more", true);
		
		undo();
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 4);
		undo();
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 2);
		
		redo();
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 2);

		assert.equal(canUndo.value, true);

		assert.equal(canRedo.value, true);

		assert.equal(undoDescription.value, "Undo change setting");

		assert.equal(redoDescription.value, "Redo change setting and more");

		const dont = undoable("just"); // just don't
		dont.value = "dont";
		undo();
		undo();// before it existed
		assert(dont.value, "just");
		redo();
		redo();

		group( () => {
			appearance.value = 9; // will be registered as the after change value 
		}, "conceptual section changes", true);
		group( () => {
			setting.value = 7; 
		}, "conceptual section changes", true); // coalesces
		undo();
		assert.equal(appearance.value, 2); // coalescing group did not affect before value
		redo();
		assert.equal(appearance.value, 9);

		group( () => {
			appearance.value = 10;
		}, "conceptual section changes", true); // does not coalesce
		assert.equal(appearance.value, 10);
		undo();
		redo();
		assert.equal(appearance.value, 9); // changes are lost

	});
});