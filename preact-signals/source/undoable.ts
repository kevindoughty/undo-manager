import { Signal, signal } from "@preact/signals";
import { Context } from "./context.js";

/**
 * An undoable object
 * @typedef {function} Undoable
 * @param {*} value
 * @param {*} description
 * @param {*} coalescing
 */
class Undoable<T> {
	private context:Context;
	private description:any;
	private coalescing:any;
	private id:number;
	private _signal:Signal<T>;
	value:T;

	constructor(context:Context, value:T, description?:any, coalescing?:any) {
		this.context = context;
		this.description = description;
		this.coalescing = coalescing;
		this.id = context.registerUndoable();
		this._signal = signal(value);
		this.value = value;
		Object.defineProperty(this, "value", {
			get() {
				const value:T = this._signal.value;
				return value;
			},
			set(value) {
				this.setter(value);
			},
			enumerable: true,
			configurable: false
		});
	}

	private registerUndo(instance:Undoable<T>, value:T) {
		const undoer = () => {
			instance.context.group( () => {
				const original:T = this._signal.value;
				this._signal.value = value;
				instance.registerUndo(instance, original);
			}, instance.description);
		};
		this.context.stack.registerUndoerInvocation(undoer, instance.id);
	}

	private setter(value:T) {
		let actualCoalescing = this.coalescing;
		if (this.coalescing === true && (this.description === null || this.description === undefined)) {
			actualCoalescing = this;
		}
		const original = this._signal.value;
		if (value !== original) {
			this.context.group( () => {
				this._signal.value = value;
				this.registerUndo(this, original);
			}, this.description, actualCoalescing);
		}
	}

	subscribe(fn: (value:T) => void) : () => void {
		return this._signal.subscribe(fn);
	}

	peek() {
		return this._signal.peek();
	}

	valueOf() {
		return this._signal.value;
	}

	toString() {
		return String(this._signal.value);
	}

	toJSON() {
		return this._signal.value;
	}
}

export { Undoable };