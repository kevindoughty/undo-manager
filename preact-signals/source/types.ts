import { ReadonlySignal } from "@preact/signals";

/**
 * An optional callback passed to the UndoManager contstructor.
 * Generates localized strings to describe changes.
 * @typedef {function} Localizer
 * @param {*} description
 * @returns {(string | undefined)} A localized string describing a change
 */
type Localizer = (description:any) => string | undefined;

/**
 * An undoable object
 * @typedef {function} Undoable
 * @param {*} value
 * @param {*} description
 * @param {*} coalescing
 */
interface Undoable<T = any> {
	subscribe(fn: (value: T) => void): () => void;
	valueOf(): T;
	toString(): string;
	toJSON(): T;
	peek(): T;
	get value(): T;
	set value(value: T);
}

/**
 * A preservable object
 * @typedef {function} Preservable
 * @param {*} value
 * @param {boolean} interrupting
 */
interface Preservable<T = any> {
	subscribe(fn: (value: T) => void): () => void;
	valueOf(): T;
	toString(): string;
	toJSON(): T;
	peek(): T;
	get value(): T;
	set value(value: T);
}

/**
 * An object that provides change and undo stack management
 * @constructor
 * @param {Localizer} undoLocalizer
 * @param {Localizer} redoLocalizer
 * @param {number} maxCount
 * @returns Primitives used for managing the undo stack
 */
declare class UndoManager {
	constructor(undoLocalizer?:Localizer, redoLocalizer?:Localizer, maxCount?:number);
	undoable:<T=any>(value:any, description?:any, coalescing?:any) => Undoable<T>;
	preservable:<T=any>(value:any, interrupting?:any) => Preservable<T>;
	group:(callback:()=>void, description?:any, coalescing?:any) => void;
	undo:()=>void;
	redo:()=>void;
	readonly canUndo:ReadonlySignal<boolean>;
	readonly canRedo:ReadonlySignal<boolean>;
	readonly undoDescription:ReadonlySignal<string | undefined>;
	readonly redoDescription:ReadonlySignal<string | undefined>;
}

export type { UndoManager, Undoable, Preservable, Localizer};
