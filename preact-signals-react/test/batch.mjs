import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { batch, effect } from "@preact/signals-react";


describe("BATCH", function() {
	it("wontfix", function() { // Can't fix. Expected behavior. Don't use batch, use group. See first in test/group.js
		const { undoable, undo, canUndo, canRedo } = new UndoManager();
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting1.value, 0);
		assert.equal(setting2.value, 0);
		batch(()=> {
			setting1.value = 1;
			setting2.value = 2;
		});
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 0);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting1.value, 0);
		assert.equal(setting2.value, 0);
	});

	it("batching multiple undos", function() {
		const { undoable, preservable, group, undo, redo } = new UndoManager();
		const setting = undoable(1);
		let sum = 0;
		effect(() => sum += setting.value);
		setting.value = 2;
		setting.value = 3;
		batch( () => { // must use batch, not group
			undo();
			undo();
		});
		assert.equal(setting.value, 1);
		assert.equal(sum, 7);
	});

	it("not batching multiple undos", function() {
		const { undoable, preservable, group, undo, redo } = new UndoManager();
		const setting = undoable(1);
		let sum = 0;
		effect(() => sum += setting.value);
		setting.value = 2;
		setting.value = 3;
		undo();
		undo();
		assert.equal(setting.value, 1);
		assert.equal(sum, 9);
	});
});