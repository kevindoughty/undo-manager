import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { computed, effect } from "@preact/signals-react";


describe("EFFECT", function() {
	it("one", function() {
		const { undoable, preservable, undo, redo } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		const derived = computed( () => {
			return setting.value + appearance.value;
		});
		let count = 0;
		let summation = 0;
		effect(() => {
			count++;
			summation = summation + derived.value;
		});
		assert.equal(count, 1);
		assert.equal(summation, 0);
		appearance.value = 1;
		assert.equal(count, 2);
		assert.equal(summation, 1); // 0 + (0 + 1)
		setting.value = 10;
		assert.equal(count, 3);
		assert.equal(summation, 12); // 1 + (10 + 1)
		appearance.value = 100;
		assert.equal(count, 4);
		assert.equal(summation, 122); // 12 + (10 + 100)
		setting.value = 1000;
		assert.equal(count, 5);
		assert.equal(summation, 1222); // 122 + (1000 + 100)
		appearance.value = 10000;
		assert.equal(count, 6);
		assert.equal(summation, 12222); // 1222 + (1000 + 10000)
		undo(); // Changes in undo are grouped.
		assert.equal(count, 7);
		assert.equal(summation, 12332); // 12222 + (10 + 100)
		undo(); // Changes in undo are grouped.
		assert.equal(count, 8);
		assert.equal(summation, 12333); // 12332 + (0 + 1)
		redo(); // Changes in redo are grouped.
		assert.equal(count, 9);
		assert.equal(summation, 12344); // 12333 + (10 + 1)
		redo(); // Changes in redo are grouped.
		assert.equal(count, 10);
		assert.equal(summation, 13444); // 12344 + (1000 + 100)
	});
});