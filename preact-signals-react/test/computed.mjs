import { assert } from "chai";
import { UndoManager } from "../dist/undo-manager.mjs";
import { computed } from "@preact/signals-react";


describe("COMPUTED", function() {
	it("one", function() {
		const { undoable, preservable } = new UndoManager();
		const setting = undoable(0);
		const appearance = preservable(0);
		const derived = computed( () => {
			return setting.value + appearance.value;
		});
		assert.equal(derived.value, 0);
		setting.value = 1;
		assert.equal(derived.value, 1);
		appearance.value = 2;
		assert.equal(derived.value, 3);
	});
});