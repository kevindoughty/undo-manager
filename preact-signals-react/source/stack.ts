import { ReadonlySignal, Signal, signal, computed } from "@preact/signals-react";
import type { Localizer } from "./types.js";
import { Entry, type Invocation, type Undoer } from "./entry.js";

class Stack {
	private maxCount:number;
	private next: Entry;
	private staging: Entry | undefined;
	private undoStack:Signal<Array<Entry>>;
	private redoStack:Signal<Array<Entry>>;
	private undoDescriptions:Signal<Array<any>>;
	private redoDescriptions:Signal<Array<any>>;
	canUndo:ReadonlySignal<boolean>;
	canRedo:ReadonlySignal<boolean>;
	undoDescription:ReadonlySignal<string | undefined>;
	redoDescription:ReadonlySignal<string | undefined>;

	constructor(undoLocalizer?:Localizer, redoLocalizer?:Localizer, maxCount?: number) {
		if (!undoLocalizer) undoLocalizer = () => undefined;
		if (!redoLocalizer) redoLocalizer = () => undefined;
		if (!Number.isInteger(maxCount) || !maxCount || maxCount < 0) maxCount = Infinity;
		this.maxCount = maxCount;
		this.next = this.empty();
		this.staging = this.empty();
		this.undoStack = signal([]);
		this.redoStack = signal([]);
		this.undoDescriptions = signal([]);
		this.redoDescriptions = signal([]);
		
		this.canUndo = computed(() => {
			return this.undoStack.value.length > 0;
		});

		this.canRedo = computed(() => {
			return this.redoStack.value.length > 0;
		});

		this.undoDescription = computed(() => {
			const input:Array<any> = this.undoDescriptions.value;
			const length = input.length;
			const value = length ? input[length-1] : undefined;
			if (value === null || value === undefined) return undefined;
			return undoLocalizer(value);
		});

		this.redoDescription = computed(() => {
			const input:Array<any> = this.redoDescriptions.value;
			const length = input.length;
			const value = length ? input[length-1] : undefined;
			if (value === null || value === undefined) return undefined;
			return redoLocalizer(value);
		});
	}

	private empty() {
		return new Entry();
	}

	willUndo() { // pop undo stack to staging
		const undoPrevious = this.undoStack.value;
		const undoNext = [ ...undoPrevious ];
		this.staging = undoNext.pop();
		this.undoStack.value = undoNext;
		this.next = this.empty();
		const redoNext = [ ...this.redoStack.value, this.next ];
		this.redoStack.value = redoNext;

		const undoDescriptionsNext = [ ...this.undoDescriptions.value ];
		const currentUndoDescription = undoDescriptionsNext.pop();
		this.undoDescriptions.value = undoDescriptionsNext;
		this.redoDescriptions.value = [ ...this.redoDescriptions.value, currentUndoDescription ];
	}

	willRedo() { // pop redo stack to staging
		const redoPrevious = this.redoStack.value;
		const redoNext = [ ...redoPrevious ];
		this.staging = redoNext.pop();
		this.redoStack.value = redoNext;
		this.next = this.empty();
		const undoNext = [ ...this.undoStack.value, this.next ];
		this.undoStack.value = undoNext;
		
		const redoDescriptionsNext = [ ...this.redoDescriptions.value ];
		const currentRedoDescription = redoDescriptionsNext.pop();
		this.redoDescriptions.value = redoDescriptionsNext;
		this.undoDescriptions.value = [ ...this.undoDescriptions.value, currentRedoDescription ];
	}

	changeReset() { // before change
		this.next = this.empty();
		this.redoDescriptions.value = [];
	}

	coalesceReset() { // wipe existing after state for recapture
		const nextNext = Entry.clone(this.next);
		nextNext.after = [];
		this.next = nextNext;
	}

	afterChange(changeDescription?:any, coalesce?:any) {
		if (this.maxCount > 0 && Object.keys(this.next.invocations).length) {
			if (coalesce) {
				const nextStack = this.undoStack.value.slice(0);
				nextStack.pop();
				nextStack.push(this.next);
				this.undoStack.value = nextStack;
				const nextInput = this.undoDescriptions.value.slice(0);
				if (changeDescription !== undefined && changeDescription !== null) {
					nextInput.pop();
					nextInput.push(changeDescription);
				}
				this.undoDescriptions.value = nextInput;
			} else {
				const undoStackNext = [ ...this.undoStack.value ];
				const undoDescriptionsNext = [ ...this.undoDescriptions.value ];
				if (undoStackNext.length === this.maxCount) {
					undoStackNext.shift();
					undoDescriptionsNext.shift();
				}
				if (undoStackNext.length < this.maxCount) {
					undoStackNext.push(this.next);
					undoDescriptionsNext.push(changeDescription);
				}
				this.undoStack.value = undoStackNext;
				this.redoStack.value = [];
				this.undoDescriptions.value = undoDescriptionsNext;
				this.redoDescriptions.value = [];
			}
		}
	}

	registerBeforeInvocation(invocation:Invocation) {
		this.next.before.push(invocation);
	}

	registerUndoerInvocation(undoer:Undoer, key:number) {
		const existing = this.next.invocations[key]; // don't overwrite existing when coalescing
		if (existing === undefined) {
			this.next.invocations[key] = undoer;
		}
	}

	registerAfterInvocation(invocation:Invocation) {
		this.next.after.push(invocation);
	}

	applier(direction:number) {
		if (this.staging !== undefined) {
			const before = this.staging.before;
			before.forEach( invocation => {
				this.next.before.push(invocation(direction));
			});
			const invocations = this.staging.invocations;
			Object.keys(this.staging.invocations).forEach( key => {
				const invocation = invocations[Number(key)];
				invocation(direction);
			});
			const after = this.staging.after;
			after.forEach( invocation => {
				this.next.after.push(invocation(direction));
			});
		}
	}
}

export { Stack };