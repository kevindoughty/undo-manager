import { Signal, signal } from "@preact/signals-react";
import { Context } from "./context.js";

/**
 * A preservable object
 * @typedef {function} Preservable
 * @param {*} value
 * @param {boolean} interrupting
 */
class Preservable<T> {
	private _signal:Signal<T>;
	value:T;
	constructor(context:Context, value:T, interrupting?:boolean) {
		this._signal = signal(value);
		this.value = value; // required for typescript
		Object.defineProperty(this, "value", { // public
			get() {
				return this._signal.value;
			},
			set(value) {
				if (interrupting) context.makeNonCoalescingChange();
				this._signal.value = value;
			},
			enumerable: true,
			configurable: false
		});	
		context.registerPreservable(this, this.registerBefore.bind(this), this.registerAfter.bind(this));
	}

	private registerBefore(value:T) {
		const invocation = (direction:number) => {
			if (direction < 0) { // undo
				this._signal.value = value;
			}
			return this.registerBefore(value);
		};
		return invocation;
	}
	
	private registerAfter(value:T) {
		const invocation = (direction:number) => {
			if (direction > 0) { // redo
				this._signal.value = value;
			}
			return this.registerAfter(value);
		};
		return invocation;
	}

	subscribe(fn: (value:T) => void) : () => void {
		return this._signal.subscribe(fn);
	}

	peek() {
		return this._signal.peek();
	}

	valueOf() {
		return this._signal.value;
	}

	toString() {
		return String(this._signal.value);
	}

	toJSON() {
		return this._signal.value;
	}
}

export { Preservable };